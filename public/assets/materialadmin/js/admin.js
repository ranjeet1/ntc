$(function () {

    $.ajaxSetup({
        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
    });

    $('ul.mainnav').find('a').each(function (i, v) {

        var href = $(this).attr('href');

        var url = window.location.href;

        if (url == href || ( url.match(href + '/') && href != siteUrl + '/admin' ) || url.match(href + '#')) {
            $(this).parent('li').parents('li.treeview').find('li.active').removeClass('active');
            $(this).parent('li').addClass('active').parents('li.treeview').addClass('active');
        } else {

        }

    });

    $('.deleteContentForm').on('submit', function (e) {
        e.preventDefault();

        $this = $(this);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover any files related to this item!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $this.unbind('submit').submit();
            } else {
                swal.close();
            }
        });

    });


});
