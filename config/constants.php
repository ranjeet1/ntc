<?php

return [
    'type_superuser' => 0,
    'type_admin' => 1,
    'type_agent' => 2,

    'project_name' => 'Advertisement Management',
    'imageUploadPath' => 'uploads/content',

    'date_formats' => [
        'F j, Y' => 'March 14, 2016',
        'Y-m-d' =>  '2016-03-14',
        'm/d/Y' => '03/14/2016',
        'd/m/Y' => '14/03/2016'
    ],
//    'week' => [
//        '0' => 'Sunday',
//        '1' => 'Monday',
//        '2' => 'Tuesday',
//        '3' => 'Wednesday',
//        '4' => 'Thursday',
//        '5' => 'Friday',
//        '6' => 'Saturday',
//    ],
    'week' => [
        '0' => 'Sun',
        '1' => 'Mon',
        '2' => 'Tue',
        '3' => 'Wed',
        '4' => 'Thu',
        '5' => 'Fri',
        '6' => 'Sat'
    ],
    'page_statuses' => [
        'draft' => 'Draft',
        'publish' => 'Publish'
    ],
    'items_per_page' => [
        '1' => '1',
        '5' => '5',
        '10' => '10',
        '20' => '20',
        '50' => '50',
        '100' => '100'
    ],
    'payment_types' => [
        'cash' => 'Cash',
        'cheque' => 'Cheque'
    ],

    'import_path' => base_path('NTCDATA/import'),

    'import_files' =>  'NTCDATA/import',

    'rate_types' => [
        'flat' => 'Flat',
        'fixed' => 'Fixed',
        'incremental' => 'Incremental',
        'hybrid' => 'Hybrid'
    ]

];