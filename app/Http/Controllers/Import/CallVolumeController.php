<?php
namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Handlers\CallVolume\CallVolumeImport;

class CallVolumeController extends Controller {


    function __construct(){

    }


    function import(CallVolumeImport $import) {

        $import->handleImport();

    }

}