<?php
namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use App\Http\Requests\Import\ImportExcelFileRequest;
use App\Modules\Services\CurrencyExchange\CurrencyExchangeService;

/**
 * Class CurrencyExchangeController
 * @package App\Http\Controllers\Import
 */
class CurrencyExchangeController extends Controller
{

    /**
     * @var CurrencyExchangeService
     */
    protected $currencyExchange;

    /**
     * @param CurrencyExchangeService $currencyExchange
     */
    public function __construct(CurrencyExchangeService $currencyExchange)
    {
        $this->middleware('auth');
        $this->currencyExchange = $currencyExchange;
    }

    /**
     * Display view for importing forex rates
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        return view('admin.import.currency-exchange');
    }

    /**
     * Function for calculating the average rate of currency between two dates
     * @param $date1
     * @param $date2
     * @param $currency
     * @return mixed
     */
    public function average($date1, $date2, $currency)
    {
        return $this->currencyExchange->average($date1, $date2, $currency);
    }

    /**
     * Function for importing FOREX excel file
     * @param ImportExcelFileRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(ImportExcelFileRequest $request)
    {

        if ($this->currencyExchange->import($request->all())) {

            return redirect()->route('import.currency')->with('success', 'File imported successfully.');
        } else {
            return redirect()->route('import.currency')->with('error', 'File could not be imported.');
        }

    }
}
