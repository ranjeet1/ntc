<?php
namespace App\Http\Controllers\Import;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Handlers\CallVolume\CallVolumeImport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\Import\OperatorFileRequest;


use App\Modules\Models\Import\ImportOperatorFile;

class CallOpertorDetailsController extends Controller{

   private $operatorFiles;

    function __construct(ImportOperatorFile $operatorFiles){
         $this->operatorFiles = $operatorFiles;
         $this->page = 20;
    }

    public function getOperatorFiles()
    {
        $operatorDetails = $this->operatorFiles->paginate($this->page);
    	return view('file.file',compact('operatorDetails'));
    }

    public function postOperatorFiles(OperatorFileRequest $request)
    {


    	if($request->hasFile('file'))
        {
        	$fileName= rand(1000,10000). '.' . $request->file('file')->getClientOriginalExtension();

    		$filePath = base_path() . '/NTCDATA/import/crosswalk/';

        	$request->file('file')->move($filePath ,$fileName );

        	$filePath.= $fileName;

            $results = \Excel::load($filePath)->get();
            $date = date('Y-M-D');
            $udf1 = "null";
            $udf2 = "null";

            foreach($results as $rows)
			{
            	foreach($rows as $row)
				{
					$this->operatorFiles->create([

	               	    'source_file_name'=>$filePath,
	               		'imported_date'=>$date,
	               	    'operator_code'=>$row->billing_operator,
					    'operator_name'=>$row->billing_operator_name,
					    'udf1'=>$udf1,
					    'udf2'=>$udf2,
			        ]);


			   }

            }


	    }
	    return redirect()->back()->with('success', 'Operator file uploaded successfully!');
    }
}

   