<?php
namespace App\Http\Controllers\Handlers\TierDetail;

use Maatwebsite\Excel\Files\ImportHandler;

class TierDetailImportHandler implements ImportHandler
{

    function handle($import) {
        //dd('I am testing');
        $results = $import->get();
        $filename = $import->getFile();

        dd($import);die;

        $filename = substr($filename, strpos($filename, 'call_volume'), strlen($filename));

        DB::beginTransaction();

        $insertData = [];

        foreach($results as $result) {
            $insertData[] = $this->mapKeys($result, $filename);
        }

        DataCallVolume::insert($insertData);
        ImportedSourceFile::insert(
            ['source_file_name' => $filename, 'imported_date' => date('Y-m-d', time())]
        );

        DB::commit();

    }


    function mapKeys($result, $fileName) {

        $keyMap = [
            'billing operator name' => 'operator_name',
            'billing operator' => 'operator_code',
            'component direction' => 'component_direction',
            'tier' => 'tier',
            'call count' => 'call_count',
            'call type' => 'call_type',
            'event duration minutes' => 'call_duration',

            'billing_operator_name' => 'operator_name',
            'billing_operator' => 'operator_code',
            'component_direction' => 'component_direction',
            'call_count' => 'call_count',
            'call_type' => 'call_type',
            'event_duration_minutes' => 'call_duration'
        ];

        $data = [];

        foreach($result as $k => $v){
            foreach($keyMap as $kk => $vv) {
                if( strtolower($k) == strtolower($kk) || strstr(strtolower($kk), strtolower($k)) ){
                    $data[$vv] = $v;

                    continue;
                }
            }
            $data['source_file_name'] = $fileName;
            $data['received_date'] = date('Y-m-d', time());
        }

        return $data;

    }


}