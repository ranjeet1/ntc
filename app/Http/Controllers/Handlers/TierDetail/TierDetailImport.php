<?php
namespace App\Http\Controllers\Handlers\TierDetail;

use App\Http\Requests\Import\ImportExcelFileRequest;
use Maatwebsite\Excel\Files\ExcelFile;

class TierDetailImport extends ExcelFile
{
    private $request;

    function __construct(ImportExcelFileRequest $request) {
        $this->request = $request;
    }

    function getFile() {
        return $this->uploadFile();
    }


    function uploadFile() {
        $destinationPath = base_path() . '/uploads';

        $file = $this->request->file('file');
        $fileName = $file->getClientOriginalName();
        $file_type = $file->getClientOriginalExtension();

        $newFileName = sprintf("%s.%s", sha1($fileName . time()), $file_type);
        try {
            $uploaded = $file->move($destinationPath, $newFileName);
            return $uploaded->getRealPath();
        } catch (Exception $e) {
            $this->logger->error(sprintf('File could not be uploaded : %s', $e->getMessage()));
        }
    }

}




