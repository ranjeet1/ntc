<?php

namespace App\Http\Controllers\Handlers\CallVolume;

use Maatwebsite\Excel\Files\ExcelFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CallVolumeImport extends ExcelFile {

    function getFile() {

        //now get all files from the directory
        $directory = 'call_volume';
        $files = Storage::files($directory);

        //get all imported files from the database
        $sourceFiles = DB::select('select distinct source_file_name from imported_source_files');
        $sourceFilesArr = [];
        foreach($sourceFiles as $srcFile){
            $sourceFilesArr[] = $srcFile->source_file_name;
        }

        $newFiles = array_diff($files, $sourceFilesArr);

        foreach( $newFiles as $newFile ) {
            $filePath = config('constants.import_files').'/'.$newFile;
            return $filePath;
        }

        return false;

    }

}