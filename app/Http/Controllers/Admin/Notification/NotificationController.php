<?php
namespace App\Http\Controllers\Admin\Notification;

use App\Http\Controllers\Controller;
use App\Modules\Services\Notification\NotificationService;

/**
 * Class NotificationController
 * @package App\Http\Controllers\Admin\Notification
 */
class NotificationController extends Controller
{

    /**
     * @var NotificationService
     */
    protected $notification;

    /**
     * @param NotificationService $notification
     */
    public function __construct(NotificationService $notification)
    {
        $this->notification = $notification;
    }


}

?>


}
