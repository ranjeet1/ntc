<?php
namespace App\Http\Controllers\Admin\Contract;

use App\Modules\Services\Contract\ContractService;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Modules\Models\Operator\Operator;
use Redirect;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Admin\Contract
 */
class ProfileController extends Controller
{

    /**
     * @var ContractService
     */
    protected $contract;
    protected $operator;


    /**
     * ProfileController constructor.
     *
     * @param ContractService $contract
     * @param Operator        $operator
     */
    public function __construct(ContractService $contract, Operator $operator)
    {
        $this->contract = $contract;
        $this->operator = $operator;
    }


    /**
     * write brief description
     * @return string
     */
    public function index()
    {
        return 'go to contracts/profile/create';
    }

    /**
     * Show the form for creating a new Contract.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.contract.profile.create');
    }

    /**
     * Store a newly created Contract.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $contractData = $request->all();

        if ($request->hasFile('terms_and_conditions')) {

            $file = $request->file('terms_and_conditions');

            $destinationPath                      = base_path().'/public/termsConditions/Files/';
            $contractData['terms_and_conditions'] = date("Y-M-D").$file->getClientOriginalName();
            $file->move($destinationPath, $contractData['terms_and_conditions']);

        }
        if ($this->contract->create($contractData)) {
            return redirect()->route('admin.contracts.index')->with('success', 'Contract successfully created.');

        }

        return redirect()->route('admin.contracts.index')->with('error', 'Contract could not be created.');
    }


    /**
     * write brief description
     *
     * @param $contract_id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($contract_id)
    {
        $contracts = $this->contract->find($contract_id);

        return view('admin.contract.show', compact('contracts'));
    }

    /**
     * Show the form for editing the specified Contract.
     *
     * @param  int $contract_id
     *
     * @return Response
     */
    public function edit($contract_id)
    {
        $operatorIds = $this->operator->lists('name', 'id');
        $contracts   = $this->contract->find($contract_id);

        return view('admin.contract.edit', compact('contracts', 'operatorIds'));
    }

    /**
     * Update the specified Contract.
     *
     * @param Request $request
     * @param  int    $contract_id
     *
     * @return Response
     */
    public function update(Request $request, $contract_id)
    {
        $contractData                         = $request->all();
        $file                                 = $request->file('terms_and_conditions');
        $destinationPath                      = base_path().'/public/termsConditions/Files/';
        $contractData['terms_and_conditions'] = date("Y-M-D").$file->getClientOriginalName();
        $file->move($destinationPath, $contractData['terms_and_conditions']);

        if ($this->contract->update($contract_id, $contractData)) {
            return redirect()->route('admin.contracts.index')->with('success', 'Contract successfully updated.');

        }

        return redirect()->route('admin.contracts.index')->with('error', 'Contract could not be updated.');
    }

    /**
     * Remove the specified Contract.
     *
     * @param  int $post_id
     *
     * @return Response
     */
    public function destroy($contract_id)
    {
        if ($this->contract->delete($contract_id)) {
            return redirect()->back()->withSuccess('Contract successfully deleted.');
        }

        return redirect()->back()->withError('Contract could not be delete.');
    }
}
