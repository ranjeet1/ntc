<?php
namespace App\Http\Controllers\Admin\Contract;

use App\Modules\Services\Contract\ContractService;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\Contract\ContractRequest;
use App\Modules\Models\Operator\Operator;

/**
 * Class ContractController
 * @package App\Http\Controllers\Admin\Contract
 */
class ContractController extends Controller
{

    /**
     * @var ContractService
     */
    protected $contract;
    protected $operator;


    /**
     * ContractController constructor.
     *
     * @param ContractService $contract
     * @param Operator $operator
     */
    public function __construct(ContractService $contract, Operator $operator)
    {
        $this->contract = $contract;
        $this->operator = $operator;
    }


    /**
     * display listing of Contract
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id)
    {

        $operator = $this->operator->find($id);
        $contracts = $operator->contracts()->paginate(10);

        return view('admin.contract.index', compact('contracts', 'operator'));
    }

    /**
     * Display the specified Contract along with its rate profile.
     *
     * @param $operatorId
     * @param $contractId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($operatorId, $contractId)
    {
        $contract = $this->contract->find($contractId);
        if (!$contract)
            abort(404);

        $rateProfiles = $contract->rateProfiles;

        if ($rateProfiles->count() > 0) {
            $first = $rateProfiles->first(function ($key, $rateProfile) {
                return $rateProfile->ceiling == -1;
            });
            if($first) {
                $first = $rateProfiles->shift();
                $first->ceiling = "Infinity";
                $rateProfiles->push($first);
            }
        }

        return view('admin.contract.show', compact('contract', 'rateProfiles'));
    }


    /**
     * Show the form for creating a new Contract.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id)
    {

        $operator = $this->operator->find($id);
        if (!$operator) {
            abort(404);
        }

        return view('admin.contract.create', compact('operator'));

    }


    /**
     *Store a newly created Contract.
     *
     * @param Request $request
     * @param int $operatorId
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContractRequest $request, $operatorId)
    {
        $contractData = $request->all();

        $currentDate = date('Y-m-t');
        $breakDate = explode("-", $currentDate);
        $lastDayofMonth = $breakDate[2];
        $userEneterendDate = $request->get('end_date');

        $breakUserEnterDate = explode("/", $userEneterendDate);
        if (empty($request->get('end_date'))) {
            $finalYearMonthDay = "";
        } else {
            $yearAndMonth = $breakUserEnterDate[2] . "/" . $breakUserEnterDate[1];
            $finalYearMonthDay = $yearAndMonth . "/" . $lastDayofMonth;
        }
        $contractData = $request->all();
        $startDate = $request->get('start_date');
        $res = explode("/", $startDate);
        $changedDate = $res[2] . "/" . $res[1] . "/" . '01';
        $contractData['operator_id'] = $operatorId;
        $contractData['start_date'] = $changedDate;
        $contractData['end_date'] = $finalYearMonthDay;

        if ($this->contract->create($contractData)) {
            return redirect()->route('admin.operator.{id}.contracts.index', [$operatorId])->with(
                'success',
                'Contract
            successfully
            created.'
            );

        }

        return redirect()->route('admin.contracts.index')->with('error', 'Contract could not be created.');
    }


    /**
     * Show the form for editing the specified Contract
     *
     * @param $contractId
     * @param $operatorId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($operatorId, $contractId)
    {
        $contract = $this->contract->find($contractId);
        $contractId = $this->operator->find($operatorId);
        $operator = $this->operator->find($operatorId);

        return view('admin.contract.edit', compact('contract', 'contractId', 'operator'));
    }


    /**
     *  Update the specified Contract.
     *
     * @param Request $request
     * @param         $contractId
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ContractRequest $request, $contractId)
    {
        $contractData = $request->all();
        $startDate = $request->get('start_date');
        $res = explode("/", $startDate);
        $changedDate = $res[0] . "/" . $res[1] . "/" . '01';
        $contractData['start_date'] = $changedDate;
        $currentDate = date('Y-m-t');
        $breakDate = explode("-", $currentDate);
        $lastDayofMonth = $breakDate[2];
        $userEneterendDate = $request->get('end_date');
        $breakUserEnterDate = explode("/", $userEneterendDate);
        if (empty($request->get('end_date'))) {
            $finalYearMonthDay = "";
        } else {
            $yearAndMonth = $breakUserEnterDate[2] . '/' . $breakUserEnterDate[1];
            $finalYearMonthDay = $yearAndMonth . '-' . $lastDayofMonth;
        }
        $contractData['end_date'] = $finalYearMonthDay;
        // dd($contractData);
        if ($this->contract->update($contractId, $contractData)) {
            return redirect()->route('admin.operator.{id}.contracts.index', [$contractId])->with(
                'success',
                'Contract
            Updated Successfully.'
            );

        }

        return redirect()->route('admin.contracts.edit')->with('error', 'Contract could not be updated.');
    }


    /**
     * Remove the specified Contract.
     *
     * @param $contractId
     *
     * @return mixed
     */
    public function destroy($contractId)
    {
        if ($this->contract->delete($contractId)) {
            return redirect()->back()->with('success', 'Contract successfully deleted.');
        }

        return redirect()->back()->with('error', 'Contract could not be delete.');
    }
}
