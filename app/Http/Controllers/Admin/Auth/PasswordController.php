<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    protected $linkRequestView = 'admin.auth.passwords.email';

    protected $resetView = 'admin.auth.passwords.reset';

    protected $auth;

    use ResetsPasswords;


    /**
     * Create a new password controller instance.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->middleware('guest', ['except' => ['showChangeForm', 'change']]);
        $this->middleware('auth', ['only' => ['showChangeForm', 'change']]);
        $this->auth = $auth;
    }

    /**
     * Display change password form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangeForm()
    {
        return view('admin.auth.passwords.change');
    }

    /**
     * Change password
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change(Request $request)
    {
        $this->validate($request, [
            'present_password' => 'required',
            'new_password' => 'required|confirmed|min:6',
            'new_password_confirmation' => 'required'
        ]);
        if (!Hash::check($request->input('present_password'), $this->auth->user()->password)) {
            return redirect()->back()->with('error', 'Sorry, your present password is incorrect.');
        }
        $this->auth->user()->password = bcrypt($request->input('new_password'));
        $this->auth->user()->save();
        $this->auth->logout();

        return redirect('admin')->with('success', 'Your password has been changed.');
    }


}
