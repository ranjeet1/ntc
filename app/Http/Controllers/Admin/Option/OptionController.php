<?php

namespace App\Http\Controllers\Admin\Option;

use App\Modules\Services\Option\OptionService;
use App\Http\Requests\Option\OptionRequest;
use App\Http\Controllers\Controller;

class OptionController extends Controller
{
    protected $option;

    public function __construct(OptionService $option)
    {
        $this->middleware('auth');
        $this->option = $option;
    }

    /**
     * Show the form for updating options.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $options = $this->option->getOptions();
        $dateFormats = config('constants.date_formats');
        $week = config('constants.week');
        return view('admin.option.index', compact('options', 'dateFormats', 'week'));
    }

    /**
     * Save changes to option.
     *
     * @param OptionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OptionRequest $request)
    {
        $optionData = array_except($request->all(), '_token');
        if ($this->option->addOptions($optionData)) {
            return redirect()->route('admin.option')->with('success', 'Settings saved.');
        }

        return redirect()->route('admin.option.add')->with('error', 'Settings could not be saved.');
    }

    /**
     * Remove the specified option from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->option->delete($id)) {
            return redirect()->route('admin.option.index')->with('success', 'option deleted successfully.');
        }

        return redirect()->route('admin.option.index')->with('error', 'option could not be deleted.');
    }
}

