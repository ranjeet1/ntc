<?php

namespace App\Http\Controllers\Admin\Service;

use App\Modules\Services\Service\ServiceService;
use App\Http\Requests\Service\ServiceRequest;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    protected $service;

    public function __construct(ServiceService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Display a listing of the service.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = $this->service->paginate();

        return view('admin.service.index', compact('services'));
    }

    /**
     * Show the form for creating a new service.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.service.create');
    }

    /**
     * Store a newly created service in storage.
     *
     * @param ServiceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ServiceRequest $request)
    {
        if ($this->service->create($request->all())) {
            return redirect()->route('admin.service.index')->with('success', 'service created successfully.');
        }

        return redirect()->route('admin.service.create')->with('error', 'service could not be created.');
    }

    /**
     * Show the form for editing the specified service.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = $this->service->find($id);

        return view('admin.service.edit', compact('service'));
    }

    /**
     * Update the specified service in storage.
     *
     * @param ServiceRequest $request
     * @param                 $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ServiceRequest $request, $id)
    {
        if($this->service->update($id, $request->all())) {
            return redirect()->route('admin.service.index')->with('success', 'service updated successfully.');
        }

        return redirect()->route('admin.service.edit')->with('error', 'service could not be updated.');
    }

    /**
     * Remove the specified service from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->service->delete($id)) {
            return redirect()->route('admin.service.index')->with('success', 'service deleted successfully.');
        }

        return redirect()->route('admin.service.index')->with('error', 'service could not be deleted.');
    }
}

