<?php

namespace App\Http\Controllers\Admin\Role;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\PermissionRequest;
use App\Modules\Services\Role\PermissionService;

class PermissionController extends Controller
{
    private $permission;

    function __construct(PermissionService $permission) {
        $this->permission = $permission;
    }

    /**
     * write brief description
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $permissions = $this->permission->all();
        return view('admin.setting.index', compact('permissions'));
    }

    /**
     * write brief description
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('admin.setting.roles.create');
    }

    /**
     * write brief description
     * @param PermissionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PermissionRequest $request) {

        $this->permission->create($request->all());

        return redirect()->route('admin.roles.index')->with('success', 'Role saved successfully');

    }

    /**
     * write brief description
     * @param $permissionId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($permissionId) {
        $permission = $this->permission->find($permissionId);

        return view('admin.setting.roles.edit', compact('role'));
    }


    /**
     * write brief description
     * @param PermissionRequest $request
     * @param $permissionId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PermissionRequest $request, $permissionId) {

        $this->permission->update($permissionId, $request->all());

        return redirect()->route('admin.roles.index')->with('success', 'Role updated successfully');

    }

    /**
     * Delete the selected role
     * @param $permissionId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($permissionId) {
        $this->permission->delete($permissionId);

        return redirect()->route('admin.roles.index')->with('success', 'Role deleted successfully');
    }

}