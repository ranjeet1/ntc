<?php

namespace App\Http\Controllers\Admin\Role;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\RoleRequest;
use App\Modules\Services\Role\RoleService;

class RoleController extends Controller
{
    private $role;

    function __construct(RoleService $role) {
        $this->role = $role;
    }

    /**
     * write brief description
     * @return $this
     */
    public function index() {

        $roles = $this->role->all();
        return view('admin.setting.roles.index', compact('roles'));
    }

    /**
     * write brief description
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        return view('admin.setting.roles.create');
    }

    /**
     * write brief description
     * @param RoleRequest $request
     * @return $this
     */
    public function store(RoleRequest $request) {

        $this->role->create($request->all());

        return redirect()->route('admin.roles.index')->with('success', 'Role saved successfully');

    }

    /**
     * write brief description
     * @param $roleId
     * @return $this
     */
    public function edit($roleId) {
        $role = $this->role->find($roleId);

        return view('admin.setting.roles.edit', compact('role'));
    }


    /**
     * write brief description
     * @param RoleRequest $request
     * @param $roleId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RoleRequest $request, $roleId) {

        $this->role->update($roleId, $request->all());

        return redirect()->route('admin.roles.index')->with('success', 'Role updated successfully');

    }

    /**
     * Delete the selected role
     * @param $roleId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($roleId) {
        $this->role->delete($roleId);

        return redirect()->route('admin.roles.index')->with('success', 'Role deleted successfully');
    }

}