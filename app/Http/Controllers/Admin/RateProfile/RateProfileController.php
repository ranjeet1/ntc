<?php

namespace App\Http\Controllers\Admin\RateProfile;

use App\Modules\Services\Contract\ContractService;
use App\Modules\Services\RateProfile\RateProfileService;
use App\Http\Requests\RateProfile\RateProfileRequest;
use App\Http\Controllers\Controller;
use App\Modules\Services\TierDetail\TierService;

class RateProfileController extends Controller
{
    protected $rateProfile;

    protected $contractService;

    protected $tierService;

    public function __construct(RateProfileService $rateProfile, ContractService $contractService, TierService $tierService)
    {
        $this->middleware('auth');
        $this->rateProfile = $rateProfile;
        $this->contractService = $contractService;
        $this->tierService = $tierService;
    }

    /**
     * Display a listing of the rate profile.
     *
     * @param int $contractId
     * @return \Illuminate\Http\Response
     */
    public function index($contractId)
    {
        $contract = $this->contractService->find($contractId);

        if (!$contract)
            abort(404);

        $rateProfiles = $contract->rateProfiles()->paginate();

        return view('admin.rateProfile.index', compact('rateProfiles', 'contractId'));
    }

    /**
     * Show the form for creating a new rate profile.
     *
     * @param $contractId
     * @return \Illuminate\Http\Response
     */
    public function create($contractId)
    {
        $contract = $this->contractService->find($contractId);
        $tiers = $this->tierService->all();
        $rateTypes = config('constants.rate_types');

        if (!$contract)
            abort(404);

        return view('admin.rateProfile.create', compact('contract', 'tiers', 'rateTypes'));
    }

    /**
     * Store a newly created rate profile in storage.
     *
     * @param RateProfileRequest $request
     * @param int $contractId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($contractId, RateProfileRequest $request)
    {

        $result = $this->rateProfile->create($contractId, $request->all());

        if ($request->ajax()) {
            $redirectUrl = route('admin.contracts.{id}.rate-profile.index', [$contractId]);

            if ($result) {
                $request->session()->flash('success', 'Rate profile created successfully.');

                return ['status' => true, 'redirectUrl' => $redirectUrl];
            }

            $request->session()->flash('error', 'Rate profile could not be created.');

            return ['status' => false, 'redirectUrl' => $redirectUrl];

        } else {
            if ($result) {

                return redirect()->route('admin.contracts.{id}.rate-profile.index', [$contractId])->with('success', 'Rate profile created successfully.');
            }

            return redirect()->back()->with('error', 'Rate profile could not be created.');
        }


    }

    /**
     * Show the form for editing the specified rate profile.
     *
     * @param  int $id
     * @param  int $contractId
     * @return \Illuminate\Http\Response
     */
    public function edit($contractId, $id)
    {
        $contract = $this->contractService->find($contractId);

        if (!$contract)
            abort(404);

        $rateProfiles = $contract->rateProfiles;
        dd($rateProfiles);

        return view('admin.rateProfile.edit', compact('rateProfile', 'contract'));
    }

    /**
     * Update the specified rate profile in storage.
     *
     * @param RateProfileRequest $request
     * @param              int $id
     * @param              int $contractId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RateProfileRequest $request, $contractId, $id)
    {
        if ($this->rateProfile->update($id, $request->all())) {
            return redirect()->route('admin.contracts.{id}.rate-profile.index', [$contractId])->with('success', 'Rate profile updated successfully.');
        }

        return redirect()->back()->with('error', 'Rate profile could not be updated.');
    }

    /**
     * Remove the specified rate profile from storage.
     *
     * @param  int $id
     * @param int $contractId
     * @return \Illuminate\Http\Response
     */
    public function destroy($contractId, $id)
    {
        if ($this->rateProfile->delete($id)) {
            return redirect()->route('admin.contracts.{id}.rate-profile.index', [$contractId])->with('success', 'Rate profile deleted successfully.');
        }

        return redirect()->back()->with('error', 'Rate profile could not be deleted.');
    }
}

