<?php
namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Modules\Services\Role\RoleService;
use App\Modules\Services\User\UserService;
use App\Http\Requests\User\UserRequest;
use App\Modules\Services\Notification\NotificationService;
use Mail;


/**
 * Class UserController
 * @package App\Http\Controllers\User
 */
class UserController extends Controller
{

    /**
     * @var UserService
     */
    protected $user;
    protected $role;

    protected $request;

    /**
     * @param UserService $user
     */
    public function __construct(UserService $user, RoleService $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->user->paginate();
        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new user.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = $this->role->all();
        return view('admin.user.create', compact('roles'));
    }

    /**
     * Store a newly created user.
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        $userData = $request->all();
        $user = $this->user->create($userData);

        if ($user) {

            $data['email'] = $user->email;
            $data['confirmation_code'] = $user->confirmation_code;
            Mail::send('emails.welcome', $data, function ($message) use ($data) {
                $message->subject("Welcome to site name");
                $message->to($data['email']);
            });

            return redirect()->route('admin.user.index');
        }

        return redirect()->back()->with('error', 'User Could Not Be Created. Please Try Again Later');
    }


    /**
     * Display the specified user.
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($userId)
    {
        $user = $this->user->find($userId);
        $roles = $this->role->all();

        return view('admin.user.show', compact('user', 'roles'));
    }

    /**
     * Show the form for editing the specified user
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($userId)
    {
        $user = $this->user->find($userId);
        $roles = $this->role->all();
        return view('admin.user.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified user
     * @param UserRequest $request
     * @param NotificationService $notificationService
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, NotificationService $notificationService, $userId)
    {
        $userData = $request->all();
        if ($this->user->update($userId, $userData, $notificationService)) {

            return redirect()->route('admin.user.show', array($userId));

        }

        return redirect()->back()->with('error', 'User could not be update.');
    }

    /**
     * Remove the specified user
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($userId)
    {
        if ($this->user->delete($userId)) {
            return redirect()->route('admin.user.index');
        }

        return redirect()->back()->with('error', 'User could not be delete.');
    }

    /**
     * Verify the user
     * @param $confirmation_code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify($confirmation_code)
    {
        $user =$this->user->find($confirmation_code,'confirmation_code');
        if (!empty($user)) {
            $user->confirmed = 1;
            $user->save();
            return redirect()->route('dashboard');

        }

        //else return view with information for verification of email


    }

   

}
