<?php

namespace App\Http\Controllers\Admin\Operator;

use App\Modules\Services\Operator\OperatorTypeService;
use App\Http\Requests\Operator\OperatorTypeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class OperatorTypesController extends Controller
{
    protected $operatorType;

    /**
     * OperatorTypesController constructor.
     *
     * @param OperatorTypeService $operatorType
     */
    public function __construct(OperatorTypeService $operatorType)
    {
        $this->middleware('auth');
        $this->operatorType = $operatorType;
    }


    /**
     * display listing of operator services
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $filter['limit'] = 10;
        $operatorsTypes  = $this->operatorType->paginate($filter);

        return view('admin.operator.operator-type.index', compact('operatorsTypes'));
    }

    /**
     * Show the form for creating a new operator.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.operator.operator-type.create');
    }


    /**
     *storing operator types data
     *
     * @param OperatorTypeRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OperatorTypeRequest $request)
    {
        $operatorName = $request->get('name');
        $slug         = preg_replace('/[^A-Za-z0-9-]+/', '-', $operatorName);
        if ($this->operatorType->create(['name' => $operatorName, 'slug' => $slug])) {
            return redirect()->route('admin.operator-types.index')->with(
                'success',
                'Operator type created successfully.'
            );
        }

        return redirect()->route('admin.operator-types.create')->with('error', 'Operator type could not be created.');
    }


    /**
     * Show the form for editing the specified operator types
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $operator = $this->operatorType->find($id);

        return view('admin.operator.operator-type.edit', compact('operator'));
    }


    /**
     *
     *
     * @param Request $request
     * @param         $operatorId
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $operatorId)
    {
        if ($this->operatorType->update($operatorId, $request->only('name'))) {
            return redirect()->route('admin.operator-types.index')->with(
                'success',
                'Operator Type could  Updated Successfully.'
            );
        }

        return redirect()->route('admin.operator-types.index')->with('error', 'Operator Type could not be updated.');
    }

    /**
     * Remove the specified operator from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->operatorType->delete($id)) {
            return redirect()->route('admin.operator-types.index')->with(
                'success',
                'Operator Type deleted successfully.'
            );
        }

        return redirect()->route('admin.operator-types.index')->with('error', 'Operator Type could not be deleted.');
    }


}

