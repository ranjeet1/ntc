<?php

namespace App\Http\Controllers\Admin\Operator;

use App\Http\Requests\Operator\OperatorImportRequest;
use App\Modules\Services\Operator\OperatorService;
use App\Http\Requests\Operator\OperatorRequest;
use App\Http\Controllers\Controller;
use App\Modules\Services\Operator\OperatorTypeService;
use App\Modules\Services\Service\ServiceService;

class OperatorController extends Controller
{
    protected $operator;

    protected $operatorTypes;

    protected $service;

    public function __construct(OperatorService $operator,OperatorTypeService $operatorType,ServiceService $service)
    {
        $this->middleware('auth');
        $this->operator = $operator;
        $this->operatorTypes = $operatorType;
        $this->service =$service;

    }

    /**
     * Display a listing of the operator.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filter['limit'] = 10;
        $operators = $this->operator->paginate($filter);
        return view('admin.operator.index', compact('operators'));
    }

    /**
     * Show the form for creating a new operator.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.operator.create');
    }

    /**
     * Store a newly created operator in storage.
     *
     * @param OperatorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(OperatorRequest $request)
    {
        if ($this->operator->create($request->all())) {
            return redirect()->route('admin.operator.index')->with('success', 'Operator created successfully.');
        }

        return redirect()->route('admin.operator.create')->with('error', 'Operator could not be created.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $operator = $this->operator->find($id);
        $operatorTypes=$this->operatorTypes->all();
        $services=$this->service->all();
        $operatorServices=$operator->services()->get();



        $serviceIdLists=array();
        foreach($operatorServices as $service)
        {
            $serviceIdLists[]=$service->id;
        }

        $banks=$operator->banks()->get();




        return view('admin.operator.profile',compact('operator','operatorTypes','services','serviceIdLists','banks'));
        //return view('admin.operator.edit', compact('operator'));
    }

    /**
     * Update the specified operator in storage.
     *
     * @param OperatorRequest $request
     * @param                 $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(OperatorRequest $request,$id)
    {
        if ($this->operator->update($id, array_except($request->all(), ['name', 'code']))) {


            return redirect()->route('admin.operator.index')->with('success', 'Operator updated successfully.');
        }


        return redirect()->back()->with('error', 'Operator could not be updated.');
    }

    public function show($id)
    {

    }
    /**
     * Remove the specified operator from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->operator->delete($id)) {
            return redirect()->route('admin.operator.index')->with('success', 'Operator deleted successfully.');
        }

        return redirect()->route('admin.operator.index')->with('error', 'Operator could not be deleted.');
    }

    /**
     * Import excel sheet containing operator details into operators table
     *
     * @param OperatorImportRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(OperatorImportRequest $request)
    {
        if ($this->operator->import($request->all())) {
            return redirect()->route('admin.operator.index')->with('success', 'Operator imported successfully.');
        } else {
            return redirect()->route('admin.operator.index')->with('error', 'Operator could not be imported.');
        }
    }

}

