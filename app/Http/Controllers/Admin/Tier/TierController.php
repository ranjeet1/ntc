<?php
namespace App\Http\Controllers\Admin\Tier;

use App\Http\Requests\Tier\TierRequest;
use App\Modules\Services\TierDetail\TierService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Import\ImportExcelFileRequest;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Models\Tier\Tier;


/**
 * Class TierController
 * @package App\Http\Controllers\Tier
 */
class TierController extends Controller
{

    /**
     * @var TierService
     */
    protected $tier;

    protected $request;

    /**
     * @param TierService $tier
     */
    public function __construct(TierService $tier)
    {
        $this->tier = $tier;
    }

    /**
     * Display a listing of tiers.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $tiers = $this->tier->all();
        return view('admin.tier.index', compact('tiers'));
    }

    /**
     * Show the form for creating a new tier.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.tier.create');
    }

    /**
     * Store a newly created tier.
     * @param TierRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TierRequest $request)
    {
        $tierData = $request->only(['tier_name', 'tier_code']);

        if ($this->tier->create($tierData)) {
            return redirect()->route('admin.tier.index');
        }

        return redirect()->back()->with('error', 'Tier could not be create.');
    }

    /**
     * Display the specified tier.
     * @param $tierID
     */
    public function show($tierID)
    {

    }

    /**
     * Show the form for editing the specified tier
     * @param $tierID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($tierID)
    {
        $tier = $this->tier->find($tierID);
        return view('admin.tier.edit', compact('tier'));
    }

    /**
     * Update the specified tier
     * @param TierRequest $request
     * @param $tierID
     * @return mixed
     */
    public function update(TierRequest $request, $tierID)
    {

        $tierData = $request->only(['tier_name', 'tier_code']);
        if ($this->tier->update($tierID, $tierData)) {
            return redirect()->route('admin.tier.index')->with('success', 'Tier Updated Successfully');
        }

        return redirect()->back()->with('error', 'Tier could not be update.');
    }

    /**
     * Remove the specified tier.
     * @param $tierID
     * @return mixed
     */
    public function destroy($tierID)
    {
        if ($this->tier->delete($tierID)) {
            return redirect()->back()->with('success', 'Tier successfully deleted.');
        }

        return redirect()->back()->with('error', 'Tier could not be delete.');
    }

    /**
     * Import the tiers through excel file
     * @param ImportExcelFileRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doImport(ImportExcelFileRequest $request)
    {

        $this->request = $request;

        $uploaded = $this->__uploadFile();

        $filePath = $uploaded->getRealPath();
        $filename = $uploaded->getFilename();

        $reader = Excel::selectSheetsByIndex(0)->load($filePath, function ($reader) {
        });

        $results = $reader->get();

        DB::beginTransaction();

        $insertData = [];
        $allData = [];
        $resultKeyArr = [];

        foreach ($results as $result) {
            $allData[] = $this->mapKeys($result, $filename);
        }


        $allDataKeyArr = [];
        foreach ($allData as $k => $value) {
            $resultKeyArr[] = $value['tier_code'];
            $allDataKeyArr[$value['tier_code']] = $value;
        }


        //now get all tiers from database, and add new tiers, update existing tiers
        $allTiers = Tier::all();

        $tierKeys = [];
        foreach ($allTiers as $aTier) {
            $tierKeys[] = $aTier->tier_code;
        }

        $newTiers = [];
        $existingTiers = [];

        $newTiers = array_diff($resultKeyArr, $tierKeys);
        $existingTiers = array_diff($tierKeys, $resultKeyArr);

        foreach ($newTiers as $iData) {
            $insertData[] = $allDataKeyArr[$iData];
        }

        $updateData = [];
        foreach ($existingTiers as $k => $eTier) {
            Tier::where('tier_code', '=', $k)->update($allDataKeyArr[$eTier]);
        }

        Tier::insert($insertData);
        DB::commit();

        return redirect()->route('admin.tier.index');
    }

    /**
     * write brief description
     * @return \Symfony\Component\HttpFoundation\File\File
     */
    function __uploadFile()
    {
        $destinationPath = base_path() . '/public/uploads';

        $file = $this->request->file('file');
        $fileName = $file->getClientOriginalName();
        $file_type = $file->getClientOriginalExtension();

        $newFileName = sprintf("%s.%s", sha1($fileName . time()), $file_type);
        try {
            return $file->move($destinationPath, $newFileName);
        } catch (Exception $e) {
            $this->logger->error(sprintf('File could not be uploaded : %s', $e->getMessage()));
        }
    }

    /**
     * write brief description
     * @param $result
     * @param $fileName
     * @return array
     */
    function mapKeys($result, $fileName)
    {

        $keyMap = [
            'tier' => 'tier_code',
            'tier_name' => 'tier_name'
        ];

        $data = [];

        foreach ($result as $k => $v) {
            foreach ($keyMap as $kk => $vv) {
                if (strtolower($k) == strtolower($kk) || strstr(strtolower($kk), strtolower($k))) {
                    $data[$vv] = $v;
                    continue;
                }
            }
            $data['source_file_name'] = $fileName;
            $data['received_date'] = date('Y-m-d', time());
        }

        return $data;

    }


}
