<?php

namespace App\Http\Controllers\Admin\Invoice;

use App\Modules\Services\Invoice\InvoiceService;
use Redirect;
use App\Http\Requests\Invoice\InvoiceRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Modules\Models\Operator\Operator;
use App\Modules\Models\Invoice\Invoice;
use App\Modules\Models\Invoice\DataFinalAmount;
use App\Modules\Models\Invoice\InvoiceDetail;

class InvoiceController extends Controller
{


    protected $invoiceDetailService;
    protected $operator;
    protected $invoice;
    protected $invoiceDetail;

    /**
     * InvoiceController constructor.
     *
     * @param InvoiceService $invoiceDetailService
     */
    public function __construct(
        InvoiceService $invoiceDetailService,
        Operator $operator,
        Invoice $invoice,
        InvoiceDetail $invoiceDetail
    ) {

        $this->middleware('auth');
        $this->invoiceDetailService = $invoiceDetailService;
        $this->operator             = $operator;
        $this->invoice              = $invoice;
        $this->invoiceDetail        = $invoiceDetail;

    }


    /**
     * Display a listing of the Invoice details
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $invoices = $this->invoiceDetailService->invoice();

        return view('admin.invoice.list-of-inbound-invoices', compact('invoices'));

    }

    /**
     * Show the form for creating a new Invoice details.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('admin.invoice.index');
    }

    /**
     * Store a newly created Invoice details in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(InvoiceRequest $request)
    {

        $invoiceData           = $request->all();
        $invoiceData['status'] = '0';

        if ($invoiceId = $this->invoiceDetailService->create($invoiceData)) {
            $invoicelastId = $invoiceId->id;

            $operatorId             = $invoiceId->operator_id;
            $opertorInformation     = DB::table('operators')->where('id', $operatorId)->first();
            $operatorCode           = $opertorInformation->code;
            $infoFromDataFinalTable = DB::table('data_final_amount')->where('billing_operator', $operatorCode)->first();
            $tier                   = $infoFromDataFinalTable->rate_profile;
            $callCount              = $infoFromDataFinalTable->call_count;
            $callMinute             = $infoFromDataFinalTable->event_duration_minutes;
            $rateApplied            = $infoFromDataFinalTable->average_rate;
            $totalAmount            = $infoFromDataFinalTable->total_amount;


            $this->invoiceDetail->create(
                [
                    'invoice_id'   => $invoicelastId,
                    'call_count'   => $callCount,
                    'call_minutes' => $callMinute,
                    'average_rate' => $rateApplied,
                    'total_amount' => $totalAmount,
                    'tier'         => $tier,
                ]
            );


            return redirect()->back()->with('success', 'Invoice Created successfully.');

        }

        return redirect()->back()->with('error', 'Invoice Details could not be created.');
    }

    /**
     * Show the form for editing the specified Invoice details.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoices = Invoice::where('id', $id)->first();

        return view('admin.invoice.edit', compact('invoices'));
    }

    /**
     * Update the specified invoice details in storage.
     *
     * @param Request         $request
     * @param                 $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $invoiceId)
    {
        if ($id = $this->invoiceDetailService->update(
            $invoiceId,
            array_except($request->all(), ['document_num', 'invoice_number'])
        )
        )

        {
            return redirect()->route('company-invoice-details',[$id])->with('success',' Invoice updated
           successfully');
        }

        return redirect()->route('company-invoice-details',[$id])->with('error', 'Invoice could not be updated.');
    }

    /**
     * Remove the specified invoice details from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->invoiceDetailService->delete($id)) {
            return redirect()->back()->with('success', 'Operator deleted successfully.');
        }

        return redirect()->back()->with('error', 'Operator could not be deleted.');
    }

    public function show($code)
    {
        $invoices        = $this->operator->where('id', $code)->first();
        $monthlyInvoices = $this->invoiceDetailService->monthlyInvoices($code);

        //invoices details exist for that code then show details else show entry form
        $invoicesDataExist = Invoice::where('operator_id', $code)->count();
        //fetch invoices details for that operator code if present

        $invoicesDetail = Invoice::where('operator_id', $code)->first();

        $sumAmount = DataFinalAmount::sum('total_amount');

        return view(
            'admin.invoice.index',
            compact('invoices', 'sumAmount', 'invoicesDataExist', 'invoicesDetail', 'monthlyInvoices')
        );

    }

    public function invoiceOfCompany($id)
    {

        $companyInvoices = $this->invoice->where('operator_id', $id)->with('operators')->get();
        $companyName     = $this->invoiceDetailService->invoiceOfCompany($id);

        if (!$companyInvoices) {
            return redirect()->back();
        }


        return view("admin.invoice.company-invoice", compact('companyInvoices', 'companyName'));
    }

    public function invoiceDetails($invoiceNumber)
    {

        $invoicesDetail                                       = $this->invoice->where('invoice_number', $invoiceNumber)
                                                                              ->first();
        $matchOperatorCodeWithBillingOperatorofDataFinalTable = $this->invoiceDetailService->invoiceDetails
        (
            $invoiceNumber
        );
        $operatorDetails                                      = $this->invoiceDetailService->operatorDetails(
            $invoiceNumber
        );

        $invoiceNumber = DB::table('invoices')->where('invoice_number', $invoiceNumber)->first();
        $invoiceId     = $invoiceNumber->id;
        $operatorId    = $invoiceNumber->operator_id;


        $monthlyOutBoundInvoicesDetails = DB::table('invoice_details')->where('invoice_id', $invoiceId)->get();
        $sumCallCount                   = DB::table('invoice_details')->where('invoice_id', $invoiceId)->sum(
            'call_count'
        );
        $sumCallMinute                  = DB::table('invoice_details')->where('invoice_id', $invoiceId)->sum(
            'call_minutes'
        );
        $sumTotalAmount                 = DB::table('invoice_details')->where('invoice_id', $invoiceId)->sum(
            'total_amount'
        );

        //find operator country
        $operatorDetails = DB::table('operators')->where('id', $operatorId)->first();

        return view(
            'admin.invoice.invoice-details',
            compact(
                'invoicesDetail',
                'matchOperatorCodeWithBillingOperatorofDataFinalTable',
                'operatorDetails',
                'monthlyOutBoundInvoicesDetails',
                'operatorDetails',
                'sumCallCount',
                'sumCallMinute',
                'sumTotalAmount'
            )
        );
    }

    public function selectYearAndMonthForInvoice(Request $request)
    {
        $invoices = $this->invoiceDetailService->selectYearMonthforInvoiceService($request);
        return view('admin.invoice.list-of-inbound-invoices', compact('invoices'));

    }

}

