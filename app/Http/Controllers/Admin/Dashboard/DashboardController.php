<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Controller;
use App\Modules\Services\User\UserService;
use Illuminate\Support\Facades\Auth;

    class DashboardController extends Controller
{

    public function __construct(UserService $user)
    {
        $this->middleware('auth');
    }

    /**
     * Display dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard.index');
    }

}

