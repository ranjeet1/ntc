<?php

namespace App\Http\Controllers\Export\Advertisement;

use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Files\NewExcelFile;

class AdvertisementListExport extends NewExcelFile
{

    /**
     * Get file
     * @return string
     */
    public function getFilename()
    {
        return 'Advertisement Lists';
    }


}
