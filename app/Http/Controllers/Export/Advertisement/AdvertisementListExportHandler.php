<?php

namespace App\Http\Controllers\Export\Advertisement;

use App\Http\Requests;
use App\Modules\Services\Advertisement\AdvertisementService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Files\ExportHandler;

class AdvertisementListExportHandler implements ExportHandler
{
    private $advertisement;
    private $request;

    public function __construct(AdvertisementService $advertisement, Request $request)
    {

        $this->advertisement = $advertisement;
        $this->request = $request;

    }

    /**
     * @param AgentListExport $export
     * @return mixed
     */
    public function handle($export)
    {
        return $export->sheet('Advertisements', function ($sheet) {
            $advertisements = $this->advertisement->get($this->request->all());

            $finalArr = [];
            foreach ($advertisements as $adv) {
                $advertisementsArr = [];

                $advertisementsArr['Name'] = $adv->name;
                //$advertisementsArr['Image'] = '<img src="'. asset('uploads/content/thumb/'.$adv->image). '" />';
                $advertisementsArr['Type'] = $adv->advertisementType->name;
                $advertisementsArr['Agent'] = $adv->agent->name;
                $advertisementsArr['Cost'] = 'NRS. ' . $adv->total_cost;
                $advertisementsArr['Start Date'] = formatDate($adv->start_date);
                $advertisementsArr['End Date'] = formatDate($adv->end_date);

                $finalArr[] = $advertisementsArr;
            }

            $sheet->fromArray($finalArr);

            $sheet->row(1, function ($row) {
                $row->setBackground('#5D9BB0');
                $row->setFontColor('#ffffff');
            });

        })->export('xls');
    }

}
