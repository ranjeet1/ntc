<?php

namespace App\Http\Controllers\Export\Agent;

use App\Http\Requests;
use App\Modules\Services\User\UserService;
use Maatwebsite\Excel\Files\ExportHandler;

class AgentListExportHandler implements ExportHandler
{
    private $user;

    public function __construct(UserService $user){

        $this->user = $user;

    }
    /**
     * @param AgentListExport $export
     * @return mixed
     */
    public function handle($export)
    {
        return $export->sheet('Agents', function($sheet)
        {
            $agents = $this->user->get(config('constants.type_agent'), ['name', 'email', 'phone', 'remarks'])->toArray();

            $sheet->fromArray($agents);

            $sheet->row(1, function($row) {
                $row->setBackground('#5D9BB0');
                $row->setFontColor('#ffffff');
            });

        })->export('xls');
    }

}
