<?php

namespace App\Http\Controllers\Export\Agent;

use Illuminate\Http\Request;

use App\Http\Requests;
use Maatwebsite\Excel\Files\NewExcelFile;

class AgentListExport extends NewExcelFile
{

    /**
     * Get file
     * @return string
     */
    public function getFilename()
    {
        return 'All Agent Lists';
    }


}
