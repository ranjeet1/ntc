<?php

foreach(File::allFiles(app_path().'/Http/Routes') as $route) {
    require_once $route->getPathname();
}

$router->get('ms_invoice_incoming', function() {
	return view('designs.invoice-incoming');
});

$router->get('ms_invoice_outgoing', function() {
	return view('designs.invoice-outgoing');
});

$router->get('ms_contract_incoming', function() {
	return view('designs.contract-incoming');
});

$router->get('ms_contract_incoming_display', function() {
	return view('designs.contract-incoming-display');
});


