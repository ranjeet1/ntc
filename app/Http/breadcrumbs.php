<?php

//Put all your breadcumbs here

//Dashboard(HOME)
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', url('/'));
});
//General Setting
Breadcrumbs::register('admin.option', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Setting', route('admin.option'));
});

// User
Breadcrumbs::register('admin.user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('User', route('admin.user.index'));
});

//User Profile
Breadcrumbs::register('admin.user.show', function ($breadcrumbs, $userId) {
    $breadcrumbs->parent('admin.user.index');
    $breadcrumbs->push('Profile', route('admin.user.show', $userId));
});

//User Profile Edit
Breadcrumbs::register('admin.user.edit', function ($breadcrumbs, $userId) {
    $breadcrumbs->parent('admin.user.show', $userId);
    $breadcrumbs->push('Edit', route('admin.user.edit', $userId));
});

//Tier
Breadcrumbs::register('admin.tier.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Tier', route('admin.tier.index'));
});


//Edit Tier
Breadcrumbs::register('admin.tier.edit', function ($breadcrumbs, $tierId) {
    $breadcrumbs->parent('admin.tier.index');
    $breadcrumbs->push('Edit', route('admin.tier.edit', $tierId));
});



?>

