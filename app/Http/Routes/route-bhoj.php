<?php


$router->get('/', 'Admin\Auth\AuthController@showLoginForm');

$router->group(['namespace' => 'Import', 'prefix' => 'admin'], function ($router) {

    $router->get('import-call-volume', 'CallVolumeController@import');

    $router->get('crosswalk-operator-details', 'CallOpertorDetailsController@getOperatorFiles');
    $router->post('crosswalk-operator-details', 'CallOpertorDetailsController@postOperatorFiles');


});


$router->group(['namespace' => 'Admin', 'prefix' => 'admin'], function ($router) {

    $router->resource('roles', 'Role\RoleController');
    $router->resource('permissions', 'Role\PermissionController');

});
