<?php

$router->group(['namespace' => 'Import', 'prefix' => 'admin'], function ($router) {

    $router->get('import-tier-details', ['as' => 'admin.import.tier', 'uses' => 'TierController@index']);
    $router->post('import-tier-details', ['as' => 'admin.import.tier', 'uses' => 'TierController@doImport']);

});