<?php


$router->group(['namespace' => 'Admin', 'prefix' => 'admin'], function ($router) {

    $router->resource('/tier', 'Tier\TierController');

    $router->post('import-tier-details', ['as' => 'admin.tier.import', 'uses' => 'Tier\TierController@doImport']);

    $router->resource('/user', 'User\UserController');
    $router->get('verify/{code}', ['as' => 'user.verify', 'uses' => 'User\UserController@verify']);

    


});

$router->group(['namespace' => 'Import', 'prefix' => 'admin'], function ($router) {

    $router->get('import-currency', ['as' => 'import.currency', 'uses' => 'CurrencyExchangeController@index']);
    $router->post('import-currency', ['as' => 'import.currency.add', 'uses' => 'CurrencyExchangeController@import']);
    $router->get('get-average/{date1}/{date2}/{currency}', ['as' => 'currency.average', 'uses' => 'CurrencyExchangeController@average']);

});






?>