<?php
$router->get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

$router->group(['namespace' => 'Admin', 'prefix' => 'admin'], function ($router) {

    Route::auth();
    $router->any('register', function () {
        return view('errors.404');
    });

    $router->get('/', ['as' => 'dashboard', 'uses' => 'Dashboard\DashboardController@index']);



    //Option
    $router->get('option', ['as' => 'admin.option', 'uses' => 'Option\OptionController@index']);
    $router->post('option', ['as' => 'admin.option.add', 'uses' => 'Option\OptionController@store']);

    //Operator
    $router->resource('operator', 'Operator\OperatorController');
    $router->post('operator/import', ['as' => 'importOperator', 'uses' => 'Operator\OperatorController@import']);
   
   //operator types
    $router->resource('operator-types', 'Operator\OperatorTypesController');
    


    //invoice
    $router->resource('invoice', 'Invoice\InvoiceController');

    $router->get('operator/{code}/invoice', 'Invoice\InvoiceController@show');
    $router->get('operator/invoice/{id}',['as'=>'company-invoice','uses'=>
        'Invoice\InvoiceController@invoiceOfCompany']);
    $router->get('operator/invoice/details/{invoiceNumber}',['as'=>'company-invoice-details','uses'=>
        'Invoice\InvoiceController@invoiceDetails']);
    $router->get('invoice/search/results',['as'=>'search-invoices','uses'=>
    'Invoice\InvoiceController@selectYearAndMonthForInvoice']);



    //Contracts
    $router->resource('operator/{id}/contracts', 'Contract\ContractController');
    $router->resource('/contracts', 'Contract\ContractController');

    //contract profile
    $router->resource('/contracts/profile', 'Contract\ProfileController');




    // Password Change Routes...
    $router->get('password/change', 'Auth\PasswordController@showChangeForm');
    $router->post('password/change', ['as' => 'changeUserPassword', 'uses' => 'Auth\PasswordController@change']);


    $router->resource('contracts/{id}/rate-profile', 'RateProfile\RateProfileController');


    $router->resource('service','Service\ServiceController');


});

?>