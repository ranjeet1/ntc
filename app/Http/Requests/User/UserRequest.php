<?php
namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [

        ];

        if ($this->isMethod('patch') || $this->isMethod('put')) {
            $id = $this->segment(3);
            $rules['email'] = "required|unique:users,email,$id";

        } else {
            $rules['email'] = "required|unique:users,email";
        }

        return $rules;
    }
}
