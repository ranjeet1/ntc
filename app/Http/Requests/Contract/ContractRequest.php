<?php

namespace App\Http\Requests\Contract;

use App\Http\Requests\Request;

class ContractRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $endDate = $this->get('end_date');
        $rules   = [];


        if (empty($endDate)) {
            $rules['start_date']    = "required";
            $rules['interest_rate'] = "numeric:contracts";
        } else {
            $rules['start_date']    = 'required|date|before:end_date';
            $rules['end_date']      = 'date|after:start_date';
            $rules['interest_rate'] = "numeric:contracts";
        }

        return $rules;

    }

}
