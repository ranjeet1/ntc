<?php
namespace App\Http\Requests\Notification;

use App\Http\Requests\Request;

class NotificationRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'created_by' => 'required',
            'created_for' => 'required',
            'message' => 'required',
            'link' => 'required',
            'status' => 'required'

        ];
    }
}
