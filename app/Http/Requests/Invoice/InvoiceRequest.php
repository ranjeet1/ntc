<?php

namespace App\Http\Requests\Invoice;

use App\Http\Requests\Request;

class InvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'settlement_date'=>'required',
            'settlement_due_date'=>'required',
            'billing_start_date'=>'required',
            'billing_end_date'=>'required',
            'document_num' => 'required|unique:invoices',
            'invoice_number'=>'required|unique:invoices',


        ];
    }
}
