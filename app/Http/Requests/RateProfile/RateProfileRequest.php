<?php

namespace App\Http\Requests\RateProfile;

use App\Http\Requests\Request;

class RateProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'tierDetailId' => 'required',
            'rateType' => 'required'
        ];

        //write additional conditional rules here

        return $rules;
    }
}
