<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\Request;

class RoleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|unique:roles',
            'display_name' => 'required'
        ];

        if($this->isMethod('put') || $this->isMethod('patch')){
            $id = $this->segment(3);
            $rules['name'] = 'required|unique:roles,id,'.$id;
        }

        return $rules;
    }
}
