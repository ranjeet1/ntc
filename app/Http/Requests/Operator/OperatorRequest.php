<?php

namespace App\Http\Requests\Operator;

use App\Http\Requests\Request;

class OperatorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd('operreq');
        $rules = [];
        $rules['operator_type_id']='required';

        if($this->isMethod('patch') || $this->isMethod('put')) {
            $id = $this->segment(3);
            //$rules['code'] = "required|unique:operators,code,$id";

            $rules['name'] = "required|unique:operators,name,$id";
        } else {
            $rules['code'] = 'required|unique:operators,code';
            $rules['name'] = 'required|unique:operators,name';

        }

        return $rules;
    }
}
