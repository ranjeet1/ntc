<?php

namespace App\Http\Requests\Operator;

use App\Http\Requests\Request;

class OperatorTypeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:operator_types',
            'slug'=>'unique:operator_types'
        ];
    }
}
