<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerUserRepo();

        $this->registerAdvertisementTypeRepo();
        $this->registerAdvertisementRepo();
        $this->registerOptionRepo();
        $this->registerAdvertisementTransactionRepo();

        $this->registerInvoiceRepo();


        $this->registerRoleRepo();
        $this->registerNotificationRepo();
        $this->registerRateProfileRepo();
    }

    private function registerAdvertisementTransactionRepo()
    {
        $this->app->bind(
            'App\Modules\Repositories\AdvertisementTransaction\AdvertisementTransactionRepositoryInterface',
            'App\Modules\Repositories\AdvertisementTransaction\AdvertisementTransactionRepository'
        );
    }

    private function registerUserRepo()
    {
        $this->app->bind(
            'App\Modules\Repositories\User\UserRepositoryInterface',
            'App\Modules\Repositories\User\UserRepository'
        );
    }

    private function registerAdvertisementTypeRepo()
    {
        $this->app->bind(
            'App\Modules\Repositories\AdvertisementType\AdvertisementTypeRepositoryInterface',
            'App\Modules\Repositories\AdvertisementType\AdvertisementTypeRepository'
        );
    }

    private function registerAdvertisementRepo()
    {
        $this->app->bind(
            'App\Modules\Repositories\Advertisement\AdvertisementRepositoryInterface',
            'App\Modules\Repositories\Advertisement\AdvertisementRepository'
        );
    }

    private function registerOptionRepo()
    {
        $this->app->bind(
            'App\Modules\Repositories\Option\OptionRepositoryInterface',
            'App\Modules\Repositories\Option\OptionRepository'
        );


        $this->app->bind(
            'App\Modules\Repositories\Operator\OperatorRepositoryInterface',
            'App\Modules\Repositories\Operator\OperatorRepository'
        );
        
          $this->app->bind(
            'App\Modules\Repositories\TierDetail\TierRepositoryInterface',
            'App\Modules\Repositories\TierDetail\TierRepository'
        );

          $this->app->bind(
            'App\Modules\Repositories\CurrencyExchange\CurrencyExchangeRepositoryInterface',
            'App\Modules\Repositories\CurrencyExchange\CurrencyExchangeRepository'
        );

         $this->app->bind(
            'App\Modules\Repositories\Operator\OperatorTypeRepositoryInterface',
            'App\Modules\Repositories\Operator\OperatorTypeRepository'
        );

        $this->app->bind(
            'App\Modules\Repositories\Contract\ContractRepositoryInterface',
            'App\Modules\Repositories\Contract\ContractRepository'
        );


        $this->app->bind(
            'App\Modules\Repositories\Service\ServiceRepositoryInterface',
            'App\Modules\Repositories\Service\ServiceRepository'
        );
    }
    private function registerInvoiceRepo()
    {
        $this->app->bind(
            'App\Modules\Repositories\Invoice\InvoiceRepositoryInterface',
            'App\Modules\Repositories\Invoice\InvoiceRepository'

        );
    }

    private function registerRoleRepo()
    {
        $this->app->bind(
            'App\Modules\Repositories\Role\RoleRepositoryInterface',
            'App\Modules\Repositories\Role\RoleRepository'
        );
    }

    public function registerRateProfileRepo()
    {
        $this->app->bind(
            'App\Modules\Repositories\RateProfile\RateProfileRepositoryInterface',
            'App\Modules\Repositories\RateProfile\RateProfileRepository'
        );

         $this->app->bind(
            'App\Modules\Repositories\Role\PermissionRepositoryInterface',
            'App\Modules\Repositories\Role\PermissionRepository'
        );
    }


    private function registerNotificationRepo() {
        $this->app->bind(
            'App\Modules\Repositories\Notification\NotificationRepositoryInterface',
            'App\Modules\Repositories\Notification\NotificationRepository'
        );
    }

}
