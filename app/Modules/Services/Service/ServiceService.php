<?php

namespace App\Modules\Services\Service;

use App\Modules\Models\Service\ServiceModel;
use App\Modules\Repositories\Service\ServiceRepositoryInterface;
use App\Modules\Services\Service;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Psr\Log\LoggerInterface;

/**
 * Class ServiceService
 * @package App\Modules\Services\Service
 */
Class ServiceService extends Service
{
    /**
     * @var ServiceRepositoryInterface
     */
    protected $service;

    /**
     * @var LoggerInterface
     */
    protected $logger;


    /**
     * @param ServiceRepositoryInterface $service
     * @param LoggerInterface $logger
     */
    public function __construct(ServiceRepositoryInterface $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }

    /**
     * Create new Service
     *
     * @param array $serviceData
     * @return Service|null
     */
    public function create(array $serviceData)
    {
        try {
            $service = $this->service->create($serviceData);
            $this->logger->info('Service created successfully', $serviceData);

            return $service;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return null;
        }
    }

    /**
     * Paginate all Service
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 5;

        return $this->service->paginate($filter);
    }

    /**
     * Get all Services
     *
     * @return Collection
     */
    public function all()
    {
        return $this->service->all();
    }

    /**
     * Get a Service
     *
     * @param $serviceId
     * @return Service|null
     */
    public function find($serviceId)
    {
        try {
            return $this->service->find($serviceId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update a Service
     *
     * @param       $serviceId
     * @param array $serviceData
     * @return bool
     */
    public function update($serviceId, array $serviceData)
    {
        try {
            $service = $this->service->find($serviceId);
            $service = $service->update($serviceData);
            $this->logger->info(' created successfully', $serviceData);

            return $service;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Delete a Service
     *
     * @param $serviceId
     * @return bool
     */
    public function delete($serviceId)
    {
        try {
            $service = $this->service->find($serviceId);

            return $service->delete();
        } catch (Exception $e) {
            return false;
        }
    }
}
