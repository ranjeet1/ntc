<?php
namespace App\Modules\Services;

use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Modules\Service\Notification\NotificationService;

/**
 * Class Service
 *
 * @package App\Services
 */
abstract class Service
{

    protected $uploadPath;


    /**
     * Upload contract file
     *
     * @param UploadedFile $file
     * @param int $width
     * @param int $height
     * @return bool
     */
    public function upload(UploadedFile $file, $width = 320, $height = 240)
    {
        
        $destination = $this->uploadPath;

        if ($file->isValid()) {
            $fileName = $file->getClientOriginalName();
            $file_type = $file->getClientOriginalExtension();
            $newFileName = sprintf("%s.%s", sha1($fileName . time()), $file_type);
            try {
                $image = $file->move($destination, $newFileName);
                if(substr($file->getClientMimeType(), 0, 5) == 'image')
                    $this->createThumb($image, $width, $height);
                    
                return $image->getFilename();
            } catch (Exception $e) {
                
                $this->logger->error(sprintf('File could not be uploaded : %s', $e->getMessage()));
            }
            ;
            return false;

        }
      
        return false;
    }


    /**
     * Create Image thumb
     *
     * @param File $image
     * @param int $width
     * @param int $height
     * @return boolean
     */
    public function createThumb(File $image, $width = 320, $height = 240)
    {
        $img = Image::make($image->getPathname());
        $img->fit($width, $height);
        $path = sprintf('%s/thumb/%s', $image->getPath(), $image->getFilename());
        $directory = sprintf('%s/thumb', $image->getPath());
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
        return $img->save($path);
    }

    /**
     * Delete Image
     *
     * @param $image
     * @return bool
     */
    public function deleteImage($image)
    {
        $path = $this->uploadPath;
        try {
            $large = sprintf('%s/thumb/%s', $path, $image);
            unset($large);
            $thumb = sprintf('%s/thumb/%s', $path, $image);
            unset($thumb);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

  

}
