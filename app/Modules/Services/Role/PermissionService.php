<?php

namespace App\Modules\Services\Role;

use App\Modules\Services\Service;
use App\Modules\Repositories\Role\PermissionRepositoryInterface;
use Psr\Log\LoggerInterface;

class PermissionService extends Service
{
    /**
     * @var PermissionRepositoryInterface
     */
    protected $permission;
    protected $logger;

    /**
     * @param PermissionRepositoryInterface $permission
     * @param Log $log
     */
    public function __construct(PermissionRepositoryInterface $permission, LoggerInterface $log)
    {
        $this->permission = $permission;
        $this->logger = $log;
    }

    /**
     * write brief description
     * @param array $roleData
     * @return \App\Modules\Models\Role\Role|null
     */
    public function create(array $permissionData)
    {
        try {
            return $this->permission->create($permissionData);
        } catch (\Exception $e) {
            $this->logger->error('Permission->create: ' . $e->getMessage());
            return null;
        }
    }

    /**
     * Role Pagination
     *
     * @param array $filter
     * @return collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 5;

        return $this->permission->paginate($filter);
    }

    /**
     * write brief description
     * @param $roleId
     * @param array $roleData
     * @return bool
     */
    public function update($roleId, array $roleData)
    {
        try {
            $role = $this->permission->find($roleId);
            $role = $role->update($roleData);
            $this->logger->info('Role updated successfully', $roleData);

            return $role;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Delete Role
     *
     * @param $roleId
     * @return mixed
     */
    public function delete($roleId)
    {
        try {
            $role = $this->permission->find($roleId);

            return $role->delete();
        } catch (\Exception $e) {
            $this->logger->error('Role->delete: ' . $e->getMessage());

            return false;
        }
    }

    /**
     * Get Role by ID
     *
     * @param $roleId
     * @return Role
     */
    public function find($roleId)
    {
        try {
            return $this->permission->find($roleId);
        } catch (\Exception $e) {
            $this->logger->error('Role->find: ' . $e->getMessage());

            return null;
        }
    }

    /**
     * write brief description
     */
    public function all() {
        return $this->permission->all();
    }
}