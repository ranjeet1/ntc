<?php

namespace App\Modules\Services\Role;

use App\Modules\Repositories\Role\RoleRepositoryInterface;
use App\Modules\Services\Service;
use Illuminate\Support\Facades\Config;
use Psr\Log\LoggerInterface;


class RoleService extends Service
{

    /**
     * @var RoleRepositoryInterface
     */
    protected $role;
    protected $logger;

    /**
     * @param RoleRepositoryInterface $role
     * @param LoggerInterface $log
     */
    public function __construct(RoleRepositoryInterface $role, LoggerInterface $log)
    {
        $this->role = $role;
        $this->logger = $log;
    }

    /**
     * write brief description
     * @param array $roleData
     * @return \App\Modules\Models\Role\Role|null
     */
    public function create(array $roleData)
    {
        try {
            return $this->role->create($roleData);
        } catch (\Exception $e) {
            $this->logger->error('Role->create: ' . $e->getMessage());
            return null;
        }
    }

    /**
     * Role Pagination
     *
     * @param array $filter
     * @return collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 5;

        return $this->role->paginate($filter);
    }

    /**
     * write brief description
     * @param $roleId
     * @param array $roleData
     * @return bool
     */
    public function update($roleId, array $roleData)
    {
        try {
            $role = $this->role->find($roleId);
            $role = $role->update($roleData);
            $this->logger->info('Role updated successfully', $roleData);

            return $role;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Delete Role
     *
     * @param $roleId
     * @return mixed
     */
    public function delete($roleId)
    {
        try {
            $role = $this->role->find($roleId);

            return $role->delete();
        } catch (\Exception $e) {
            $this->logger->error('Role->delete: ' . $e->getMessage());

            return false;
        }
    }

    /**
     * Get Role by ID
     *
     * @param $roleId
     * @return Role
     */
    public function find($roleId)
    {
        try {
            return $this->role->find($roleId);
        } catch (\Exception $e) {
            $this->logger->error('Role->find: ' . $e->getMessage());

            return null;
        }
    }

    /**
     * write brief description
     */
    public function all() {
        return $this->role->all();
    }

}