<?php

namespace App\Modules\Services\Operator;
use App\Modules\Repositories\Operator\OperatorTypeRepositoryInterface;
use App\Modules\Services\Service;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Psr\Log\LoggerInterface;

/**
 * Class OperatorTypeService
 * @package App\Modules\Services\Operator
 */
Class OperatorTypeService extends Service
{
    /**
     * @var OperatorTypeRepositoryInterface
     */
    protected $operatorService;

    /**
     * @var LoggerInterface
     */
    protected $logger;


    /**
     * @param OperatorTypeRepositoryInterface $operatorService
     * @param LoggerInterface $logger
     */
    public function __construct(
        OperatorTypeRepositoryInterface $operatorService,
        LoggerInterface $logger
    ) {
        $this->operatorService = $operatorService;
        $this->logger          = $logger;
    }


    /**
     * Create new OperatorService
     *
     * @param array $operatorData
     *
     * @return \App\Modules\Repositories\Operator\Operator|null
     */
    public function create(array $operatorData)
    {

        try {
            return $this->operatorService->create($operatorData);
        } catch (\Exception $e) {
            $this->logger->error('operatorService->create: '.$e->getMessage());

            return null;
        }

    }

    /**
     * Paginate all OperatorType
     *
     * @param array $filter
     *
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        if (!isset($filter['limit'])) {
            $filter = 10;
        }

        return $this->operatorService->paginate($filter);
    }

    /**
     * Get all OperatorsService
     *
     * @return Collection
     */
    public function all()
    {
        return $this->operatorService->all();
    }

    /**
     * Get a Operatorservice
     *
     * @param $operatorId
     *
     * @return Operator|null
     */
    public function find($operatorId)
    {
        try {
            return $this->operatorService->find($operatorId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update a OperatorService
     *
     * @param       $operatorId
     * @param array $operatorData
     *
     * @return bool
     */
    public function update($operatorId, array $operatorData)
    {
        try {
            $operatorType       = $this->operatorService->find($operatorId);
            $operatorType->name = $operatorData['name'];

            return $operatorType->save();
        } catch (\Exception $e) {
            $this->logger->error('operatorService->update: '.$e->getMessage());

            return false;
        }
    }

    /**
     * Delete a operatorService
     *
     * @param Id
     *
     * @return bool
     */
    public function delete($operatorId)
    {
        try {
            $operator = $this->operatorService->find($operatorId);
            $this->logger->info('Operator Service deleted successfully', [$operatorId]);

            return $operator->delete();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }


}
