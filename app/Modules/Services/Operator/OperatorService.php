<?php

namespace App\Modules\Services\Operator;

use App\Modules\Repositories\Operator\OperatorRepositoryInterface;
use App\Modules\Services\Service;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Psr\Log\LoggerInterface;

/**
 * Class OperatorService
 * @package App\Modules\Services\Operator
 */
Class OperatorService extends Service
{
    /**
     * @var OperatorRepositoryInterface
     */
    protected $operator;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Excel
     */
    protected $excel;


    /**
     * @param OperatorRepositoryInterface $operator
     * @param LoggerInterface             $logger
     */
    public function __construct(OperatorRepositoryInterface $operator, LoggerInterface $logger, Excel $excel)
    {
        $this->operator   = $operator;
        $this->logger     = $logger;
        $this->excel      = $excel;
        $this->uploadPath = config('constants.imageUploadPath');
    }

    /**
     * Create new Operator
     *
     * @param array $operatorData
     *
     * @return Operator|null
     */
    public function create(array $operatorData)
    {
        try {
            $operatorData['logo'] = '';
            if (isset($operatorData['file'])) {
                $operatorData['logo'] = $this->upload($operatorData['file'], 200, 75);
            }


            $operator = $this->operator->create($operatorData);
            $this->logger->info('Operator created successfully', $operatorData);

            return $operator;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return null;
        }
    }

    /**
     * Paginate all Operator
     *
     * @param array $filter
     *
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        if (!isset($filter['limit'])) {
            $filter['limit'] = 5;
        }

        return $this->operator->paginate($filter);
    }

    /**
     * Get all Operators
     *
     * @return Collection
     */
    public function all()
    {
        return $this->operator->all();
    }

    /**
     * Get a Operator
     *
     * @param $operatorId
     *
     * @return Operator|null
     */
    public function find($operatorId)
    {
        try {
            return $this->operator->find($operatorId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update a Operator
     *
     * @param       $operatorId
     * @param array $operatorData
     *
     * @return bool
     */
    public function update($operatorId, array $operatorData)
    {
        try {
            $operator = $this->operator->find($operatorId);

            $operatorData['logo'] = $operator->logo;

            if (isset($operatorData['file'])) {
                $operatorData['logo'] = $this->upload($operatorData['file']);

                if (!empty($operator->logo)) {
                    $this->deleteImage($operator->logo);
                }
            }



            if(!isset($operatorData['interest_radio']) || $operatorData['interest_radio']=='off')
            {
                $operatorData['interest']='';
            }

             if(!isset($operatorData['tax_radio']) || $operatorData['tax_radio']=='off')
            {
                $operatorData['tax_rate']='';
            }

            $banks=$operator->banks()->get();
            if(!empty($banks)){
                foreach($banks as $k=>$bank)
                {
                    $bank->delete();
                }
            }

            if(isset($operatorData['activated'])){
                $activated=$operatorData['activated'][0];

                foreach($operatorData['bank_name'] as $k=>$bank_name)
                {

                    if(!empty($bank_name)&& !empty($operatorData['account_number'][$k])){
                        if($k==$activated)
                        {
                            $active=1;
                        }
                        else{
                            $active=0;
                        }
                        $operator->banks()->create([
                            'name'=>$bank_name,
                            'account_number'=>$operatorData['account_number'][$k],
                            'active'=>$active
                        ]);
                    }


                }
            }


            if(!empty($operatorData['services']))
            {
                 $operator->services()->sync($operatorData['services']);
            }
            
           
            $operator = $operator->update($operatorData);
            $this->logger->info('Operator updated successfully', $operatorData);

            return $operator;
            
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Delete a operator
     *
     * @param Id
     *
     * @return bool
     */
    public function delete($operatorId)
    {
        try {
            $operator = $this->operator->find($operatorId);
            $this->logger->info('Operator deleted successfully', [$operatorId]);

            return $operator->delete();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Import excel sheet containing operator details into operators table
     *
     * @param $postData
     * @return bool
     */
    public function import($postData)
    {
        if (!is_dir(base_path() . '/public/uploads/crosswalk'))
            mkdir(base_path() . '/public/uploads/crosswalk');

        $this->uploadPath = base_path() . '/public/uploads/crosswalk';
        $fileName = $this->upload($postData['file']);

        $filePath = $this->uploadPath . '/' . $fileName;
        $reader = $reader = Excel::selectSheetsByIndex(0)->load($filePath, function ($reader) {
        });

        $results = $reader->get();

        $allData = [];
        $insertData = [];
        $allDataKeyArr = [];
        $resultKeyArr = [];

        foreach ($results as $result) {
            $allData[] = $this->__mapKeys($result, $postData['file']->getClientOriginalName());
        }

        foreach ($allData as $k => $value) {
            $resultKeyArr[] = $value['code'];
            $allDataKeyArr[$value['code']] = $value;
        }

        DB::beginTransaction();

        //now get all operators from database, and add new operators
        $allOperators = $this->operator->all();

        $operatorKeys = [];
        foreach ($allOperators as $aOperator) {
            $operatorKeys[] = $aOperator->code;
        }

        $newOperators = array_diff($resultKeyArr, $operatorKeys);
//        $existingOperators = array_diff($operatorKeys, $resultKeyArr);

        foreach ($newOperators as $iData) {
            if (!empty($iData))
                $insertData[$iData] = $allDataKeyArr[$iData];
        }

        $this->operator->insert($insertData);
        DB::commit();

        $this->logger->info('Operator imported successfully');
    }

    /**
     * This is private function and will be called to map excel column with database column
     * @param $result
     * @param $fileName
     * @return array
     */
    private function __mapKeys($result, $fileName)
    {

        $keyMap = [
            'billing_operator' => 'code',
            'billing_operator_name' => 'name'
        ];

        $data = [];

        foreach ($result as $k => $v) {
            foreach ($keyMap as $kk => $vv) {
                if (strtolower($k) == strtolower($kk) || strstr(strtolower($kk), strtolower($k))) {
                    $data[$vv] = $v;
                    continue;
                }
            }
            $data['source_file_name'] = $fileName;
        }

        return $data;
    }
}
