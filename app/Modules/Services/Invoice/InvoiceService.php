<?php
namespace App\Modules\Services\Invoice;

use App\Modules\Repositories\Invoice\InvoiceRepositoryInterface;
use App\Modules\Services\Service;
use GuzzleHttp\Psr7\Request;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Requests;

/**
 * Class InvoiceService
 * @package App\Services\Invoice
 */
class InvoiceService extends Service
{

    /**
     * @var InvoiceService
     */
    protected $invoice;
    /**
     * @var Log
     */
    protected $logger;

    /**
     * @param InvoiceRepositoryInterface $invoice
     * @param Log                        $logger
     */
    public function __construct(InvoiceRepositoryInterface $invoice, Log $logger)
    {
        $this->invoice = $invoice;
        $this->logger  = $logger;
    }

    /**
     * Create New Invoice
     *
     * @param array $invoiceData
     *
     * @return Invoice|null
     */
    public function create(array $invoiceData)
    {
        try {
            return $this->invoice->create($invoiceData);
        } catch (\Exception $e) {
            $this->logger->error('Invoice->create: '.$e->getMessage());

            return null;
        }
    }

    /**
     * Invoice Pagination
     *
     * @param array $filter
     *
     * @return Invoice
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 10;

        return $this->invoice->paginate($filter);
    }

    /**
     * Update Invoice
     *
     * @param array $invoiceId
     * @param array $invoiceData
     *
     * @return bool
     */
    public function update($invoiceId, array $invoiceData)
    {
        try {
            $invoice = $this->invoice->find($invoiceId);
            $invoiceNumber = $invoice->invoice_number;
            $invoice = $invoice->update($invoiceData);
            $this->logger->info('Invoice updated successfully', $invoiceData);

            return $invoiceNumber;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }

    }


    /**
     * Delete Invoice
     *
     * @param $invoiceId
     *
     * @return mixed
     */
    public function delete($invoiceId)
    {
        try {
            $invoice = $this->invoice->find($invoiceId);

            return $invoice->delete();
        } catch (\Exception $e) {
            $this->logger->error('Invoice->delete: '.$e->getMessage());

            return false;
        }
    }

    /**
     * Get Invoice by ID
     *
     * @param $invoiceId
     *
     * @return $invoice
     */
    public function find($incoiceId)
    {
        try {
            return $this->invoice->find($incoiceId);
        } catch (\Exception $e) {
            $this->logger->error('Post->find: '.$e->getMessage());

            return null;
        }
    }
    /**
     * Get Invoices  from invoices table
     *
     *
     *
     *
     */

    public  function invoice()
    {
        return  $this->invoice->invoice();
    }

    public  function  monthlyInvoices($code)
    {
        return $this->invoice->monthlyInvoices($code);
    }

    public function invoiceOfCompany($id)
    {
        return $this->invoice->invoiceOfCompany($id);
    }

    public  function  invoiceDetails($invoiceNumber)
    {
        return $this->invoice->invoiceDetails($invoiceNumber);
    }
    public function operatorDetails($invoiceNumber)
    {
        return $this->invoice->operatorDetails($invoiceNumber);

    }
    public function monthlyOutBoundInvoicesDetails($invoiceNumber)
    {
        return $this->invoice->monthlyOutBoundInvoicesDetails($invoiceNumber);
    }
    public  function sumCall($invoiceNumber)
    {
        return $this->invoice->sumCall($invoiceNumber);
    }

    public function selectYearMonthforInvoiceService($request)
    {
        return $this->invoice->selectYearMonthforInvoice( $request);
    }

}