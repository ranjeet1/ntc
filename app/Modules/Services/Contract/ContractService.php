<?php
namespace App\Modules\Services\Contract;

use App\Modules\Models\Contract\Contract;
use App\Modules\Repositories\Contract\ContractRepositoryInterface;
use App\Modules\Services\Service;
use GuzzleHttp\Psr7\Request;
use Illuminate\Contracts\Logging\Log;

/**
 * Class ContractService
 * @package App\Modules\Services\Contract
 */
class ContractService extends Service
{

    /**
     * @var ContractRepositoryInterface
     */
    protected $contract;
    /**
     * @var Log
     */
    protected $logger;

    /**
     * @param ContractRepositoryInterface $contract
     * @param Log                         $logger
     */
    public function __construct(ContractRepositoryInterface $contract, Log $logger)
    {
        $this->contract = $contract;
        $this->logger   = $logger;
    }


    /**
     * Create New Contract
     *
     * @param array $contractData
     *
     * @return Contract|null
     */
    public function create(array $contractData)
    {

        try {
            $contractData['terms_and_conditions'] = '';
            if (isset($contractData['file'])) {
                $contractData['terms_and_conditions'] = $this->upload($contractData['file']);
            }
            $contract = $this->contract->create($contractData);
            $this->logger->info('Contract created successfully', $contractData);

            return $contract;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return null;
        }
    }


    /**
     * Contract Pagination
     *
     * @param array $filter
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 10;

        return $this->contract->paginate($filter);
    }


    /**
     * Update Contract
     *
     * @param       $contractId
     * @param array $contractData
     *
     * @return bool
     */
    public function update($contractId, array  $contractData)
    {
        try {
            $contract = $this->contract->find($contractId);

            $contractData['terms_and_conditions'] = $contract->terms_and_conditions;

            if (isset($contractData['file'])) {
                $contractData['terms_and_conditions'] = $this->upload($contractData['file']);

                if (!empty($contract->terms_and_conditions)) {
                    $this->deleteImage($contract->terms_and_conditions);
                }
            }
            $contract = $contract->update($contractData);
            $this->logger->info('Contrcat updated successfully', $contractData);

            return $contract;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }


    /**
     *Delete Contract
     *
     * @param $contractId
     *
     * @return bool|null
     */
    public function delete($contractId)
    {
        try {
            $contract = $this->contract->find($contractId);

            return $contract->delete();
        } catch (\Exception $e) {
            $this->logger->error('Contract->delete: '.$e->getMessage());

            return false;
        }
    }

    

    /**
     * Get Contract by ID
     *
     * @param $contractId
     *
     * @return Contract|null
     */
    public function find($contractId)
    {
        try {
            return $this->contract->find($contractId);
        } catch (\Exception $e) {
            $this->logger->error('Contract->find: '.$e->getMessage());

            return null;
        }
    }

}