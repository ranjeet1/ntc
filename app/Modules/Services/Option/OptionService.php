<?php

namespace App\Modules\Services\Option;

use App\Modules\Repositories\Option\OptionRepositoryInterface;
use App\Modules\Services\Service;
use Exception;
use Psr\Log\LoggerInterface;

/**
 * Class OptionService
 * @package App\Modules\Services\Option
 */
Class OptionService extends Service
{
    /**
     * @var OptionRepositoryInterface
     */
    protected $option;


    /**
     * @var LoggerInterface
     */
    protected $logger;

    protected $uploadPath;


    /**
     * @param OptionRepositoryInterface $option
     * @param LoggerInterface $logger
     */
    public function __construct(OptionRepositoryInterface $option, LoggerInterface $logger)
    {
        $this->option = $option;
        $this->logger = $logger;
        $this->uploadPath = config('constants.imageUploadPath');
    }

    /**
     * Get a option by specified parameter.
     *
     * @param $option
     * @param string $field
     * @return Option|null
     */
    public function find($option, $field = 'id')
    {
        try {
            return $this->option->find($option, $field);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return null;
        }
    }

    /**
     * Delete a Option
     *
     * @param $optionId
     * @return bool
     */
    public function delete($optionId)
    {
        try {
            $option = $this->find($optionId);
            $this->logger->info('Option deleted successfully.', [$optionId]);

            return $option->delete();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Add or update option(s) in storage.
     *
     * @param $optionData
     * @return bool
     */
    public function addOptions($optionData)
    {
        try {
            foreach ($optionData as $key => $option) {
                $newArray = array(
                    'name' => $key,
                    'value' => $option
                );
                $option = $this->find($key, 'name');
                if ($option) {
                    $option->update($newArray);
                } else {
                    $this->option->create($newArray);
                }
            }
            foreach ($_FILES as $fileInputName => $fileArray) {
                if ($_FILES[$fileInputName]['name'] == '')
                    continue;
                $newArray = array(
                    'name' => $fileInputName,
                    'value' => $this->upload($optionData[$fileInputName], 204, 40)
                );
                $option = $this->find($fileInputName, 'name');
                if ($option) {
                    $option->update($newArray);
                    $this->deleteImage($option->value);
                } else {
                    $this->option->create($newArray);
                }

            }

            return true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Get all or specified option(s) from storage.
     *
     * @param null $name
     * @return \stdClass
     */
    public function getOptions($name = NULL)
    {
        if ($name) {
            $option = $this->find($name, 'name');

            return $option ? $option->value : '';
        } else {
            $options = new \stdClass();

            $allOptions = $this->option->all();
            foreach ($allOptions as $option) {
                $options->{$option->name} = $option->value;
            }

            return $options;
        }

    }


}
