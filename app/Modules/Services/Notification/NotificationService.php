<?php
namespace App\Modules\Services\Notification;

use App\Modules\Repositories\Notification\NotificationRepositoryInterface;
use App\Modules\Services\Service;


/**
 * Class NotificationService
 * @package App\Modules\Services
 */
class NotificationService extends Service
{
    /**
     * @var UserRepositoryInterface
     */
    protected $notification;

    /**
     * @param notificationRepositoryInterface $notification
     */
    public function __construct(NotificationRepositoryInterface $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Create a new notification
     *
     * @param array $data
     *
     * @return true
     */
    public function create(array $data)
    {

        $notification = $this->notification->create($data);
        return true;
    }


}