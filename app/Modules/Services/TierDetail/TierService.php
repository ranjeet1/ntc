<?php
namespace App\Modules\Services\TierDetail;

use App\Modules\Models\Tier\Tier;
use App\Modules\Repositories\TierDetail\TierRepositoryInterface;
use App\Modules\Services\Service;
use Illuminate\Contracts\Logging\Log;

/**
 * Class TierService
 * @package App\Services\Post
 */
class TierService extends Service
{

    /**
     * @var TierRepositoryInterface
     */
    protected $tier;
    /**
     * @var Log
     */
    protected $logger;

    /**
     * @param TierRepositoryInterface $tier
     * @param Log $logger
     */
    public function __construct(TierRepositoryInterface $tier, Log $logger)
    {
        $this->tier = $tier;
        $this->logger = $logger;
    }

    /**
     * Get all tiers
     *
     * @return tier
     */
    public function all()
    {
        return $this->tier->all();
    }

    /**
     * Create a new Tier
     * @param array $tierData
     * @return \App\Models\Tier\Tier|null
     */
    public function create(array $tierData)
    {
        try {
            return $this->tier->create($tierData);
        } catch (\Exception $e) {
            $this->logger->error('Tier->create: ' . $e->getMessage());

            return null;
        }
    }

    /**
     * Tier Pagination
     * @param array $filter
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 50;

        return $this->tier->paginate($filter);
    }

    /**
     * Update the specific Tier
     * @param $tierId
     * @param array $tierData
     * @return bool
     */
    public function update($tierId, array $tierData)
    {
        try {
            $tier = $this->tier->find($tierId);
            $tier->tier_name = $tierData['tier_name'];
            $tier->tier_code = $tierData['tier_code'];

            return $tier->save();
        } catch (\Exception $e) {
            $this->logger->error('Tier->update: ' . $e->getMessage());

            return false;
        }
    }

    /**
     * Delete the specified Tier
     * @param $tierId
     * @return bool
     */
    public function delete($tierId)
    {
        try {
            $tier = $this->tier->find($tierId);

            return $tier->delete();
        } catch (\Exception $e) {
            $this->logger->error('Tier->delete: ' . $e->getMessage());

            return false;
        }
    }

    /**
     * Get the particular Tier by id
     * @param $tierId
     * @return \App\Models\Tier\Tier|null
     */
    public function find($tierId)
    {
        try {
            return $this->tier->find($tierId);
        } catch (\Exception $e) {
            $this->logger->error('Tier->find: ' . $e->getMessage());

            return null;
        }
    }

}