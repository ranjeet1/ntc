<?php

namespace App\Modules\Services\RateProfile;

use App\Modules\Models\RateProfile\RateProfile;
use App\Modules\Repositories\RateProfile\RateProfileRepositoryInterface;
use App\Modules\Services\Contract\ContractService;
use App\Modules\Services\Service;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Psr\Log\LoggerInterface;

/**
 * Class RateProfileService
 * @package App\Modules\Services\RateProfile
 */
Class RateProfileService extends Service
{
    /**
     * @var RateProfileRepositoryInterface
     */
    protected $rateProfile;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    protected $contract;


    /**
     * @param RateProfileRepositoryInterface $rateProfile
     * @param LoggerInterface $logger
     * @param ContractService $contract
     */
    public function __construct(RateProfileRepositoryInterface $rateProfile, LoggerInterface $logger, ContractService $contract)
    {
        $this->rateProfile = $rateProfile;
        $this->logger = $logger;
        $this->contract = $contract;
    }

    /**
     * Create new RateProfile
     *
     * @param array $rateProfileData
     * @param $contractId
     * @return RateProfile|null
     */
    public function create($contractId, array $rateProfileData)
    {
        try {
            $contract = $this->contract->find($contractId);

            if(!$contract) {
                $this->logger->info("Contract with specified $contractId not found.", '');

                return false;
            }

            $insertData = array();
            $core = array(
                'tier_detail_id' => $rateProfileData['tierDetailId'],
                'contract_id' => $contractId,
                'rate_type' => $rateProfileData['rateType'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            $mantle = array();

            if ( $rateProfileData['rateType'] == 'flat' ) {
                $mantle['ceiling'] = 0;
                $mantle['rate'] = 0;
                $mantle['amount'] = $rateProfileData['flatAmount'];
                $mantle['hybrid_type'] = 'NULL';
                $insertData[] = array_merge($core, $mantle);

            } else if( $rateProfileData['rateType'] == 'fixed' ) {
                $mantle['ceiling'] = 0;
                $mantle['rate'] = $rateProfileData['fixedRate'];
                $mantle['amount'] = 0;
                $mantle['hybrid_type'] = 'NULL';
                $insertData[] = array_merge($core, $mantle);

            } else if ($rateProfileData['rateType'] == 'hybrid' || $rateProfileData['rateType'] == 'incremental') {

                foreach($rateProfileData['hybrid'] as $key => $pure) {

                    if($rateProfileData['rateType'] == 'incremental' && $pure['type'] == 'flat')
                        continue;

                    $mantle['ceiling'] = $pure['ceiling'] == 'Infinity' ? -1 : $pure['ceiling'];
                    $mantle['rate'] = $pure['type'] == 'incremental' ? $pure['value'] : 0;
                    $mantle['amount'] = $pure['type'] == 'flat' ? $pure['value'] : 0;
                    $mantle['hybrid_type'] = $pure['type'];
                    $insertData[] = array_merge($core, $mantle);
                }

            } else {
                return false;
            }


            $result = $this->rateProfile->insert($insertData);


//                dd($insertData);
//            $rateProfile = $this->rateProfile->create($rateProfileData);
//            $rateProfile = $contract->rateProfiles()->create($rateProfileData);
            $this->logger->info('Rate profile created successfully', $rateProfileData);

            return $result;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return null;
        }
    }

    /**
     * Paginate all RateProfile
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 15;

        return $this->rateProfile->paginate($filter);
    }

    /**
     * Get all RateProfiles
     *
     * @return Collection
     */
    public function all()
    {
        return $this->rateProfile->all();
    }

    /**
     * Get a RateProfile
     *
     * @param $rateProfileId
     * @return RateProfile|null
     */
    public function find($rateProfileId)
    {
        try {
            return $this->rateProfile->find($rateProfileId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update a RateProfile
     *
     * @param       $rateProfileId
     * @param array $rateProfileData
     * @return bool
     */
    public function update($rateProfileId, array $rateProfileData)
    {
        try {
            $rateProfile = $this->rateProfile->find($rateProfileId);
            $rateProfile = $rateProfile->update($rateProfileData);
            $this->logger->info('Rate profile created successfully', $rateProfileData);

            return $rateProfile;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Delete a rate profile
     *
     * @param $rateProfileId
     * @return bool
     */
    public function delete($rateProfileId)
    {
        try {
            $rateProfile = $this->rateProfile->find($rateProfileId);

            return $rateProfile->delete();
        } catch (Exception $e) {
            return false;
        }
    }
}
