<?php

namespace App\Modules\Services\CurrencyExchange;

use App\Modules\Repositories\CurrencyExchange\CurrencyExchangeRepositoryInterface;
use App\Modules\Services\Service;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Excel;
use Psr\Log\LoggerInterface;

/**
 * Class CurrencyExchangeService
 * @package App\Modules\Services\Imp
 */
Class CurrencyExchangeService extends Service
{
    /**
     * @var CurrencyExchangeRepositoryInterface
     */
    protected $currencyExchange;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Excel
     */
    protected $excel;


    /**
     * @param CurrencyExchangeRepositoryInterface $currencyExchange
     * @param LoggerInterface $logger
     */
    public function __construct(CurrencyExchangeRepositoryInterface $currencyExchange, LoggerInterface $logger, Excel $excel)
    {
        $this->currencyExchange = $currencyExchange;
        $this->logger = $logger;
        $this->excel = $excel;

    }

    /**
     * Get all ExchangeRates
     *
     * @return Collection
     */
    public function all()
    {
        return $this->currencyExchange->all();
    }


    /**
     * Import excel sheet containing rates details into currency_exchange
     * @param $postData
     * @return mixed
     */
    public function import($postData)
    {
        $this->uploadPath = import_path('currency');
        $source_file_name = $this->upload($postData['file']);
        $filePath = $this->uploadPath . '/' . $source_file_name;
        $results = $this->excel->load($filePath)->get();
        $data = array();


        foreach ($results as $rows) {

            $data[] = [
                'Date' => $rows->date,
                'IND_S' => $rows->indian_rupee_sell,
                'USD_S' => $rows->ussell,
                'EUR_S' => $rows->euro_sell,
                'AUD_S' => $rows->aus_sell,
                'GBP_S' => $rows->st_pound_sell,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];

        }


        $insertData = [];
        $resultKeyArr = [];

        $allDataKeyArr = [];
        foreach ($data as $k => $value) {
            $resultKeyArr[] = $value['Date'];
            $allDataKeyArr[$value['Date']] = $value;
        }


        $allRates = $this->all();

        $keys = [];
        foreach ($allRates as $rate) {
            $keys[] = $rate->Date;
        }

        $newRates = [];
        $existingRates = [];

        $newRates = array_diff($resultKeyArr, $keys);
        $existingRates = array_diff($keys, $resultKeyArr);


        foreach ($newRates as $iData) {
            $insertData[] = $allDataKeyArr[$iData];
        }


        // $updateData = [];
        // foreach($existingRates as $k => $eRate){
        //     Tier::where('Date', '=', $k)->update($allDataKeyArr[$eRate]);
        // }

        $this->logger->info('Operator imported successfully', $data);
        return $this->currencyExchange->insert($insertData);

    }

    /**
     * Calculate average of the currency
     * @param $date1
     * @param $date2
     * @param $currency
     * @return mixed
     */
    public function average($date1, $date2, $currency)
    {

        return $this->currencyExchange->average($date1, $date2, $currency);
    }
}
