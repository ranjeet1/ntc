<?php
namespace App\Modules\Services\User;

use App\Modules\Repositories\User\UserRepositoryInterface;
use App\Modules\Services\Role\RoleService;
use App\Modules\Services\Service;
use Exception;
use App\Modules\Models\User\User;
use Mail;
use Psr\Log\LoggerInterface;

/**
 * Class UserService
 * @package App\Modules\Services
 */
class UserService extends Service
{
    /**
     * @var UserRepositoryInterface
     */
    protected $user;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var RoleService
     */
    protected $role;

    /**
     * @param UserRepositoryInterface $user
     * @param LoggerInterface $logger
     */
    public function __construct(UserRepositoryInterface $user, LoggerInterface $logger, RoleService $role)
    {
        $this->logger = $logger;
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * Create a new user
     * @param array $userData
     * @return User|null
     */
    public function create(array $userData)
    {

        $this->uploadPath = base_path() . '/public/uploads/content';
//        try {
            if (!isset($userData['password']))
                $userData['password'] = bcrypt('password');

            if (isset($userData['photo']) && !empty($userData['photo']))
                $userData['photo'] = $this->upload($userData['photo']);

            if (isset($userData['idf']) && !empty($userData['idf']))
                $userData['identification_document'] = $this->upload($userData['idf']);


            $userData['confirmation_code'] = md5(str_random(64) . time() * 64);
            $userData['confirmed'] = 0;

            $user = $this->user->create($userData);

            if( isset($userData['role']) )
                $user->attachRole( $userData['role'] );

            return $user;
//        } catch (Exception $e) {
//
//            return null;
//        }

    }

    /**
     * Paginate all users with filter
     * @param array $filter
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function paginate(array $filter = [])
    {
        if (!isset($filter['limit']))
            $filter['limit'] = 5;

        return $this->user->paginate($filter);
    }

    /**
     * Get all users
     * @return \Illuminate\Database\Eloquent\Collection
     */

    public function all()
    {
        return $this->user->all();
    }

    /**
     * Get the user with particular id or any specified field
     * @param $user
     * @param string $field
     * @return User|null
     */

    public function find($user, $field = 'id')
    {
        try {
            return $this->user->find($user, $field);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return null;
        }


    }

    /**
     * Get a user with given userId
     * @param $userId
     * @param array $userData
     * @param $notificationService
     * @return User|bool|int
     */
    public function update($userId, array $userData, $notificationService)
    {
        //try {
        $this->uploadPath = base_path() . '/public/uploads/content';
        $user = $this->user->find($userId);

        if (isset($userData['photo']) && !empty($userData['photo'])) {
            $userData['photo'] = $this->upload($userData['photo']);

            if (!empty($user->photo)) {
                $this->deleteImage($user->photo);
            }
        } else {
            $userData['photo'] = $user->photo;
        }

        $userData['identification_document'] = $user->identification_document;

        if (isset($userData['idf']) && !empty($userData['idf'])) {

            $userData['identification_document'] = $this->upload($userData['idf']);
            if (!empty($user->identification_document)) {
                $this->deleteImage($user->identification_document);
            }
        }

        $user->update($userData);

        //now get role object which user is assigned to

        if( isset($userData['role']) ) {
            $roleId = $userData['role'];
            $user->roles()->detach();
            $user->attachRole($roleId);
        }

        //created notification data
        $currentUser = \Auth::user();
        $notificationData = array('created_by' => $currentUser->id,
            'created_for' => $userId,
            'message' => 'Your profile has been updated by ' . $currentUser->first_name,
            'link' => 'user/' . $userId,
            'status' => 0);


        $notification = $notificationService->create($notificationData);

        return $user;

    }

    /**
     * Delete a user
     * @param $userId
     * @return bool|null
     */
    public function delete($userId)
    {
        try {
            $user = $this->user->find($userId);

            return $user->delete();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Get the select fields only
     * @param $type
     * @param string $fields
     * @return mixed
     */
    public function get($type, $fields = '')
    {
        return $this->user->get($type, $fields);
    }
}