<?php

namespace App\Modules\Services\OperatorService;

use App\Modules\Models\OperatorService\OperatorService;
use App\Modules\Repositories\OperatorService\OperatorServiceRepositoryInterface;
use App\Modules\Services\Service;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Psr\Log\LoggerInterface;

/**
 * Class OperatorServiceService
 * @package App\Modules\Services\OperatorService
 */
Class OperatorServiceService extends Service
{
    /**
     * @var OperatorServiceRepositoryInterface
     */
    protected $operatorService;

    /**
     * @var LoggerInterface
     */
    protected $logger;


    /**
     * @param OperatorServiceRepositoryInterface $operatorService
     * @param LoggerInterface $logger
     */
    public function __construct(OperatorServiceRepositoryInterface $operatorService, LoggerInterface $logger)
    {
        $this->operatorService = $operatorService;
        $this->logger  = $logger;
    }

    /**
     * Create new OperatorService
     *
     * @param array $operatorServiceData
     * @return OperatorService|null
     */
    public function create(array $operatorServiceData)
    {
        try {
            $operatorService = $this->operatorService->create($operatorServiceData);
            $this->logger->info('OperatorService created successfully', $operatorServiceData);

            return $operatorService;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return null;
        }
    }

    /**
     * Paginate all OperatorService
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter = [])
    {
        $filter['limit'] = 5;

        return $this->operatorService->paginate($filter);
    }

    /**
     * Get all OperatorServices
     *
     * @return Collection
     */
    public function all()
    {
        return $this->operatorService->all();
    }

    /**
     * Get a OperatorService
     *
     * @param $operatorServiceId
     * @return OperatorService|null
     */
    public function find($operatorServiceId)
    {
        try {
            return $this->operatorService->find($operatorServiceId);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Update a OperatorService
     *
     * @param       $operatorServiceId
     * @param array $operatorServiceData
     * @return bool
     */
    public function update($operatorServiceId, array $operatorServiceData)
    {
        try {
            $operatorService = $this->operatorService->find($operatorServiceId);
            $operatorService = $operatorService->update(Data);
            $this->logger->info(' created successfully', Data);

            return $operatorService;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return false;
        }
    }

    /**
     * Delete a OperatorService
     *
     * @param $operatorServiceId
     * @return bool
     */
    public function delete($operatorServiceId)
    {
        try {
             $operatorService= $this->operatorService->find($operatorServiceId);

            return $operatorService->delete();
        } catch (Exception $e) {
            return false;
        }
    }
}
