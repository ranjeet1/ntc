<?php
namespace App\Modules\Repositories\Operator;

use App\Modules\Models\Operator\Operator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface OperatorRepositoryInterface
 * @package App\Modules\Repositories\Operator
 */
interface OperatorRepositoryInterface
{
    /**
     * Create a operator
     *
     * @param array $operatorData
     * @return Operator
     */
    public function create(array  $operatorData);

    /**
     * Pagination for all operator with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter);

    /**
     * Get all operator
     *
     * @return Collection
     */
    public function all();

    /**
     * Get a operator
     *
     * @param  $operatorId
     * @return Operator
     */
    public function find( $operatorId);

    /**
     * Create multiple operators
     *
     * @param $operatorData
     * @return bool
     */
    public function  insert($operatorData);
}
