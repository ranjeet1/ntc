<?php

namespace App\Modules\Repositories\Operator;

use App\Modules\Models\Operator\OperatorType;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class OperatorTypeRepository
 *
 * @package App\Modules\Repositories\Operator
 */
class OperatorTypeRepository implements OperatorTypeRepositoryInterface
{
    /**
     * @var Operator
     */
    protected $operatorType;

    /**
     * @param OperatorType $operatorType
     */
    public function __construct(OperatorType $operatorType)
    {
        $this->operatorType = $operatorType;
    }

    /**
     * Create a new OperatorType
     *
     * @param array $operatorData
     * @return Operator
     */
    public function create(array $operatorData)
    {
        return $this->operatorType->create($operatorData);
    }

    /**
     * Pagination for all operator with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter)
    {
        return $this->operatorType->orderBy('id','DESC')->paginate($filter['limit']);
    }

    /**
     * Get all operatorType
     *
     * @return Collection
     */
    public function all()
    {
        return $this->operatorType->all();
    }

    /**
     * Get a operator
     *
     * @param $operatorId
     * @return Operator
     */
    public function find($operatorId)
    {
        return $this->operatorType->findOrFail($operatorId);
    }

    /**
     * Create multiple operators
     *
     * @param $operatorData
     * @return bool
     */
    public function insert($operatorData)
    {
        return $this->operatorType->insert($operatorData);
    }

}

