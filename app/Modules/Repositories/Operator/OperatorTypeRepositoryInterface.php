<?php
namespace App\Modules\Repositories\Operator;

use App\Modules\Models\Operator\OperatorType;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface OperatorTypeRepositoryInterface
 * @package App\Modules\Repositories\Operator
 */
interface OperatorTypeRepositoryInterface
{
    /**
     * Create a operator
     *
     * @param array $operatorTypeData
     * @return Operator
     */
    public function create(array  $operatorTypeData);

    /**
     * Pagination for all operator with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter);

    /**
     * Get all operator
     *
     * @return Collection
     */
    public function all();

    /**
     * Get a operator
     *
     * @param  $operatorId
     * @return Operator
     */
    public function find($operatorId);

    /**
     * Create multiple operators
     *
     * @param $operatorTypeData
     * @return bool
     */
    public function  insert($operatorTypeData);
}
