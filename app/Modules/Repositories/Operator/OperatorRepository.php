<?php

namespace App\Modules\Repositories\Operator;

use App\Modules\Models\Operator\Operator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class OperatorRepository
 *
 * @package App\Modules\Repositories\Operator
 */
class OperatorRepository implements OperatorRepositoryInterface
{
    /**
     * @var Operator
     */
    protected $operator;

    /**
     * @param Operator $operator
     */
    public function __construct(Operator $operator)
    {
        $this->operator = $operator;
    }


    /**
     * Create a new operator
     *
     * @param array $operatorData
     *
     * @return static
     */
    public function create(array $operatorData)
    {
        return $this->operator->create($operatorData);
    }

    /**
     * Pagination for all operator with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter)
    {
        return $this->operator->paginate($filter['limit']);
    }

    /**
     * Get all operator
     *
     * @return Collection
     */
    public function all()
    {
        return $this->operator->all();
    }

    /**
     * Get a operator
     *
     * @param $operatorId
     * @return Operator
     */
    public function find($operatorId)
    {
        return $this->operator->findOrFail($operatorId);
    }

    /**
     * Create multiple operators
     *
     * @param $operatorData
     * @return bool
     */
    public function insert($operatorData)
    {
        return $this->operator->insert($operatorData);
    }

}

