<?php

namespace App\Modules\Repositories\Service;

use App\Modules\Models\Service\ServiceModel;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ServiceRepository
 *
 * @package App\Modules\Repositories\Service
 */
class ServiceRepository implements ServiceRepositoryInterface
{
    /**
     * @var $service
     */
    protected $service;

    /**
     * @param ServiceModel $service
     */
    public function __construct(ServiceModel $service)
    {
        $this->service = $service;
    }

    /**
     * Create a new
     *
     * @param array $serviceData
     * @return Service
     */
    public function create(array $serviceData)
    {
        return $this->service->create($serviceData);
    }

    /**
     * Pagination for all  with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter)
    {
        return $this->service->paginate($filter['limit']);
    }

    /**
     * Get all
     *
     * @return Collection
     */
    public function all()
    {
        return $this->service->all();
    }

    /**
     * Get a
     *
     * @param $serviceId
     * @return Service
     */
    public function find($serviceId)
    {
        return $this->service->findOrFail($serviceId);
    }


}

