<?php
namespace App\Modules\Repositories\Service;

use App\Modules\Models\Service\ServiceModel;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface ServiceRepositoryInterface
 * @package App\Modules\Repositories\Service
 */
interface ServiceRepositoryInterface
{
    /**
     * Create a Service
     *
     * @param array $serviceData
     * @return Service
     */
    public function create(array  $serviceData);

    /**
     * Pagination for all  with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter);

    /**
     * Get all
     *
     * @return Collection
     */
    public function all();

    /**
     * @param $serviceId
     * @return mixed
     */
    public function find( $serviceId);
}
