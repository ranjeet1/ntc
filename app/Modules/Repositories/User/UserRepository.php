<?php
namespace App\Modules\Repositories\User;

use App\Modules\Models\User\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class UserRepository
 * @package App\Modules\Repositories\User
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Create a user
     *
     * @param array $userData
     * @return User
     */
    public function create(array $userData)
    {
        return $this->user->create($userData);
    }

    /**
     * Pagination for all users with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter)
    {
        $users = $this->user;
        if (isset($filter['type']))
            $users = $users->ofType($filter['type']);

        return $users->latest()->paginate($filter['limit']);
    }

    /**
     * Get all users
     *
     * @return collection
     */
    public function all()
    {
        return $this->user->all();
    }

    /**
     * Get the user with given userId
     *
     * @param $userId
     * @return User
     */
    public function find($userId)
    {
        return $this->user->findOrFail($userId);
    }

    /**
     * Get the user with selected type and only select selected fields
     *
     * @param null $type
     * @param string $fields
     * @return mixed
     */
    public function get($type = NULL, $fields = '') {

        $users = $this->user;
        if ( !empty($type) )
            $users = $users->ofType($type);

        if( !empty($fields) )
            return $users->get($fields);

        return $users->get();
    }
}