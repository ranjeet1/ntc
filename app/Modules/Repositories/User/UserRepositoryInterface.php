<?php
namespace App\Modules\Repositories\User;

use App\Modules\Models\User\User;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface UserRepositoryInterface
 * @package App\Modules\Repositories\User
 */
interface UserRepositoryInterface
{
    /**
     * Create a user
     *
     * @param array $userData
     * @return User
     */
    public function create(array $userData);

    /**
     * Pagination for users with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter);

    /**
     * Get all users
     *
     * @return Collection
     */
    public function all();

    /**
     * Get a user with given userId
     *
     * @param $userId
     * @return User
     */
    public function find($userId);

    /**
     * @return mixed
     */
    public function get($type = NULL, $fields = '');
}

