<?php

namespace App\Modules\Repositories\RateProfile;

use App\Modules\Models\RateProfile\RateProfile;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class RateProfileRepository
 *
 * @package App\Modules\Repositories\RateProfile
 */
class RateProfileRepository implements RateProfileRepositoryInterface
{
    /**
     * @var RateProfile
     */
    protected $rateProfile;

    /**
     * @param RateProfile $rateProfile
     */
    public function __construct(RateProfile $rateProfile)
    {
        $this->rateProfile = $rateProfile;
    }

    /**
     * Create a new rate profile
     *
     * @param array $rateProfileData
     * @return RateProfile
     */
    public function create(array $rateProfileData)
    {
        return $this->rateProfile->create($rateProfileData);
    }

    /**
     * Pagination for all rate profile with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter)
    {
        return $this->rateProfile->paginate($filter['limit']);
    }

    /**
     * Get all rate profile
     *
     * @return Collection
     */
    public function all()
    {
        return $this->rateProfile->all();
    }

    /**
     * Get a rate profile
     *
     * @param $rateProfileId
     * @return RateProfile
     */
    public function find($rateProfileId)
    {
        return $this->rateProfile->findOrFail($rateProfileId);
    }

    /**
     * Create multiple rateProfiles
     *
     * @param $rateProfileData
     * @return bool
     */
    public function insert($rateProfileData)
    {
        return $this->rateProfile->insert($rateProfileData);
    }

}

