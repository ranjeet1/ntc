<?php
namespace App\Modules\Repositories\RateProfile;

use App\Modules\Models\RateProfile\RateProfile;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface RateProfileRepositoryInterface
 * @package App\Modules\Repositories\RateProfile
 */
interface RateProfileRepositoryInterface 
{
    /**
     * Create a rate profile
     *
     * @param array $rateProfileData
     * @return RateProfile
     */
    public function create(array  $rateProfileData);

    /**
     * Pagination for all rate profile with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter);

    /**
     * Get all rate profile
     *
     * @return Collection
     */
    public function all();

    /**
     * Get a rate profile
     *
     * @param  $rateProfileId
     * @return RateProfile
     */
    public function find( $rateProfileId);

    /**
     * Create multiple rate profiles
     *
     * @param $rateProfileData
     * @return bool
     */
    public function insert($rateProfileData);
}
