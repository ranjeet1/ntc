<?php
namespace App\Modules\Repositories\TierDetail;

use App\Models\Tier\Tier;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface TierRepositoryInterface
 * @package App\Repository
 */
interface TierRepositoryInterface
{

    /**
     * Create New Tier
     *
     * @param array $tierData
     * @return Tier
     */
    public function create(array $tierData);

    /**
     * Tier Pagination
     *
     * @param array $filter
     * @return collection
     */
    public function paginate(array $filter);

    /**
     * Get Tier by ID
     * @param $tier_id
     * @return Tier
     */
    public function find($tier_id);
}
