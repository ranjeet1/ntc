<?php
namespace App\Modules\Repositories\TierDetail;

use App\Modules\Models\Tier\Tier;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class TierRepository
 * @package App\Repository
 */
class TierRepository implements TierRepositoryInterface
{

    /**
     * @var Tier
     */
    protected $tier;

    /**
     * @param Tier $tier
     */
    public function __construct(Tier $tier)
    {
        $this->tier = $tier;
    }

    /**
     * Get all tiers
     *
     * @return tier
     */
    public function all()
    {
        return $this->tier->all();
    }

    /**
     * Create New tier
     *
     * @param array $tierData
     * @return tier|null
     */
    public function create(array $tierData)
    {
        return $this->tier->create($tierData);
    }

    /**
     * tier Pagination
     *
     * @param array $filter
     * @return collection
     */
    public function paginate(array $filter)
    {
        return $this->tier->paginate($filter['limit']);
    }

    /**
     * Get tier by ID
     *
     * @param $tier_id
     * @return tier
     */
    public function find($tier_id)
    {
        return $this->tier->findOrFail($tier_id);
    }
}