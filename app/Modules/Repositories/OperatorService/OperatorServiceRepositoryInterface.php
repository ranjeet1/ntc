<?php
namespace App\Modules\Repositories\OperatorService;

use App\Modules\Models\OperatorService\OperatorService;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface OperatorServiceRepositoryInterface
 * @package App\Modules\Repositories\OperatorService
 */
interface OperatorServiceRepositoryInterface
{
    /**
     * Create a OperatorService
     *
     * @param array $operatorService
     * @return OperatorService
     */
    public function create(array  $operatorService);

    /**
     * Pagination for all OperatorService with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter);

    /**
     * Get all OperatorService
     *
     * @return Collection
     */
    public function all();

    /**
     * Get a OperatorService
     *
     * @param  $operatorServiceId
     * @return OperatorService
     */
    public function find( $operatorServiceId);
}
