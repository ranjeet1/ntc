<?php

namespace App\Modules\Repositories\OperatorService;

use App\Modules\Models\OperatorService\OperatorService;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class OperatorServiceRepository
 *
 * @package App\Modules\Repositories\OperatorService
 */
class OperatorServiceRepository implements OperatorServiceRepositoryInterface
{
    /**
     * @var OperatorService
     */
    protected $operatorService;

    /**
     * @param OperatorService $operatorService
     */
    public function __construct(OperatorService $operatorService)
    {
        $this->operatorService = $operatorService;
    }

    /**
     * Create a new OperatorService
     *
     * @param array $operatorServiceData
     * @return OperatorService
     */
    public function create(array $operatorServiceData)
    {
        return $this->operatorService->create($operatorServiceData);
    }

    /**
     * Pagination for all OperatorService with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter)
    {
        return $this->operatorService->paginate($filter['limit']);
    }

    /**
     * Get all OperatorService
     *
     * @return Collection
     */
    public function all()
    {
        return $this->operatorService->all();
    }

    /**
     * Get a OperatorService
     *
     * @param $operatorServiceId
     * @return OperatorService
     */
    public function find($operatorServiceId)
    {
        return $this->operatorService->findOrFail($operatorServiceId);
    }

}

