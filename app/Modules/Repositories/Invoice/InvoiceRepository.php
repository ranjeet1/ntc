<?php

namespace App\Modules\Repositories\Invoice;

use App\Modules\Models\Invoice\Invoice;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;


class InvoiceRepository implements InvoiceRepositoryInterface
{

    protected $invoice;

    public function __construct(Invoice $invoice)
    {

        $this->invoice = $invoice;
    }

    /**
     * Create a new invoice
     *
     * @param array $invoiceData
     *
     * @return $invoice
     */
    public function create(array $invoiceData)
    {
        return $this->invoice->create($invoiceData);
    }

    /**
     * Pagination for all invoice with filter
     *
     * @param array $filter
     *
     * @return Collection
     */
    public function paginate(array $filter)
    {
        return $this->invoice->latest()->paginate($filter['limit']);
    }

    /**
     * Get all operator
     *
     * @return Collection
     */
    public function all()
    {
        return $this->invoice->all();
    }

    /**
     * Get a invoice
     *
     * @param $invoiceId
     *
     * @return @operator
     */
    public function find($invoiceId)
    {
        return $this->invoice->find($invoiceId);
    }

    /**
     * Create multiple Inoice
     *
     * @param $invoiceData
     *
     * @return bool
     */
    public function insert($invoiceData)
    {
        return $this->invoice->insert($invoiceData);
    }

    public function invoice()
    {
        $invoices = DB::table('operators')
                      ->join('invoices', 'operators.id', '=', 'invoices.operator_id')
                      ->select('operators.id as operatorId', 'operators.*', 'invoices.*')
                      ->paginate(10);

        return $invoices;
    }

    public function monthlyInvoices($code)
    {
        $monthlyInvoices = DB::table('invoices')
                             ->join('operators', 'operators.id', '=', 'invoices.operator_id')
                             ->join('data_final_amount', 'data_final_amount.billing_operator', '=', 'operators.code')
                             ->where('operators.id', $code)
                             ->select('data_final_amount.*', 'operators.code', 'invoices.operator_id')
                             ->get();

        return $monthlyInvoices;

    }


    public function invoiceOfCompany($id)
    {

        //  $companyInvoices = $this->invoice->where('operator_id', $id)->with('operators')->get();
        $companyName = DB::table('invoices')
                         ->join('operators', 'invoices.operator_id', '=', 'operators.id')
                         ->where('invoices.operator_id', $id)->first();

        return $companyName;
    }

    public function invoiceDetails($invoiceNumber)
    {

        $invoicesDetail = $this->invoice->where('invoice_number', $invoiceNumber)->first();
        //operatorId from invoices table
        $operatorIdFromInvoices = $invoicesDetail->operator_id;
        //match operator_id of incoices with id of operators table
        $operatorIdFromInvoicesTable = DB::table('operators')->where('id', $operatorIdFromInvoices)->first();
        //take operator code of corresponding id
        $operatorCode = $operatorIdFromInvoicesTable->code;
        //match operators code   with billing_operator of data_final_amount table
        $matchOperatorCodeWithDataFinalTableBillingOperator = DB::table('data_final_amount')
                                                                ->where('billing_operator', $operatorCode)->first();

        return $matchOperatorCodeWithDataFinalTableBillingOperator;
    }

    public function operatorDetails($invoiceNumber)
    {
        $operatorInfo    = DB::table('invoices')->where('invoice_number', $invoiceNumber)->first();
        $operatorId      = $operatorInfo->operator_id;
        $operatorDetails = DB::table('operators')->where('operators.id', $operatorId)->first();

        return $operatorDetails;
    }

    public function monthlyOutBoundInvoicesDetails($invoiceNumber)
    {
        $invoiceNumber = DB::table('invoices')->where('invoice_number', $invoiceNumber)->first();
        $invoiceId     = $invoiceNumber->id;
        $operatorId    = $invoiceNumber->operator_id;


        $monthlyOutBoundInvoicesDetails = DB::table('invoice_details')->where('invoice_id', $invoiceId)->get();

        return $monthlyOutBoundInvoicesDetails;
    }

    public function sumCall($invoiceNumber)
    {

        $invoiceNumber = DB::table('invoices')->where('invoice_number',$invoiceNumber)->first();
        $invoiceId = $invoiceNumber->id;
        $operatorId = $invoiceNumber->operator_id;
        $monthlyOutBoundInvoicesDetails = DB::table('invoice_details')->where('invoice_id',$invoiceId)->get();
        $sumCallCount = DB::table('invoice_details')->where('invoice_id',$invoiceId)->sum('call_count');
        return $sumCallCount;

    }


    public function selectYearMonthforInvoice(Request $request)
    {
        $year  = $request->get('year');
        $month = $request->get('month');

        $invoices = $this->invoice->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)
                                  ->paginate();
        return $invoices;
    }


}

