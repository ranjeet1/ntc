<?php
namespace App\Modules\Repositories\Invoice;

use App\Modules\Models\Invoice\InvoiceDetail;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface InvoiceRepositoryInterface
 * @package App\Modules\Repositories\Invoice
 */
interface InvoiceRepositoryInterface
{
    /**
     * Create a Invoice
     *
     * @param array $invoiceData
     * @return invoice
     */
    public function create(array  $invoiceData);

    /**
     * Pagination for all Invoice with filter
     *
     * @param array $filter
     * @return Collection
     */
    public function paginate(array $filter);

    /**
     * Get all Invoice
     *
     * @return Collection
     */
    public function all();

    /**
     * Get a Invoce
     *
     * @param  invoiceId
     * @return Invoice
     */
    public function find($invoiceId);

    /**
     * Create multiple Invoice
     *
     * @param $invoiceData
     * @return bool
     */
    public function  insert($invoiceData);


}
