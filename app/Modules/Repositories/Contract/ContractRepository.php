<?php
namespace App\Modules\Repositories\Contract;

use App\Modules\Models\Contract\Contract;
use Illuminate\Database\Eloquent\Collection;


class ContractRepository implements ContractRepositoryInterface {



    protected $contract;

    /**
     * ContractRepository constructor.
     *
     * @param Contract $contract
     */
    public function __construct(Contract $contract)
    {

        $this->contract = $contract;
    }


    /**
     *  Create New Contract
     *
     * @param array $contractData
     *
     * @return static
     */
    public function create(array $contractData)
    {
        return $this->contract->create($contractData);
    }


    /**
     *Contract Pagination
     *
     * @param array $filter
     *
     * @return mixed
     */
    public function paginate(array $filter)
    {

        return $this->contract->latest()->paginate($filter['limit']);
    }
//    public function getOne()
//    {
//
//        return $this->contract->latest->get();
//    }



    /**
     * Get Contract by ID
     *
     * @param $contract_id
     *
     * @return mixed
     */
    public function find($contract_id)
    {
        return $this->contract->findOrFail($contract_id);
    }
}