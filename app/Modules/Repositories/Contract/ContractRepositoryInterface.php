<?php
namespace App\Modules\Repositories\Contract;

use App\Modules\Models\Contract\Contract;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface ContractRepositoryInterface
 * @package App\Modules\Repositories\Contract
 */
interface ContractRepositoryInterface {

    /**
     * Create New Contract
     *
     * @param array $contractData
     * @return Contract
     */
    public function create(array $contractData);

    /**
     * Contract Pagination
     *
     * @param array $filter
     * @return collection
     */
    public function paginate(array $filter);
    //public function getOne();

    /**
     * Get Contract by ID
     * @param $contract_id
     * @return Contract
     */
    public function find($contract_id);
}
