<?php
namespace App\Modules\Repositories\Notification;

use App\Modules\Models\Notification\Notification;


/**
 * Interface NotificationRepositoryInterface
 * @package App\Modules\Repositories\Notification
 */
interface NotificationRepositoryInterface
{
    /**
     * Create a notification
     *
     * @param array $data
     * @return notification
     */
    public function create(array $data);

    /**
     * Get a notification with given notificaionId
     *
     * @param $notificaionId
     * @return Notification
     */
    public function find($notificaionId);


}

