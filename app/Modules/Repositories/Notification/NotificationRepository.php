<?php
namespace App\Modules\Repositories\Notification;

use App\Modules\Models\Notification\Notification;


/**
 * Class NotificationRepository
 * @package App\Modules\Repositories\Notification
 */
class NotificationRepository implements NotificationRepositoryInterface
{
    /**
     * @var Notification
     */
    protected $notification;

    /**
     * @param Notification $notification
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Create a notification
     *
     * @param array $data
     * @return Notification
     */
    public function create(array $data)
    {
        return $this->notification->create($data);
    }


    /**
     * Get the notification with given notificationId
     *
     * @param $notificationId
     * @return User
     */
    public function find($notificationId)
    {
        return $this->user->findOrFail($notificationId);
    }

}