<?php
namespace App\Modules\Repositories\Role;

interface PermissionRepositoryInterface
{

    /**
     * Create New Permission
     *
     * @param array $permissionData
     * @return Permission
     */
    public function create(array $permissionData);

    /**
     * Permission Pagination
     *
     * @param array $filter
     * @return collection
     */
    public function paginate(array $filter);

    /**
     * Get Permission by ID
     * @param $roleId
     * @return Permission
     */
    public function find($roleId);

    /**
     * Get all permissions from database
     * @return mixed
     */
    public function all();

    /**
     * Get the user with selected fields from database
     * @param string $fields
     * @return mixed
     */
    public function get($fields = '');

}