<?php

namespace App\Modules\Repositories\Role;

use App\Modules\Models\Role\Role;
use Mockery\Exception;

class RoleRepository implements RoleRepositoryInterface
{
    protected $role;

    public function __construct(Role $role){
        $this->role = $role;
    }

    /**
     * Create New Role
     *
     * @param array $roleData
     * @return Role
     */
    public function create(array $roleData)
    {
        try {
            $role = $this->role->create($roleData);
            return $role;
        } catch (Exception $e) {
            //load the exception into a table
            return null;
        }
    }

    /**
     * Role Pagination
     *
     * @param array $filter
     * @return collection
     */
    public function paginate(array $filter)
    {
        $roles = $this->role;
        if (isset($filter['type']))
            $roles = $roles->ofType($filter['type']);

        return $roles->latest()->paginate($filter['limit']);
    }

    /**
     * Get Role by ID
     * @param $roleId
     * @return Role
     */
    public function find($roleId)
    {
        return $this->role->findOrFail($roleId);
    }

    /**
     * Get all roles from database
     * @return mixed
     */
    public function all()
    {
        return $this->role->all();
    }

    /**
     * Get the user with selected fields from database for roles
     * @param string $fields
     * @return mixed
     */
    public function get($fields = '')
    {
        $roles = $this->role;
        if( !empty($fields) )
            return $roles->get($fields);

        return $roles->get();
    }


}