<?php

namespace App\Modules\Repositories\Role;

use App\Modules\Models\Role\Role;
use Illuminate\Database\Eloquent\Collection;

interface RoleRepositoryInterface
{

    /**
     * Create New Role
     *
     * @param array $roleData
     * @return Role
     */
    public function create(array $roleData);

    /**
     * Role Pagination
     *
     * @param array $filter
     * @return collection
     */
    public function paginate(array $filter);

    /**
     * Get Role by ID
     * @param $roleId
     * @return Role
     */
    public function find($roleId);

    /**
     * Get all roles from database
     * @return mixed
     */
    public function all();

    /**
     * Get the user with selected fields from database for roles
     * @param string $fields
     * @return mixed
     */
    public function get($fields = '');

}