<?php namespace App\Modules\Repositories\Role;

use App\Modules\Models\Role\Permission;

class PermissionRepository implements PermissionRepositoryInterface
{
    private $permission;

    /**
     * @param Permission $permission
     */
    public function __construct(Permission $permission){
        $this->permission = $permission;
    }

    /**
     * Create New Permission
     *
     * @param array $permissionData
     * @return Permission
     */
    public function create(array $permissionData)
    {
        try{
            return $this->permission->create($permissionData);
        }catch (\Exception $e){

        }
    }

    /**
     * Permission Pagination
     *
     * @param array $filter
     * @return collection
     */
    public function paginate(array $filter)
    {
        $permissions = $this->permission;

        if( isset($filter['type']) ){
            $permissions->ofType($filter['type']);
        }

        return $permissions->latest()->paginate($filter['limit']);
    }

    /**
     * Get Permission by ID
     * @param $roleId
     * @return Permission
     */
    public function find($roleId)
    {
        return $this->permission->findOrFail($roleId);
    }

    /**
     * Get all permissions from database
     * @return mixed
     */
    public function all()
    {
        return $this->permission->all();
    }

    /**
     * Get the user with selected fields from database
     * @param string $fields
     * @return mixed
     */
    public function get($fields = '')
    {
        $permissions = $this->permission;
        if( !empty($fields) ){
            $permissions->select($fields);
        }

        return $permissions->get();
    }

}