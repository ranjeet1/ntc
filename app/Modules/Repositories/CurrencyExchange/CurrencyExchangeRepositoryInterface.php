<?php
namespace App\Modules\Repositories\CurrencyExchange;

/**
 * Interface CurrencyExchangeRepositoryInterface
 * @package App\Modules\Repositories\Operator
 */
interface CurrencyExchangeRepositoryInterface
{

    public function  insert($currencyExchangeData);

    /**
     * Get all excahngeRates
     * @return mixed
     */
    public function all();
}
