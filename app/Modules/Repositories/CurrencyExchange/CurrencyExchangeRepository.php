<?php

namespace App\Modules\Repositories\CurrencyExchange;

use App\Modules\Models\Import\CurrencyExchange;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CurrencyExchangeRepository
 *
 * @package App\Modules\Repositories\CurrencyExchange
 */
class CurrencyExchangeRepository implements CurrencyExchangeRepositoryInterface
{
    /**
     * @var CurrencyExchange
     */
    protected $currencyExchange;

    /**
     * @param currencyExchange $currencyExchange
     */
    public function __construct(CurrencyExchange $currencyExchange)
    {
        $this->currencyExchange = $currencyExchange;
    }

    /**
     * Get all exchangeRates
     *
     * @return Collection
     */
    public function all()
    {
        return $this->currencyExchange->all();
    }

    /**
     * Calculate average of the currency
     * @param $date1
     * @param $date2
     * @param $currency
     * @return mixed
     */
    public function average($date1, $date2, $currency)
    {
        return $this->currencyExchange->average($date1, $date2, $currency);
    }

    /**
     * Insert data in currency_exchange table
     * @param $currencyExchangeData
     * @return mixed
     */
    public function insert($currencyExchangeData)
    {
        return $this->currencyExchange->insert($currencyExchangeData);
    }

}

