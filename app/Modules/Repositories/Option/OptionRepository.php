<?php

namespace App\Modules\Repositories\Option;

use App\Modules\Models\Option\Option;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class OptionRepository
 *
 * @package App\Modules\Repositories\Option
 */
class OptionRepository implements OptionRepositoryInterface
{
    /**
     * @var Option
     */
    protected $option;

    /**
     * @param Option $optionData
     */
    public function __construct(Option $optionData)
    {
        $this->option = $optionData;
    }

    /**
     * Create a new option
     *
     * @param array $optionData
     * @return Option
     */
    public function create(array $optionData)
    {
        return $this->option->create($optionData);
    }

    /**
     * Get all option
     *
     * @return Collection
     */
    public function all()
    {
        return $this->option->all();
    }

    /**
     * Get a option
     *
     * @param $option
     * @param $field
     * @return mixed
     */
    public function find($option, $field)
    {
        return $this->option->where($field, '=', $option)->first();
    }

}

