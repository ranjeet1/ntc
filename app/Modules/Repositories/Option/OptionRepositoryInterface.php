<?php
namespace App\Modules\Repositories\Option;

use App\Modules\Models\Option\Option;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface OptionRepositoryInterface
 * @package App\Modules\Repositories\Option
 */
interface OptionRepositoryInterface
{
    /**
     * Create a option
     *
     * @param array $optionData
     * @return Option
     */
    public function create(array  $optionData);

    /**
     * Get all option
     *
     * @return Collection
     */
    public function all();


    /**
     * Get a option
     *
     * @param $option
     * @param $field
     * @return mixed
     */
    public function find( $option, $field);
}
