<?php

namespace App\Modules\Models\Option;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'options';

    protected $fillable = [
        'name',
        'value'
    ];
    
}
