<?php
namespace App\Modules\Models\Tier;

use Illuminate\Database\Eloquent\Model;

/**
 * @property  title
 * @property  description
 */
class Tier extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tier_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tier_name', 'tier_code'];

    /**
     * Get tier name with code in bracket
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->tier_name . ' ('. $this->tier_code  . ')';
    }

    /**
     * Get the rate profile associated with the tier.
     */
    public function rateProfile()
    {
        return $this->hasMany('App\Modules\Models\RateProfile\RateProfile');
    }

}