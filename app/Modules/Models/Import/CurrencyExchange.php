<?php

namespace App\Modules\Models\Import;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CurrencyExchange
 * @package App\Modules\Models\Import
 */
class CurrencyExchange extends Model
{
    protected $table = 'currency_exchange';

    /**
     * Calculate monthly average of a particular currency
     * @param $date1
     * @param $date2
     * @param $currency
     * @return mixed
     */

    public function average($date1, $date2, $currency)
    {

        return $this->where('Date', '>=', $date1)->where('Date', '<=', $date2)->avg($currency);
    }

}
