<?php

namespace App\Modules\Models\Import;

use Illuminate\Database\Eloquent\Model;

class DataCallVolume extends Model
{
    protected $table = 'data_call_volumes';

    protected $fillable = ['source_file_name', 'imported_date', 'operator_name', 'operator_code', 'component_direction', 'call_count', 'call_type', 'call_duration'];
}
