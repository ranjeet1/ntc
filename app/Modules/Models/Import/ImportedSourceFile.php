<?php
/**
 * Created by PhpStorm.
 * User: Eragon
 * Date: 5/7/2016
 * Time: 4:17 PM
 */

namespace App\Modules\Models\Import;

use Illuminate\Database\Eloquent\Model;

class ImportedSourceFile extends Model {

    protected $fillable = ['source_file_name', 'imported_date'];

}