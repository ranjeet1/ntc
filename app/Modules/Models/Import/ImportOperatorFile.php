<?php


namespace App\Modules\Models\Import;

use Illuminate\Database\Eloquent\Model;

class ImportOperatorFile extends Model {

	protected $table = 'crosswalk_operator_details';

    protected $fillable = ['source_file_name', 'imported_date','operator_code','operator_name'];

}