<?php

namespace App\Modules\Models\Notification;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by','created_for', 'message', 'link', 'status'
    ];


   
   
}
