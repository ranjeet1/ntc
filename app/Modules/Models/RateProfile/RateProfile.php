<?php

namespace App\Modules\Models\RateProfile;

use Illuminate\Database\Eloquent\Model;

class RateProfile extends Model
{
    protected $table = 'rate_profiles';

    protected $fillable = ['tier_detail_id', 'contract_id', 'rate_type', 'ceiling', 'rate', 'amount', 'hybrid_type', 'hybrid_ceiling'];

    /**
     * Get the contract that owns the rate profile
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    protected function contract()
    {
        return $this->belongsTo('App\Modules\Models\Contract\Contract');
    }

    /**
     * Get the tier that owns the rate profile.
     */
    public function tier()
    {
        return $this->belongsTo('App\Modules\Models\Tier\Tier', 'tier_detail_id');
    }
}
