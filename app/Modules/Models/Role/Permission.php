<?php

namespace App\Modules\Models\Role;

use Zizaco\Entrust\EntrustPermission;

/**
 * Class Permission
 *
 * The permission model has 3 main attributes
 * name: name of permission, should be unique. And name will be used for validating permission in the application
 *         For example: create_user, edit_user
 * display_name: Human readable name for permission (is optional)
 *          For example: Create new User, Update existing User
 * description: Description of the permission, it is just explanation of permission (is optional)
 *
 * @package App\Modules\Models\Role
 */

class Permission extends EntrustPermission
{
    protected $fillable = ['name', 'display_name', 'description'];

}