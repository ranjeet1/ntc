<?php

namespace App\Modules\Models\Role;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustRole;

/**
 * Class Role
 * The Role model has three main attributes
 * Name: Unique name for the Role, used for looking up role information in the application layer.
 *          For example: "super_admin", "admin", "moderator", "user".
 * display_name — Human readable name for the Role. Not necessarily unique and optional.
 *          For example: "User Administrator", "Project Owner"
 * description — A more detailed explanation of what the Role does
 *
 * Both display_name and description are optional
 *
 * This model will use soft deleting trait
 *
 * @package App\Modules\Models\Role
 */

class Role extends EntrustRole
{

    use SoftDeletes;

    protected $fillable = ['name', 'display_name', 'description'];

}
