<?php

namespace App\Modules\Models\Operator;

use Illuminate\Database\Eloquent\Model;

class OperatorType extends Model
{
    protected $table = 'operator_types';

    protected $fillable = [
        'name',
        'slug'
    ];

    public function operatorService()
    {
        return $this->hasMany('App\Modules\Models\Operator\OperatorService');
    }

   }
