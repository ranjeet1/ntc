<?php

namespace App\Modules\Models\Operator;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    protected $table = 'operators';

    protected $fillable = [
        'name',
        'code',
        'notifiers',
        'logo',
        'source_file_name',
        'operator_type_id',
        'phone_number',
        'city',
        'state',
        'country',
        'street_address',
        'tax_rate',
        'interest',
        'contact_person_name',
        'contact_person_phone_number',
        'contact_person_email',
        'contact_person_designation',

    ];

    public function contracts()
    {
        return $this->hasMany('App\Modules\Models\Contract\Contract');
    }

    public function operatorType()
    {
        return $this->belongsTo('App\Modules\Models\Operator\OperatorType','operator_type_id');
    }

    public function services()
    {
        return $this->belongsToMany('App\Modules\Models\Service\ServiceModel','operator_service','operator_id','service_id');
    }

    public function banks()
    {
        return $this->hasMany('App\Modules\Models\OperatorBank\OperatorBank','operator_id');
    }

    public function dataFinalAmount()
    {
        return $this->hasOne('App\Modules\Models\Invoice\DataFinalAmount', 'billing_operator', 'code');
    }

    public function invoices()
    {
        return $this->belongsTo('App\Modules\Models\Invoice\Invoice','id','operator_id');
    }
}
