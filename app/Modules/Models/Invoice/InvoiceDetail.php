<?php

namespace App\Modules\Models\Invoice;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $table = 'invoice_details';

    protected $fillable = [
        'tier',
        'call_count',
        'call_minutes',
        'average_rate',
        'total_amount',
        'invoice_id',

    ];


}
