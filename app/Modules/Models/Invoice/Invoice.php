<?php

namespace App\Modules\Models\Invoice;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $fillable = [
        'document_num',
        'invoice_number',
        'settlement_date',
        'settlement_due_date',
        'billing_start_date',
        'billing_end_date',
        'operator_id',
        'call_count',
        'call_minutes',
        'rate_applied',
        'total_amount',
        'created_by',
        'approved_by',
        'published_by',
        'authorized_by',
        'status',
    ];

    public function dataFinalAmount() {
        return $this->belongsTo('App\Modules\Models\Invoice\DataFinalAmount', 'operator_id');
    }

    public function operators()
    {
        return $this->hasMany('App\Modules\Models\Operator\Operator', 'id','operator_id');

    }


}
