<?php

namespace App\Modules\Models\Invoice;

use Illuminate\Database\Eloquent\Model;

class DataFinalAmount extends Model
{
    protected $table = 'data_final_amount';

    protected $fillable = [
        'date',
        'billing_operator',
        'billing_operator_name',
        'component_direction',
        'rate_profile',
        'call_count',
        'event_duration_minutes',
        'average_rate',
        'total_amount',


    ];
   public function operator() {
          return $this->belongsTo('App\Modules\Models\Operator\Operator', 'code', 'billing_operator');
   }

    public function invoices() {
        return $this->hasMany('App\Modules\Models\Invoice\Invoice');
    }



}
