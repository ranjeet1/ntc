<?php

namespace App\Modules\Models\Contract;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $table = 'contracts';

    protected $fillable = [
        'start_date',
        'end_date',
        'remark',
        'terms_and_conditions',
        'operator_id',
        'interest',
        'interest_rate',
        'tax_included',

    ];

    public function operator()
    {
        return $this->belongsTo('App\Modules\Models\Operator\Operator','operator_id');
    }

   

    public function rateProfiles()
    {
        return $this->hasMany('App\Modules\Models\RateProfile\RateProfile')->orderBy('ceiling', 'asc');
    }


}
