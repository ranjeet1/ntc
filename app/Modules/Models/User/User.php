<?php

namespace App\Modules\Models\User;
use Illuminate\Foundation\Auth\User as Authenticable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticable
{

    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'salutation',
        'first_name',
        'last_name',
        'designation',
        'email',
        'password',
        'photo',
        'identification_document',
        'phone_number',
        'district',
        'street_name',
        'city',
        'country',
        'confirmation_code',
        'confirmed',
        'info'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Scope a query to only include users of given type
     *
     * @param $query
     * @param $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfType($query, $type)
    {

        return $query->where('type', $type);
    }

    /**
     * Will Return full name of user, with salutation
     * @return string
     */
    /**
     * Get user full name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }


    function fullName() {
        $nameArr = [
            $this->salutation,
            $this->first_name,
            $this->last_name
        ];
        return implode(' ', $nameArr);
    }

    /**
     * Return full address of User
     * @return string
     */
    function address() {
        return $this->country . ' ' . $this->city . ' ' . $this->street_name;
    }


}

