<?php

namespace App\Modules\Models\OperatorService;

use Illuminate\Database\Eloquent\Model;

class OperatorService extends Model
{

    protected $fillable = [
        'operator_id',
        'service_id',

    ];

}
