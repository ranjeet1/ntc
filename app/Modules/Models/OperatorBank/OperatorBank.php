<?php

namespace App\Modules\Models\OperatorBank;

use Illuminate\Database\Eloquent\Model;


class OperatorBank extends Model
{
    protected $table = 'operator_banks';

    protected $fillable = [
        'name',
        'account_number',
        'operator_id',
        'active',

    ];


    public function operator()
    {
        return $this->belongsTo('App\Modules\Models\Operator\Operator','operator_id');
    }





}
