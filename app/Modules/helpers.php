<?php

function delete_form($url, $label = 'Delete', $class = '', $title = '')
{
    $form = Form::open(['method' => 'DELETE', 'url' => $url, 'class' => 'deleteContentForm']);
    $form .= "<button type='submit' class='$class' title='$title' data-toggle='tooltip' data-placement='top' data-original-title='$title'><i class='fa fa-trash-o'> </i> $label</button>";
    return $form .= Form::close();
}

function formatDate($date, $format = 'Y-m-d')
{
    $formatOption = getOptions('date_format');
    $format = $formatOption ? $formatOption : $format;

    return date($format, strtotime($date));
}

/**
 * Get all or specified option(s) from storage.
 *
 * @param null $name
 * @return \stdClass
 */
function getOptions($name = NULL)
{
    $optionModel = new \App\Modules\Models\Option\Option();

    if ($name) {
        $option = $optionModel->where('name', '=', $name)->first();

        return $option ? $option->value : '';
    } else {
        $options = new \stdClass();

        $allOptions = $optionModel->all();
        foreach ($allOptions as $option) {
            $options->{$option->name} = $option->value;
        }

        return $options;
    }

}

function formatCurrency($amount, $currency = 'NRs. ')
{
    return $currency . number_format($amount, 2, '.', '');
}

function image_url($image, $thumb = true)
{
    $url = config('constants.imageUploadPath') . '/';

    if ($thumb) {
        $url .= 'thumb/';
    }
    $url .= $image;
    if ($image && file_exists($url)) {
        return url($url);
    }

    return '/assets/boostbox/img/photo-not-found.png';
}

function import_path($name)
{
    $path = config('constants.import_path') . '/' . $name;

    return $path;
}