<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operators', function (Blueprint $table) {
            $table->integer('operator_type_id')->nullable()->unsigned()->index();
            $table->foreign('operator_type_id')->references('id')->on('operator_types')->onDelete('cascade');
            $table->string('country_code');
            $table->string('phone_number');
            $table->string('contact_person_name');
            $table->string('contact_person_designation');
            $table->string('contact_person_email');
            $table->integer('contact_person_phone_number');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('street_address');
            $table->float('tax_rate');
            $table->float('interest');

        });

        Schema::table('operators', function (Blueprint $table) {
            $table->dropColumn('contact_address');
            $table->dropColumn('contact_person');
            $table->dropColumn('mailing_details');
            $table->dropColumn('phone_details');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operators', function (Blueprint $table) {
            $table->dropForeign('operators_operator_type_id_foreign');
            $table->dropColumn('operator_type_id');
            $table->dropColumn('country_code');
            $table->dropColumn('phone_number');
            $table->dropColumn('contact_person_name');
            $table->dropColumn('contact_person_designation');
            $table->dropColumn('contact_person_email');
            $table->dropColumn('contact_person_phone_number');
            $table->dropColumn('country');
            $table->dropColumn('state');
            $table->dropColumn('city');
            $table->dropColumn('street_address');
            $table->dropColumn('tax_rate');
            $table->dropColumn('interest');


        });
        Schema::table('operators', function (Blueprint $table) {
            $table->string('contact_address');
            $table->string('contact_person');
            $table->string('mailing_details');
            $table->string('phone_details');


        });
    }
}
