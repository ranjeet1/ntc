<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveHybridCeilingFieldFromRateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rate_profiles', function (Blueprint $table) {
            $table->dropColumn('hybrid_ceiling');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rate_profiles', function (Blueprint $table) {
            $table->integer('hybrid_ceiling')->default(0);
        });
    }
}
