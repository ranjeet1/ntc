<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('document_num');
            $table->string('invoice_number');
            $table->string('settlement_date');
            $table->string('settlement_due_date');
            $table->string('billing_start_date');
            $table->string('billing_end_date');
            $table->string('operator_id');
            $table->double('call_count');
            $table->double('call_minutes');
            $table->double('rate_applied');
            $table->string('total_amount');
            $table->string('created_by');
            $table->string('approved_by');
            $table->string('published_by');
            $table->string('authorized_by');
            $table->bigInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice_details');
    }
}
