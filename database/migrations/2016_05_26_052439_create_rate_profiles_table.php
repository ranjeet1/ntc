<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tier_detail_id');
            $table->integer('contract_id');
            $table->string('rate_type')->default('NULL');
            $table->integer('ceiling')->default(0);
            $table->double('rate', 15, 2)->default('0');
            $table->double('amount', 15, 2)->default('0');
            $table->string('hybrid_type')->default('NULL');
            $table->integer('hybrid_ceiling')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rate_profiles');
    }
}
