<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function($table) {

            $table->increments('id');

            $table->string('start_date');
            $table->string('end_date');
            $table->string('remark');
            $table->string('terms_and_conditions');
            $table->integer('operator_id');
            $table->enum('interest', array('yes', 'no'));
            $table->string('interest_rate');
            $table->enum('tax_included',array('yes','no'));


            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contracts');
    }
}
