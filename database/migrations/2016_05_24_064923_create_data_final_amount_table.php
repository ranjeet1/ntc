<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataFinalAmountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_final_amount', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->string('billing_operator');
            $table->string('billing_operator_name');
            $table->string('component_direction');
            $table->string('rate_profile');
            $table->string('call_count');
            $table->string('event_duration_minutes');
            $table->string('average_rate');
            $table->string('total_amount');



            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_final_amount');
    }
}
