<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class DataCallVolumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_call_volumes', function( $table ) {

            $table->bigIncrements('id');

            $table->string('source_file_name');
            $table->date('received_date');
            $table->string('operator_name');
            $table->string('operator_code');
            $table->string('component_direction')->nullable();
            $table->string('call_type')->nullable();
            $table->string('tier')->nullable();
            $table->bigInteger('call_count');
            $table->bigInteger('call_duration');

            $table->string('udf1');
            $table->string('udf2');
            $table->string('udf3');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data_call_volumes');
    }
}
