<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('photo');
            $table->string('identification_document');
            $table->string('phone_number');
            $table->string('street_name');
            $table->string('city');
            $table->string('district');
            $table->string('country');
   

        });
         Schema::table('users', function (Blueprint $table) {          
           $table->dropColumn('name');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('photo');
            $table->dropColumn('identification_document');
            $table->dropColumn('phone_number');
            $table->dropColumn('street_name');
            $table->dropColumn('city');
            $table->dropColumn('district');
            $table->dropColumn('country');
            $table->dropSoftDeletes();
          
        });
         Schema::table('users', function (Blueprint $table) {
            $table->string('name');


        });
    }
}
