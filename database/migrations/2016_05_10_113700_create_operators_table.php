<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operators', function($table) {

            $table->increments('id');

            $table->string('name');
            $table->string('contact_address');
            $table->string('contact_person');
            $table->string('notifiers');
            $table->string('logo');
            $table->string('phone_details');
            $table->string('mailing_details');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('operators');
    }
}
