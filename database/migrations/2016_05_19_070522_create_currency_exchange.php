<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyExchange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_exchange', function (Blueprint $table) {
            $table->increments('id');
            $table->date('Date');

            $table->float('IND_U');
            $table->float('IND_S');
            $table->float('USD_U');
            $table->float('USD_S');
            $table->float('GBP_U');
            $table->float('GBP_S');
            $table->float('EUR_U');
            $table->float('EUR_S');
            $table->float('AUD_U');
            $table->float('AUD_S');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currency_exchange');
    }
}
