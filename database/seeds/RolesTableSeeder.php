<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        DB::table('roles')->insert([
            'name' => 'super_admin',
            'display_name' => 'Super Admin'
        ]);

        DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'Administrator'
        ]);

        DB::table('roles')->insert([
            'name' => 'moderator',
            'display_name' => 'Moderator'
        ]);

        DB::table('roles')->insert([
            'name' => 'user',
            'display_name' => 'User'
        ]);
    }
}
