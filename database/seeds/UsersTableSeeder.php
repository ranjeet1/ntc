<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Shrestha',
            'last_name' => 'Sajjan',
            'email' => 's4jj4n@gmail.com',
            'password' => bcrypt('password')
        ]);

        DB::table('users')->insert([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'superadmin@gmail.com',
            'password' => bcrypt('password@@123')
        ]);
    }
}
