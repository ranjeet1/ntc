<?php

use Illuminate\Database\Seeder;

class DataFinalAmountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \ DB::table('data_final_amount')->truncate();

        $rows = [
            [
                "date"                   => '2015 - 08 - 30',
                "billing_operator"       => "ATT",
                "billing_operator_name"  => "AT & T - USA",
                "component_direction"    => "I",
                "rate_profile"           => "BILUSA",
                "call_count"             => "200",
                "event_duration_minutes" => "25034:00",
                "average_rate"           => "0.05",
                "total_amount"           => "19223.44",

            ],

            [
                "date"                   => '2015 - 09 - 30',
                "billing_operator"       => "ATT",
                "billing_operator_name"  => "AT & T - USA",
                "component_direction"    => "I",
                "rate_profile"           => "OFFNET1",
                "call_count"             => "2340",
                "event_duration_minutes" => "25634:00",
                "average_rate"           => "0.75",
                "total_amount"           => "19225.5",
            ],
            [
                "date"                   => '2015 - 10 - 30',
                "billing_operator"       => "ATT",
                "billing_operator_name"  => "AT & T - USA",
                "component_direction"    => "I",
                "rate_profile"           => "OFFNET2",
                "call_count"             => "3432",
                "event_duration_minutes" => "25034:00",
                "average_rate"           => "0.05",
                "total_amount"           => "19223.44",
            ],
            [
                "date"                   => '2016 - 01 - 30',
                "billing_operator"       => "ATT",
                "billing_operator_name"  => "AT & T - USA",
                "component_direction"    => "I",
                "rate_profile"           => "BILUSA",
                "call_count"             => "200",
                "event_duration_minutes" => "25034:00",
                "average_rate"           => "0.05",
                "total_amount"           => "19223.44",
            ],
            [
                "date"                   => '2016 - 02 - 30',
                "billing_operator"       => "ATT",
                "billing_operator_name"  => "AT & T - USA",
                "component_direction"    => "I",
                "rate_profile"           => "BILIND",
                "call_count"             => "200",
                "event_duration_minutes" => "25034:00",
                "average_rate"           => "0.05",
                "total_amount"           => "19223.44",
            ],
            [
                "date"                   => '2016 - 03 - 30',
                "billing_operator"       => "ATT",
                "billing_operator_name"  => "AT & T - USA",
                "component_direction"    => "I",
                "rate_profile"           => "BILIND",
                "call_count"             => "200",
                "event_duration_minutes" => "25034:00",
                "average_rate"           => "0.05",
                "total_amount"           => "19223.44",
            ],
            [
                "date"                   =>'2016 - 04 - 30',
                "billing_operator"       => "ATT",
                "billing_operator_name"  => "AT & T - USA",
                "component_direction"    => "I",
                "rate_profile"           => "BILIND",
                "call_count"             => "200",
                "event_duration_minutes" => "25034:00",
                "average_rate"           => "0.05",
                "total_amount"           => "19223.44",
            ],
        ];
        \  DB::table('data_final_amount')->insert($rows);
    }
}
