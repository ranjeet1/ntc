<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //users table

        DB::table('permission')->insert([
            'name' => 'add_user',
            'display_name' => 'Add user',
            'description' =>'user can add a new users'
        ]);

        DB::table('permission')->insert([
            'name' => 'edit_user',
            'display_name' => 'Edit user',
            'description' =>'user can edit any users'

        ]);

        DB::table('permission')->insert([
            'name' => 'delete_user',
            'display_name' => 'Delete user',
            'description' =>'user can delete a any users'

        ]);

        //operator types table

        DB::table('permission')->insert([
            'name' => 'can_import ',
            'display_name' => 'can import',
            'description' =>'user can import files for operator',

        ]);

        DB::table('permission')->insert([
            'name' => 'can_edit',
            'display_name' => 'can edit operator',
            'description' =>'user can edit information of Operator types ',

        ]);

        DB::table('permission')->insert([
            'name' => 'can_delete',
            'display_name' => 'can delete ',
            'description' =>'user can  Delete information of Operator types ',

        ]);


        //contracts table
        
        DB::table('permission')->insert([
            'name' => 'can_add',
            'display_name' => 'can add ',
            'description' =>'user can add information of contracts',

        ]);
        DB::table('permission')->insert([
            'name' => 'can_edit',
            'display_name' => 'can edit ',
            'description' =>'user can edit information of contracts',

        ]);
        DB::table('permission')->insert([
            'name' => 'can_delete',
            'display_name' => 'can delete',
            'description' =>'user can delete information of contracts',

        ]);

        //operator service

        DB::table('permission')->insert([
            'name' => 'can_add',
            'display_name' => 'can add',
            'description' =>'user can add information of operator service',

        ]);

        DB::table('permission')->insert([
            'name' => 'can_edit',
            'display_name' => 'can edit',
            'description' =>'user can edit information of operator service',

        ]);

        DB::table('permission')->insert([
            'name' => 'can_delete',
            'display_name' => 'can delete',
            'description' =>'user can delete information of operator service',

        ]);

          




       
    }
}
