@extends('layouts.admin.boostbox.external')

@section('content')
    <div class="section-body contain-lg">

        <div class="row">
            <div class="col-lg-12 text-center">
                <h1><span class="text-xxxl text-light">404 <i class="fa fa-search-minus text-primary"></i></span></h1>

                <h2 class="text-light">This page does not exist</h2>

                <h2 class="text-light"><a href="{{ url('admin') }}" class="btn btn-primary">Back to Dashboard</a></h2>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->

    </div>

@stop
