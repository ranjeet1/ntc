@extends('layouts.admin.boostbox.external')

@section('content')

    <!-- START LOGIN BOX -->
    <div class="box-type-login">
        <div class="box text-center">
            <div class="box-head">
                <h2 class="text-light text-white">Interconnect <strong>Login</strong> <i class="fa fa-rocket fa-fw"></i></h2>
                {{--<h4 class="text-light text-inverse-alt">Ease your output with BoostBox</h4>--}}
            </div>
            <div class="box-body box-centered style-inverse">
                <h2 class="text-light">Sign in to your account</h2>
                <br/>
                <form action="{{ url('/admin/login') }}" accept-charset="utf-8" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" name="email" placeholder="E-Mail Address">

                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="password" class="form-control" name="password" placeholder="Password">

                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-6 text-left">
                            <div data-toggle="buttons">
                                <label class="btn checkbox-inline btn-checkbox-primary-inverse">
                                    <input type="checkbox" name="remember" value="default-inverse1"> Remember me
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-6 text-right">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-key"></i> Sign in</button>
                        </div>
                    </div>
                </form>
            </div><!--end .box-body -->
            <div class="box-footer force-padding text-white">
                <a class="text-primary-alt" href="login.html">No account yet?</a> Or did you
                <a class="text-primary-alt" href="{{ url('admin/password/reset') }}">forgot your password?</a>
            </div>
        </div>
    </div>
    <!-- END LOGIN BOX -->


@stop