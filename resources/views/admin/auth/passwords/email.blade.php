@extends('layouts.admin.external')

@section('content')

    <div class="account-container">

        <div class="content clearfix">

            <form action="{{ url('admin/password/email') }}" method="post">
                {!! csrf_field() !!}
                <h1>Reset Password</h1>

                <div class="login-fields">

                    <p>Please provide your details</p>

                    <div class="field">
                        <label for="email">E-Mail Address</label>
                        <input type="text" id="email" name="email" value="{{ old('email') }}" placeholder="Email"
                               class="login username-field"/>
                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <!-- /field -->

                </div>
                <!-- /login-fields -->

                <div class="login-actions">
				     <button class="button btn btn-success btn-large">Send Password Reset Link</button>
                </div>
                <!-- .actions -->


            </form>

        </div>
        <!-- /content -->

    </div> <!-- /account-container -->

@stop