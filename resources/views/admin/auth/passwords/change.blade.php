@extends('layouts.admin.index')

@section('main-content')
    <div class="row">
        <div class="span8">

            <div class="widget ">

                <div class="widget-header">
                    <i class="icon-plus-sign"></i>

                    <h3>Change Password</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('changeUserPassword') }}">
                        {!! csrf_field() !!}

                        <fieldset>
                            <div class="control-group{{ $errors->has('present_password') ? ' has-error' : '' }}">
                                <label class="control-label">Present Password</label>

                                <div class="controls">
                                    <input type="password" class="span4" name="present_password"
                                           value="{{ $present_password or old('present_password') }}">

                                    @if ($errors->has('present_password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('present_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="control-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                                <label class="control-label">New Password</label>

                                <div class="controls">
                                    <input type="password" class="span4" name="new_password">

                                    @if ($errors->has('new_password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('new_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="control-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                                <label class="control-label">Confirm New Password</label>

                                <div class="controls">
                                    <input type="password" class="span4" name="new_password_confirmation">

                                    @if ($errors->has('new_password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Change Password</button>
                                <a href="{{ route('dashboard') }}" class="btn">Cancel</a>
                            </div>


                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
