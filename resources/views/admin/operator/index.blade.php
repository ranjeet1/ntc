@extends('layouts.admin.boostbox.index')

@section('content')

    <div class="col-lg-12">
        <form class="form-horizontal" action="{{ url('admin/operator/import') }}" role="form" method="post"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="form-group">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label for="file" class="control-label">Import Operator Files</label>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-5">
                    <input type="file" name="file" id="file" class="form-control">
                    <span for="name" class="help-block">{{ $errors->first('file') }}</span>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>


    <!-- START DATATABLE 2 -->
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-bordered">
                <div class="box-head">
                    <header>
                        <h4 class="text-light">Operators <strong>Table</strong>
                            {{--<small>Tabletools & row details</small>--}}
                        </h4>
                    </header>
                </div>
                <div class="box-body no-padding table-responsive">
                    <table id="datatable2" class="table table-hover">
                        <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Contact Person</th>
                            <th>Address</th>
                            <th class="text-right1" style="width:90px">Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @if($operators->count() > 0)
                            @foreach($operators as $k => $operator)
                                <?php $count = ($operators->perPage() * ($operators->currentPage() - 1)); ?>
                                <tr class="gradeX">
                                    <td>{{ $count ? ($count + $k + 1) : ++$k }}</td>
                                    <td>
                                        <a href="{{ route('admin.operator.edit', [$operator->id]) }}">
                                            {{ $operator->name }}
                                        </a>
                                    </td>
                                    <td>{{ $operator->code }}</td>
                                    <td>{{ $operator->contact_person_name }}</td>
                                    <td>{{ $operator->street_address }}</td>
                                    <td class="text-right">
                                        <a href="{{ route('admin.operator.edit', [$operator->id]) }}"
                                           class="btn btn-xs btn-default btn-equal"
                                           data-toggle="tooltip"
                                           data-placement="top" data-original-title="Edit row">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {!! delete_form(route('admin.operator.destroy', [$operator->id]), '', 'btn
                                        btn-xs btn-default btn-equal', 'Delete row') !!}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="gradeX">
                                <td colspan="6">No Operators Found.</td>
                            </tr>
                        @endif

                        </tbody>
                    </table>
                    {!! $operators->appends($_GET)->links() !!}


                </div>
                <!--end .box-body -->
            </div>
            <!--end .box -->
        </div>
        <!--end .col-lg-12 -->
    </div>
    <!-- END DATATABLE 2 -->


@stop