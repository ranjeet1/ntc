<div class="form-group">
	<div class="col-md-2">
		<label for="name" class="control-label">Name</label>
	</div>

	<div class="col-md-5">
		<input type="text" id="name" name="name" class="form-control"
			   value="{{ isset($operator->name) ? $operator->name : '' }}" readonly>
		<span for="name" class="help-block">{{ $errors->first('name') }}</span>
	</div>
</div>

<div class="form-group">
	<div class="col-md-2">
		<label for="code" class="control-label">Code</label>
	</div>

	<div class="col-md-5">
		<input type="text" id="code" name="code" class="form-control"
			   value="{{ isset($operator->code) ? $operator->code : '' }}" readonly>
		<span for="code" class="help-block">{{ $errors->first('code') }}</span>
	</div>
</div>

<div class="form-group">
	<div class="col-md-2">
		<label for="file" class="control-label">Logo</label>
	</div>

	<div class="col-md-5">
		<input type="file" name="file" id="file" class="form-control">
		<span for="file" class="help-block">{{ $errors->first('file') }}</span>
	</div>
</div>

<div class="form-group">
	<div class="col-md-2">
		<label for="contact_person" class="control-label">Contact Person</label>
	</div>

	<div class="col-md-5">
		<input type="text" id="contact_person" name="contact_person"
			   value="{{ isset($operator->contact_person) ? $operator->contact_person : '' }}" class="form-control"
			   placeholder="Contact Person Name">
		<span for="contact_person" class="help-block">{{ $errors->first('contact_person') }}</span>
	</div>
</div>

<div class="form-group">
	<div class="col-md-2">
		<label for="contact_address" class="control-label">Contact Address</label>
	</div>

	<div class="col-md-5">
		<input type="text" id="contact_address" name="contact_address"
			   value="{{ isset($operator->contact_address) ? $operator->contact_address : '' }}" class="form-control"
			   placeholder="Contact Address">
		<span for="contact_address" class="help-block">{{ $errors->first('contact_address') }}</span>
	</div>
</div>

<div class="form-group">
	<div class="col-md-2">
		<label for="phone_details" class="control-label">Phone Details</label>
	</div>

	<div class="col-md-5">
		<input type="text" id="phone_details" name="phone_details"
			   value="{{ isset($operator->phone_details) ? $operator->phone_details : '' }}" class="form-control"
			   placeholder="Phone Details">
		<span for="phone_details" class="help-block">{{ $errors->first('phone_details') }}</span>
	</div>
</div>

<div class="form-group">
	<div class="col-md-2">
		<label for="mailing_details" class="control-label">Mailing Details</label>
	</div>

	<div class="col-md-5">
		<input type="text" id="mailing_details" name="mailing_details"
			   value="{{ isset($operator->mailing_details) ? $operator->mailing_details : '' }}" class="form-control"
			   placeholder="Mailing Details">
		<span for="mailing_details" class="help-block">{{ $errors->first('mailing_details') }}</span>
	</div>
</div>

<div class="form-group">
	<div class="col-md-2">
		<label for="notifiers" class="control-label">Notifiers</label>
	</div>

	<div class="col-md-10">
		<input type="text" name="notifiers" id="notifiers"
			   value="{{ isset($operator->notifiers) ? $operator->notifiers : '' }}" data-role="tagsinput"/>
		<span for="notifiers" class="help-block">{{ $errors->first('notifiers') }}</span>
	</div>
</div>