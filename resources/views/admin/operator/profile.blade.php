@extends('layouts.admin.boostbox.index')
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	  xmlns="http://www.w3.org/1999/html">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

@section('content')

<!--modal-->
    <div class="container">

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Choose Year and Month of Invoice</h4>
                    </div>
                    <div class="modal-body">
						<form action="{{url('admin/invoice/search/results')}}" method="GET">
                        <?php $year = date('Y');?>
                        <div class="form-control">{!! Form::selectRange('year', 2000, $year) !!}</div>
                            <div class="form-control">{!! Form::selectMonth('month') !!}</div>
                    </div>
                    <div class="modal-footer">
						<button type="submit"  class="btn btn-success"  />Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
               </form>
            </div>
        </div>

    </div>

    <!--modal end-->


    <div class="row">
        <div class="col-lg-12">
            <div class="box ">
                <div class="row">
                    <div class="col-md-6 col-lg-9">
                        <div class="box-head">
                            <header>
                                <h2 class="text-light" style="padding-top:20px; padding-bottom:5px;"><i
                                            class="fa fa-pencil fa-fw"></i>Partner Profile </h2>
                            </header>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pull-right" style="margin-top:35px; margin-right:30px;">
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#myModal" style=" text-decoration:none;"><i
                                        class="fa fa-sign-in"></i> Generate Invoice
                            </button>






                        </div>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <div class="section-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="box style-transparent">
                                    <div class="box-head">
                                        <ul class="nav nav-tabs tabs-transparent" data-toggle="tabs">
                                            <li class="active"><a href="#overview"><i class="fa fa-inbox"></i>
                                                    Overview</a></li>
                                            <li><a href="#editDetails"><i class="fa fa-edit"></i> Change details</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- START PROFILE -->
                                    <div class="profile-main-controller">
                                        <div class="box-tiles style-white">

                                            <!-- START PROFILE TABS -->

                                            <!-- END PROFILE TABS -->

                                            <div class="tab-content">

                                                <!-- START PROFILE OVERVIEW -->
                                                <div class="tab-pane active" id="overview">
                                                    @include('admin.operator.partials.profile')
                                                </div>
                                                <!--end .tab-pane -->
                                                <!-- END PROFILE OVERVIEW -->

                                                <!-- START PROFILE EDITOR -->
                                                <div class="tab-pane" id="editDetails">
                                                    @include('admin.operator.partials.form')
                                                </div>
                                                <!--end .tab-content -->

                                            </div>
                                            <!--end .row -->
                                        </div>
                                        <!--end .box-body -->

                                        <!-- END PROFILE OVERVIEW -->

                                    </div>
                                    <!--end .box -->
                                </div>
                                <!--end .col-lg-12 -->
                            </div>
                            <!--end .row -->
                        </div>
                    </div>
                    <!--end .box-body -->
                </div>
                <!--end .box -->
            </div>
            <!--end .col-lg-12 -->
        </div>
        <!--end .row -->


    </div>



@stop

@section('js')
    <script type="text/javascript">
        $(function () {
            console.log($('#phone').val());

            $('.tax_rate').hide();
            var tax = $("input[name=tax_radio]:checked").val();
            if(tax=='on')
            {
                $('.tax_rate').show();
            }



            $('.interest_rate').hide();
            var interest = $("input[name=interest_radio]:checked").val();

            if(interest=='on')
            {
                $('.interest_rate').show();
            }


            $('input:radio[name="tax_radio"]').change(function () {
                if ($(this).val() == 'on') {
                    $('.tax_rate').show();
                }
                else {
                    $('.tax_rate').hide();
                }
            });

            $('input:radio[name="interest_radio"]').change(function () {
                if ($(this).val() == 'on') {
                    $('.interest_rate').show();
                }
                else {
                    $('.interest_rate').hide();
                }
            });


            $("#banks_button").click(function () {
                        var bankLen = $('.child-banks').length;
                        var bank_section = '<div class="row child-banks"> ' +
                                        ' <div class="col-md-6"> ' +
                                        '<div class="form-group">' +
                                        '   <label class="control-label">Name</label> ' +

                                        '  <input type="text" name="bank_name[]"' +
                                        '        class="form-control"' +
                                        '        value=""> ' +

                                        ' </div>' +
                                        '</div>' +
                                        '<div class="col-md-6"> ' +
                                        ' <div class="form-group"> ' +

                                        '    <div class="col-lg-9  col-md-9 col-sm-9 col-xs-9"> ' +
                                        '         <label class="control-label">Account ' +
                                        '            Number</label> ' +
                                        '       <input type="text" name="account_number[]"' +
                                        '             class="form-control " ' +
                                        '        value=""> ' +

                                        ' </div> ' +
                                        ' <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"> ' +


                                        ' <label class="radio-inline btn btn-primary" ' +
                                        '        style=" margin-top:24px; padding:6px 7px; background:#fff; color:#333; border:1px solid #CCC;"> ' +
                                        '    <input type="radio" name="activated[]" ' +
                                        '          class="is_active" checked value="' + bankLen + '"       ' +
                                        '         style="margin-left:1px;">&nbsp;' +
                                        ' Activated' +
                                        ' </label>' +

                                        ' </div>' +


                                        ' </div> ' +

                                        ' </div>' +
                                        '</div>'


                                ;

                        console.log(bankLen);
                        $('.banks').append(bank_section);
                    }
            );


            $("#demo").intlTelInput();
        });
    </script>
    <style>
        input, button, select, textarea {
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
            width: 120px;
        }
		.btn-primary {
			color: #ffffff;
			background-color: #4a95b1;
			border-color: #42869f;
			width: 144px
    </style>
@stop