@extends('layouts.admin.boostbox.index')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-outlined">
                <div class="box-head">
                    <header><h4 class="text-light">Operator types <strong>table</strong></h4></header>
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.operator-types.create') }}">
                        <i class="fa fa-aw fa-plus-circle"></i> Add New Operator Type
                    </a>
                </div>
                <div class="box-body">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <td>Sn</td>
                            <td>Operator Name</td>
                            <td>Slug</td>
                            <td>Added On</td>
                            <td class="pull-right">Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @if($operatorsTypes->count() > 0)
                            @foreach($operatorsTypes as $k => $operator)
                                <?php $count = ($operatorsTypes->perPage() * ($operatorsTypes->currentPage() - 1)); ?>
                                <tr>
                                    <td>{{$count ? ($count + $k + 1) : ++$k }}</td>
                                    <td>{{ $operator->name }}</td>
                                    <td>{{ $operator->slug }}</td>
                                    <td>{{date('F d Y',strtotime($operator->created_at))}}</td>


                                    <td class="text-right">
                                        <a href="{{ route('admin.operator-types.edit', [$operator->id]) }}"
                                           class="btn btn-xs btn-default btn-equal"
                                           data-toggle="tooltip"
                                           data-placement="top" data-original-title="Edit row">
                                            <i class="fa fa-pencil"></i>
                                        </a>

                                        {!! delete_form(route('admin.operator-types.destroy', [$operator->id]), '', 'btn
                                        btn-xs btn-default btn-equal', 'Delete row') !!}
                                    </td>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">No Operator Types Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    {!! $operatorsTypes->appends($_GET)->links() !!}
                </div>
            </div>
@stop