@extends('layouts.admin.boostbox.index')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="box box-outlined">
                <div class="box-head">
                    <header><h4 class="text-light"><i class="fa fa-pencil fa-fw"></i> Add <strong>Operator type
                            </strong></h4></header>
                </div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    {!! Form::open(['route' => 'admin.operator-types.store', 'class' => 'form-horizontal form-bordered
                        form-banded','files' => true])
                    !!}
                    @include('admin.operator.operator-type.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop
