<div class="box-body style-white">
    <form class=""
          action="{{ route('admin.operator.update', [$operator->id]) }}"
          accept-charset="utf-8" method="post"
          enctype="multipart/form-data" 
          >

        {!! csrf_field() !!}
        {{ method_field('patch') }}

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="title">Basic Info</div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Name</label>
                    <input type="text" id="name" name="name" class="form-control"
                           value="{{ isset($operator->name) ? $operator->name : '' }}" readonly>
                    <span for="name" class="help-block">{{ $errors->first('name') }}</span>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Logo</label>
                    <input type="file" style="width:100%; margin-top:8px;" name="file" id="file" >
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label class="control-label">Operator Type</label>

                    <select name="operator_type_id" id="operator_type" class="form-control">
                        <option value="">Select Type
                        </option>
                        @if($operatorTypes->count() > 0)
                            @foreach($operatorTypes as $k => $operatorType)
                                <option value="{{$operatorType->id}}" selected="{{($operatorType->id==$operator->operator_type_id)?'selected': ''}}">{{$operatorType->name}}</option>
                            @endforeach
                        @endif
                    </select>
                    <span for="operator_type" class="help-block">{{ $errors->first('operator_type_id') }}</span>

                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Phone Number</label>

                    <div class="icon-addon addon-lg">
                        <input type="tel" placeholder="+977" name="phone_number" class="form-control" id="demo" value="{{  $operator->phone_number }}">
                        <input type="hidden" id="phone" value="{{  $operator->phone_number }}">
                    </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <div class="form-group">

                    <label class="control-label">Partner Since</label>

                    <div class='input-group date' id='demo-date-end'>
                        <input type="text" class="form-control" placeholder="5-23-2016"/>
                        <span class="input-group-addon" style="">
                            <i class="fa fa-calendar" style="color:#4A95B1;"></i></span>
                    </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">

                    <div class="col-lg-9  col-md-9 col-sm-9 col-xs-12">
                        <label class="control-label">Services</label>

                        <div class="row">
                            @if($services->count() > 0)
                                @foreach($services as $k => $service)
                                    <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
                                        <div class="row">

                                            <div class="col-lg-1  col-md-1 col-sm-1 col-xs-12">
                                                <div data-toggle="buttons">
                                                    <label class="btn checkbox-inline btn-checkbox-primary {{in_array($service->id,$serviceIdLists)?'active':''}}">
                                                        <input type="checkbox" value="{{$service->id}}"
                                                               name="services[]">
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-8  col-md-8 col-sm-8 col-xs-12">
                                                <label class="control-label checkbox">{{$service->name}}</label>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="title"
                             style=" padding-top:20px;">Contact
                            Person
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Name</label>
                        <input type="text" value="{{ isset($operator->contact_person_name) ? $operator->contact_person_name : '' }}" name="contact_person_name" class="form-control" placeholder="Enter Contact Person Name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Designation</label>
                        <input type="text" value="{{ isset($operator->contact_person_designation) ? $operator->contact_person_designation : '' }}" name="contact_person_designation" class="form-control" placeholder="Enter Contact Person Designation">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Phone</label>
                        <input type="text" value="{{ isset($operator->contact_person_phone_number) ? $operator->contact_person_phone_number : '' }}" name="contact_person_phone_number" class="form-control" placeholder="Enter Contact Person Phone Number">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Email Address</label>
                        <input type="text" value="{{ isset($operator->contact_person_email) ? $operator->contact_person_email : '' }}" name="contact_person_email" class="form-control" placeholder="Enter Contact Person Email Address">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="title" style=" padding-top:20px;">Bank Details</div>
                    </div>
                </div>
            </div>

            <div class="banks">
                @if($banks->count() > 0)
                    @foreach($banks as $k => $bank)
                        <div class="row child-banks">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <input type="text" name="bank_name[]" class="form-control"
                                           value="{{ isset($bank->name) ? $bank->name : '' }}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">

                                    <div class="col-lg-9  col-md-9 col-sm-9 col-xs-9">
                                        <label class="control-label">Account
                                            Number</label>
                                        <input type="text"
                                               name="account_number[]"
                                               class="form-control "
                                               value="{{ isset($bank->account_number) ? $bank->account_number : '' }}">

                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

                                        <input type="hidden"
                                               name="is_active">
                                        <label class="radio-inline btn btn-primary"
                                               style=" margin-top:24px; padding:6px 7px; background:#fff; color:#333; border:1px solid #CCC;">
                                            <input type="radio"
                                                   name="activated[]"
                                                    {{($bank->active==1)?'checked':' '}}
                                                   value="{{$k}}"
                                                   style="margin-left:1px;">&nbsp;
                                            Activated
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="pull-right"
                             style="margin-top:0px; margin-right:10px;">
                            <button type="button"
                                    class="btn btn-primary"
                                    data-toggle="modal"
                                    id="banks_button"
                                    data-target=".bs-example-modal-lg"
                                    style=" text-decoration:none; font-size:9px;">
                                Add More +
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="title" 
                             style=" padding-top:20px;">Address
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">

                        <label class="control-label">Country</label>
                        <input type="text" name="country" class="form-control" value="{{ isset($operator->country ) ? $operator->country : '' }}">

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">State</label>
                        <input type="text" name="state" value="{{ isset($operator->state) ? $operator->state : '' }}"
                               class="form-control ">

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">

                        <label class="control-label">City</label>


                        <input type="text" name="city" class="form-control" value="{{ isset($operator->city) ? $operator->city : '' }}">

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Street
                            Address</label>
                        <input type="text" name="street_address"
                               class="form-control " value="{{ isset($operator->street_address) ? $operator->street_address : '' }}">

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="title"
                             style=" padding-top:20px;">Config
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2 col-sm-2">
                            <div class="form-group">

                                <label class="control-label">Tax</label>


                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <input type="radio" name="tax_radio"
                                   value="on"
                                   {{ isset($operator->tax_rate)&&($operator->tax_rate!=0)? 'checked' : '' }}
                                   style="margin-left:1px;"> Yes
                            &nbsp;&nbsp;
                            <input type="radio"
                                   name="tax_radio"
                                   value="off"
                                   {{ !isset($operator->tax_rate)||($operator->tax_rate==0)? 'checked' : '' }}
                                   style="margin-left:1px;">
                            No
                        </div>

                    </div>

                </div>

            </div>

          

            <div class="row tax_rate">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Tax
                                    rate</label>

                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" name="tax_rate"
                                       value="{{ isset($operator->tax_rate) ? $operator->tax_rate : '' }}"
                                       class="form-control ">
                            </div>
                        </div>
                        
                        <div class="col-md-2"
                             style="margin-left:-12px;">
                            <div class="form-group"
                                 style="font-size:14px; padding:5px 0 0 0;">
                                %Month
                            </div>
                        </div>
                    </div>
                </div>


            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2 col-sm-2">
                            <div class="form-group">

                                <label class="control-label">Interest</label>


                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <input type="radio"
                                   name="interest_radio"
                                   value="on"
                                   {{ isset($operator->interest) &&($operator->interest!=0)? 'checked' : '' }}
                                   style="margin-left:1px;"> Yes
                            &nbsp;&nbsp;<input type="radio"
                                               name="interest_radio"
                                               {{ !isset($operator->interest) ||($operator->interest==0)? 'checked' : '' }}
                                               value="off" 
                                               style="margin-left:1px;">
                            No
                        </div>

                    </div>

                </div>

            </div>

            <div class="row interest_rate">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label">Interest</label>

                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" name="interest"
                                       value="{{ isset($operator->interest) ? $operator->interest : '' }}"
                                       class="form-control ">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group"
                                 style="font-size:14px; padding:5px 0 0 0;">
                                %
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"> Submit</button>
                        <a href="{{ route('admin.operator.edit',[$operator->id]) }}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </div>


        </div>
    </form>
    <!--end .box-body -->
</div>