<div class="box-tiles style-white">
    <div class="row">

        <!-- START PROFILE SIDEBAR -->
        <div class="col-sm-3 style-inverse">
            <div class="holder">
                <img class="img-rounded img-responsive" src="{{ image_url($operator->logo) }}" alt=""/>
            </div>
            <div class="box-body style-inverse">
                <p class="text-support5-alt"><span
                            class="text-xl text-light">{{ $operator->name }}</span><br/>
                    <span class="text-sm">{{ $operator->operatorType }}</span>
                </p>
            </div>
            <div class="box-body-darken style-inverse">
                <ul class="nav nav-pills nav-stacked nav-transparent">
                    <li>
                        <a href="{{ route('admin.operator.{id}.contracts.index', [$operator->id]) }}">
                            <span class="badge pull-right">{{ $operator->contracts()->count() }}</span>Contracts
                        </a>
                    </li>
                    <li><a href="#">Invoices</a></li>
                    <li>
                        <a href="#">
                            <span class="badge pull-right">3</span>Invoices
                        </a>
                    </li>
                </ul>
            </div>
            <div class="box-body style-inverse">
                <address class="text-support5-alt">
                    <strong>{{ $operator->street_name }}</strong><br>
                    {{ $operator->city }}, {{ $operator->state }}, {{ $operator->country }}
                </address>
                <address class="text-support5-alt">
                    <abbr title="Phone"><i
                                class="fa fa-phone fa-fw"></i></abbr>
                    {{ $operator->phone_number }}<br>
                    <abbr title="Birthday"><i
                                class="fa fa-gift fa-fw"></i></abbr>
                    {{ formatDate($operator->partner_since) }}
                </address>
            </div>
        </div>
        <!--end .col-sm-3 -->
        <!-- END PROFILE SIDEBAR -->

        <!-- START PROFILE CONTENT -->
        <div class="col-sm-9">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8">
                        <p class="lead">Nullam quis risus eget
                            urna mollis ornare vel eu leo. Cum
                            sociis natoque penatibus et magnis
                            dis parturient montes.</p>

                        <p>Cum sociis natoque penatibus et
                            magnis dis parturient montes,
                            nascetur ridiculus mus. Donec
                            ullamcorper nulla non metus auctor
                            fringilla. Duis mollis, est non
                            commodo luctus, nisi erat porttitor
                            ligula, eget lacinia odio sem nec
                            elit.</p>
                    </div>
                    <div class="col-sm-4">
                        <div class="pie-chart flot text-center">
                            <div class="chart size-3 v-inline-middle"
                                 data-title="Site visits"
                                 data-color="#FBEED5,#2E383D"></div>
                            <div class="legend v-inline-middle text-left"></div>
                        </div>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <!-- Extra row gap-->
                <div class="row">
                    <div class="col-md-8">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                data-original-title="Edit row">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                data-original-title="Delete row">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jacob</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                data-original-title="Edit row">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                data-original-title="Delete row">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Larry</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                data-original-title="Edit row">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                data-original-title="Delete row">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--end .table-responsive -->
                    </div>
                    <!--end .col-sm-8 -->
                    <div class="col-md-4">
                        <ul class="list-group">
                            <li class="list-group-item"><span
                                        class="badge">$ 300</span>
                                Sales Today
                            </li>
                            <li class="list-group-item"><span
                                        class="badge">$ 2.100</span>
                                Sales this week
                            </li>
                            <li class="list-group-item"><span
                                        class="badge">$ 198.000</span>
                                Total sales
                            </li>
                        </ul>
                    </div>
                    <!--end .col-sm-4 -->
                </div>
                <!--end .row -->
            </div>
        </div>
        <!--end .col-sm-9 -->
        <!-- END PROFILE CONTENT -->

    </div>
    <!--end .row -->
</div>
<!--end .box-body -->