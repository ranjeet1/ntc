@extends('layouts.admin.boostbox.index')

@section('content')

    <div class="box">

        <div class="box-head">
            <header>
                <h2 class="text-light" style="padding-top:20px; padding-bottom:5px;"><i class="fa fa-pencil fa-fw"></i>Contract
                    Profile </h2>
            </header>
        </div>

        <div class="box-body">
            <div class="row">


                <div class="col-md-4">
                    <form class="form-horizontal" action="#">
                        <div class="form-group">
                            <label for="title" class="col-sm-5 control-label">Start Date:</label>

                            <div class="col-sm-7">
                                {{ formatDate($contract->start_date) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-5 control-label">End Date:</label>

                            <div class="col-sm-7">
                                {{ formatDate($contract->end_date) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-5 control-label">Interest:</label>

                            <div class="col-sm-7">
                                {{ $contract->interest_rate ? $contract->interest_rate. ' %' : 'NA' }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-5 control-label">Tax:</label>

                            <div class="col-sm-7">
                                coming soon..
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-5 control-label">Terms and Conditions:</label>

                            <div class="col-sm-7">
                                coming soon..
                            </div>
                        </div>

                    </form>
                </div>

                <div class="col-md-8">

                    <div class="row">
                        <div class="col-sm-12">
                            <p class="lead">Remark:</p>

                            <p>{{ $contract->remark }}</p>
                        </div>

                    </div>

                    <div class="row">&nbsp;</div>
                    <!-- Extra row gap-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Tier</th>
                                        <th>Ceiling</th>
                                        <th>Rate</th>
                                        <th>Amount</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($rateProfiles->count() > 0)
                                        @foreach($rateProfiles as $key => $rateProfile)
                                            <tr>
                                                <td>{{ $key == 0 ? $rateProfile->tier->fullName : '' }}</td>
                                                <td>{{ $rateProfile->ceiling }}</td>
                                                <td>{{ $rateProfile->rate ? '$' . $rateProfile->rate . ' / min' : 'NA' }}</td>
                                                <td>{{ $rateProfile->amount ? '$' . $rateProfile->amount : 'NA' }}</td>
                                                <td>
                                                    @if($key == 0)
                                                        <button type="button"
                                                                class="btn btn-xs btn-inverse btn-equal"
                                                                data-toggle="tooltip"
                                                                data-placement="top"
                                                                data-original-title="Edit">
                                                            <i class="fa fa-pencil"></i>
                                                        </button>
                                                        <button type="button"
                                                                class="btn btn-xs btn-inverse btn-equal"
                                                                data-toggle="tooltip"
                                                                data-placement="top"
                                                                data-original-title="Delete">
                                                            <i class="fa fa-trash-o"></i>
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">
                                                
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!--end .table-responsive -->
                        </div>

                    </div>
                    <!--end .row -->

                </div>

            </div>
        </div>


    </div>
@stop
