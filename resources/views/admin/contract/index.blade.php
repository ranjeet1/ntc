@extends('layouts.admin.boostbox.index')

@section('content')


	<div class="row">
		<div class="col-lg-12">
			<div class="box box-outlined">
				<div class="box-head">
					<header><h4 class="text-light">Contract <strong>table</strong></h4></header>
					<a class="btn btn-sm btn-primary" href="{{ route('admin.operator.{id}.contracts.create',
					[$operator->id]) }}">
					<i class="fa fa-aw fa-plus-circle"></i> Add New Contract
					</a>



				</div>
				<div class="box-body">

					<table class="table table-responsive">
						<thead>
						<tr>
							<td>Sn</td>
							<td>Start Date</td>
							<td>End Date</td>
							<td>Contract Types</td>
							<td>Operator Name</td>
							<td>Interest Rate</td>
							<td>Tax Included</td>
							<td>Added On</td>

						</tr>
						</thead>
						<tbody>

						@if(isset($contracts)&&count($contracts)>0)
							@foreach($contracts as $k =>$contract)
								<?php $count = ($contracts->perPage() * ($contracts->currentPage() - 1)); ?>

								<tr>
									<td>{{$count ? ($count + $k + 1) : ++$k }}</td>
									<td>{{ $contract->start_date}}</td>
									@if(!empty($contract->end_date))
										<td>{{$contract->end_date}}</td>
									@else
										<td><?php $time = strtotime($contract->start_date.'-1 days');
											echo $date = date("Y-m-d", $time);?></td>
									@endif


									@if(empty($contract->end_date))
										<td>Open</td>
									@else
										<td>Closed</td>
									@endif
									{{--<td>--}}
									{{--<a href="{{asset('termsConditions/Files')}}/{{ $contract->terms_and_conditions}}"--}}
									{{--download="{{ $contract->terms_and_conditions}}" target="_blank">Download</a>--}}
									{{--</td>--}}
									<td>{{$contract->operator->name}}</td>
									<td>{{$contract->interest_rate}}</td>
									<td>{{$contract->tax_included}}</td>
									<td>{{formatDate($contract->created_at,'Y-m-d')}}</td>
									<td>

									<td class="text-right">


                                        {{--<a href="{{ route('admin.contracts.{id}.rate-profile.index', [$contract->id]) }}"--}}
                                           {{--class="btn btn-xs btn-default btn-equal"--}}
                                           {{--data-toggle="tooltip"--}}
                                           {{--data-placement="top" data-original-title="View Rate Profiles">--}}
                                            {{--<i class="fa fa-money" aria-hidden="true"></i>--}}
                                        {{--</a>--}}

                                        <a href="{{ route('admin.operator.{id}.contracts.show', [$operator->id, $contract->id]) }}"
                                           class="btn btn-xs btn-default btn-equal"
                                           data-toggle="tooltip"
                                           data-placement="top" data-original-title="View Detail">
                                            <i class="fa fa-money" aria-hidden="true"></i>
                                        </a>


										<a href="{{ route('admin.operator.{id}.contracts.edit', [$contract->operator_id,$contract->id]) }}"
										   class="btn btn-xs btn-default btn-equal"
										   data-toggle="tooltip"
										   data-placement="top" data-original-title="Edit Contract">
											<i class="fa fa-pencil"></i>
										</a>

										{!! delete_form(route('admin.contracts.destroy', [$contract->id]), '', 'btn btn-xs btn-default btn-equal', 'Delete Contract') !!}
									</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="10">No contracts to show</td>
							</tr>
						@endif
						</tbody>
					</table>
					{!!$contracts->render()!!}
				</div>
			</div>
@stop