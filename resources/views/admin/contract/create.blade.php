@extends('layouts.admin.boostbox.index')

@section('content')

    <div class="box box-outlined">
        <div class="box-head">
            <header>
                <h4 class="text-light">
                    <i class="fa fa-pencil fa-fw"></i> Add <strong>New Contract</strong>
                </h4>
            </header>
        </div>

        <div class="box-body">
            {!! Form::open(['url' => route('admin.operator.{id}.contracts.store',[$operator->id]) ,
            'class' => 'form-horizontal form-bordered','files' => true]) !!}
            @include('admin.contract.form')
            {!! Form::close() !!}
        </div>
    </div>

@stop
