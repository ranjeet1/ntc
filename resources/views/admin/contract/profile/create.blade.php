@extends('layouts.admin.boostbox.index')

@section('content')



	<!-- BEGIN CONTENT-->

	<div id="content">
		<ol class="breadcrumb">
			<li><a href=""> Setting</a></li>
			<li class="active">partner Profile</li>
		</ol>
		<div class="section-body">

			<!-- BEGIN BASIC FORM INPUTS -->
			<div class="row">
				<div class="col-lg-12">
					<div class="box ">

						<div class="row">
							<div class="col-md-6 col-lg-9">
								<div class="box-head">
									<header>
										<h2 class="text-light" style="padding-top:20px; padding-bottom:5px;"><i class="fa fa-pencil fa-fw"></i>Partner Profile </h2>


									</header>
								</div>
							</div>
							<div class="col-md-3">
								<div class="pull-right" style="margin-top:35px; margin-right:30px;">

									<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" style=" text-decoration:none;"><i class="fa fa-sign-in"></i> Generate Invoice</button>

								</div>
							</div>

						</div>




						<div class="box-body no-padding">
							<div class="section-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="box style-transparent">
											<div class="box-head">
												<ul class="nav nav-tabs tabs-transparent" data-toggle="tabs">
													<li class="active"><a href="#overview"><i class="fa fa-inbox"></i> Overview</a></li>
													<li><a href="#editDetails"><i class="fa fa-edit"></i> Change details</a></li>
												</ul>
											</div>
											<!-- START PROFILE -->
											<div class="profile-main-controller">
												<div class="box-tiles style-white">

													<!-- START PROFILE TABS -->

													<!-- END PROFILE TABS -->

													<div class="tab-content">

														<!-- START PROFILE OVERVIEW -->
														<div class="tab-pane active" id="overview">
															<div class="box-tiles style-white">
																<div class="row">

																	<!-- START PROFILE SIDEBAR -->
																	<div class="col-sm-3 style-inverse">
																		<div class="holder"> <img class="img-rounded
																		img-responsive"
																								  src="{{asset('ntc-final-theme/assets/img/modules/boostbox/img8b6e5.jpg?1401441858')}}" alt="" />

																		</div>
																		<div class="box-body style-inverse">
																			<p class="text-support5-alt"> <span class="text-xl text-light">Daniel Johnson</span><br/>
																				<span class="text-sm">Consultant at CodeCovers</span> </p>
																		</div>
																		<div class="box-body-darken style-inverse">
																			<ul class="nav nav-pills nav-stacked nav-transparent">
																				<li><a href="#"><span class="badge pull-right">42</span>Projects</a></li>
																				<li><a href="#">Friends</a></li>
																				<li><a href="#"><span class="badge pull-right">3</span>Messages</a></li>
																			</ul>
																		</div>
																		<div class="box-body style-inverse">
																			<address class="text-support5-alt">
																				<strong>Daniel Johnson, Inc.</strong><br>
																				621 Johnson Ave, Suite 600<br>
																				San Francisco, CA 54321
																			</address>
																			<address class="text-support5-alt">
																				<abbr title="Phone"><i class="fa fa-phone fa-fw"></i></abbr> (123) 456-7890<br>
																				<abbr title="Birthday"><i class="fa fa-gift fa-fw"></i></abbr> January 15, 1976
																			</address>
																		</div>
																	</div>
																	<!--end .col-sm-3 -->
																	<!-- END PROFILE SIDEBAR -->

																	<!-- START PROFILE CONTENT -->
																	<div class="col-sm-9">

																		<div class="box-body">
																			<div class="row">
																				<div class="col-sm-8">
																					<p class="lead">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
																					<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
																				</div>
																				<div class="col-sm-4">
																					<div class="pie-chart flot text-center">
																						<div class="chart size-3 v-inline-middle" data-title="Site visits" data-color="#FBEED5,#2E383D"></div>
																						<div class="legend v-inline-middle text-left"></div>
																					</div>
																				</div>
																			</div>
																			<div class="row">&nbsp;</div>
																			<!-- Extra row gap-->
																			<div class="row">
																				<div class="col-md-8">
																					<div class="table-responsive">
																						<table class="table table-bordered">
																							<thead>
																							<tr>
																								<th>First Name</th>
																								<th>Last Name</th>
																								<th>Username</th>
																								<th>Actions</th>
																							</tr>
																							</thead>
																							<tbody>
																							<tr>
																								<td>Mark</td>
																								<td>Otto</td>
																								<td>@mdo</td>
																								<td><button type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Edit row"><i class="fa fa-pencil"></i></button>
																									<button type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Delete row"><i class="fa fa-trash-o"></i></button></td>
																							</tr>
																							<tr>
																								<td>Jacob</td>
																								<td>Thornton</td>
																								<td>@fat</td>
																								<td><button type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Edit row"><i class="fa fa-pencil"></i></button>
																									<button type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Delete row"><i class="fa fa-trash-o"></i></button></td>
																							</tr>
																							<tr>
																								<td>Larry</td>
																								<td>the Bird</td>
																								<td>@twitter</td>
																								<td><button type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Edit row"><i class="fa fa-pencil"></i></button>
																									<button type="button" class="btn btn-xs btn-inverse btn-equal" data-toggle="tooltip" data-placement="top" data-original-title="Delete row"><i class="fa fa-trash-o"></i></button></td>
																							</tr>
																							</tbody>
																						</table>
																					</div>
																					<!--end .table-responsive -->
																				</div>
																				<!--end .col-sm-8 -->
																				<div class="col-md-4">
																					<ul class="list-group">
																						<li class="list-group-item"> <span class="badge">$ 300</span> Sales Today </li>
																						<li class="list-group-item"> <span class="badge">$ 2.100</span> Sales this week </li>
																						<li class="list-group-item"> <span class="badge">$ 198.000</span> Total sales </li>
																					</ul>
																				</div>
																				<!--end .col-sm-4 -->
																			</div>
																			<!--end .row -->
																		</div>
																	</div>
																	<!--end .col-sm-9 -->
																	<!-- END PROFILE CONTENT -->

																</div>
																<!--end .row -->
															</div>
															<!--end .box-body -->
														</div>
														<!--end .tab-pane -->
														<!-- END PROFILE OVERVIEW -->

														<!-- START PROFILE EDITOR -->
														<div class="tab-pane" id="editDetails">
															<div class="box-body style-white">

																<form class="form-horizontal " action="" accept-charset="utf-8" method="post">
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				<label class="control-label">First Name</label>
																				<input type="text" class="form-control ">
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																				<label class="control-label">Last Name</label>
																				<input type="text" class="form-control ">
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				<label class="control-label">Service</label>
																				<select name="operatorservice" id="operatorservice" class="form-control" style="border: 1px solid #ccc; padding: 5px 3px; width:100%;">
																					<option value="Select">Select Service</option>
																					<option value="Multiple">Multiple</option>
																					<option value="Bilateral">Bilateral</option>
																				</select>
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																				<label class="control-label">Type</label>
																				<select name="operatortype" id="operatortype" class="form-control" style="border: 1px solid #ccc; padding: 5px 3px; width:100%;">
																					<option value="Select">Select Type</option>
																					<option value="International">International</option>
																					<option value="Domastic">Domastic</option>
																				</select>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group">
																				<label class="control-label">Contact Details</label>
																				<textarea class="form-control" rows="3" placeholder="Write your details here"></textarea>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group">
																				<label class="control-label">Bank Details</label>
																				<textarea class="form-control" rows="3" placeholder="Write your Bank details here"></textarea>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				<div data-toggle="buttons">
																					<label class="btn checkbox-inline btn-checkbox-primary"> <span style="float:left; margin-right:5px; color:#000; font-weight:600;">Tax Applied </span>
																						<input type="checkbox" value="primary2">
																					</label>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-12">
																			<div class="form-group">
																				<label class="control-label" style="font-size:16px; margin-top:10px 0 0 0;">Contact Person</label>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				<label class="control-label">Name</label>
																				<input type="text" class="form-control ">
																			</div>
																		</div>

																		<div class="col-md-6">
																			<div class="form-group">
																				<label class="control-label">Designation</label>
																				<input type="text" class="form-control ">
																			</div>
																		</div>


																	</div>

																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				<label class="control-label">Phone No.</label>
																				<input type="text" class="form-control ">
																			</div>
																		</div>

																		<div class="col-md-6">
																			<div class="form-group">
																				<label class="control-label">Email Address</label>
																				<input type="text" class="form-control ">
																			</div>
																		</div>


																	</div>
																	<button type="submit" class="btn btn-primary"> Submit </button>
																</form>
															</div>
															<!--end .box-body -->
														</div>
														<!--end .tab-pane -->
														<!-- END PROFILE EDITOR -->

													</div>
													<!--end .tab-content -->

												</div>
												<!--end .row -->
											</div>
											<!--end .box-body -->

											<!-- END PROFILE OVERVIEW -->

										</div>
										<!--end .box -->
									</div>
									<!--end .col-lg-12 -->
								</div>
								<!--end .row -->
							</div>
						</div>
						<!--end .box-body -->
					</div>
					<!--end .box -->
				</div>
				<!--end .col-lg-12 -->
			</div>
			<!--end .row -->

		</div>
	</div>

<style>
	#content {
		display: table-cell;
		vertical-align: top;
		padding-top: -31px;
	}
</style>
@stop