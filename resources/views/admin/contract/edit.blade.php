@extends('layouts.admin.boostbox.index')

@section('content')

    <div class="row">
        <div class="box box-outlined">
            <div class="box-head">
                <header><h4 class="text-light"><i class="fa fa-pencil fa-fw"></i> Edit <strong>Contract
                        </strong></h4></header>
            </div>
            <div class="box-body">
                {!! Form::model($contract,['url' => route('admin.operator.{id}.contracts.update',
                [$contract->operator_id,$contract->id]) , 'class'=>'form-horizontal',
                'method'=>'PATCH','files'=>true]) !!}
                @include('admin.contract.form')
                {!! Form::close() !!}

            </div>
            <!--end .box-body -->
        </div>
        <!--end .box -->
    </div><!--end .row -->
    <!-- END BASIC FORM INPUTS -->

@stop