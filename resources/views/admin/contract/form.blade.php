<div class="form-group">
    {!! Form::label('title', 'Start Date:', ['class'=>'col-sm-2 control-label'])!!}
    <div class="col-sm-5">
        <div class="input-group date" id="demo-date-end">
            <input type="text" name="start_date" value="{{ old('start_date', isset($contract->start_date) ? $contract->start_date : '') }}" class="form-control" id="start-date" placeholder="Select Contract Start Date">
            <span class="input-group-addon" style=""><i class="fa fa-calendar" style="color:#4A95B1;"></i></span>
        </div>
        <span class="help-block">{{ $errors->first('start_date') }}</span>
    </div>
</div>


<div class="form-group">
    {!! Form::label('title', 'End Date:', ['class'=>'col-sm-2 control-label'])!!}
    <div class="col-sm-5">
        <div class="input-group date" id="demo-date-end">
            <input type="text" name="end_date" value="{{ old('end_date', isset($contract->end_date) ? $contract->end_date : '') }}" class="form-control" id="end-date" placeholder="Select Contract End Date">
            <span class="input-group-addon" style=""><i class="fa fa-calendar" style="color:#4A95B1;"></i></span>
        </div>
        <span class="help-block">{{ $errors->first('end_date') }}</span>
    </div>
</div>

<div class="form-group">
    {!! Form::label('title', 'Terms and Conditions:', ['class'=>'col-sm-2 control-label'])!!}
    <div class="col-sm-5">
        {!! Form::file('terms_and_conditions',null, ["class"=>"form-control"])!!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('title', 'Interest:', ['class'=>'col-sm-2 control-label'])!!}

    <div class="col-sm-5">

        Yes <input type="radio" onclick="javascript:yesnoCheck();" name="interest" id="yesCheck" value="yes" checked>
        No <input type="radio" onclick="javascript:yesnoCheck();" name="interest" id="noCheck" value="no">
    </div>
</div>


<div class="form-group">
    <div id="ifYes" style="visibility:display">
        {!! Form::label('title', 'Interest Rate:', ['class'=>'col-sm-2 control-label'])!!}
        <div class="col-sm-5">
            {!! Form::text('interest_rate',null, ["class"=>"form-control","placeholder"=>"Interest rate of numeric
            value"]) !!}
            <div class="col-sm-1" style="float: right;margin-right: -50px;margin-top: -21px;">%
            </div>
            {!! $errors->first('interest_rate', '<span class="help-block">:message</span>') !!}

        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('title', 'Tax Included:', ['class'=>'col-sm-2 control-label'])!!}
    <div class="col-sm-5">

        {!!Form::select('size', array('yes' => 'yes', 'no' => 'no'), null, ['class'=>'form-control'])!!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('title', 'Remarks:', ['class'=>'col-sm-2 control-label'])!!}
    <div class="col-sm-5">
        {!! Form::textarea('remark',null, ["class"=>"form-control",'cols'=>'10','rows'=>'5','placeholder'=> 'Remarks for
        the contract'])!!}
    </div>
</div>


<div class="form-action">
    <div class="col-sm-5 col-lg-offset-2">
        <input type="submit" value="Submit" class="btn btn-primary">
        <a href="{{ route('admin.operator.{id}.contracts.index',[$operator->id ]) }}" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('js')

    <script type="text/javascript">
        $(function () {
            $("#start-date").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                onClose: function (selectedDate) {
                    $("#end-date").datepicker("option", "minDate", selectedDate);
                }
            });
            $("#end-date").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3,
                onClose: function (selectedDate) {
                    $("#start-date").datepicker("option", "maxDate", selectedDate);
                }
            });
        });

        function yesnoCheck() {
            if (document.getElementById('yesCheck').checked)
                document.getElementById('ifYes').style.visibility = 'visible';
            else
                document.getElementById('ifYes').style.visibility = 'hidden';

        }
    </script>

@stop
