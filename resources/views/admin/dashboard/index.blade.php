@extends('layouts.admin.boostbox.index')

@section('content')
    <div class="row">

        <!-- BEGIN LINE CHART -->
        <div class="col-md-9">

            <div class="row" style="margin:0 0px;">
                <div style="border:1px solid #4A95B1; background:#D9EDF7;">

                    <div class="col-md-3">
                        <div class="form-group" style="padding: 10px 0 0 0;">
                            <label for="sub" class="cats-option" style="font-size:15px !important; ">Category</label>
                            <select name="cat_operator" id="cat_operator" style="border: 2px solid #4A95B1; padding: 6px 10px; width:100%;">
                                <option value="Select">Select</option>
                                <option value="Domestic">Domestic</option>
                                <option value="International">International</option>

                            </select>



                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group" style="padding:10px 0 0 0;">
                            <label for="sub" class="cats-option" style="font-size:15px !important; ">Service Type</label>
                            <select name="cat_operator" id="cat_operator" style="border: 2px solid #4A95B1; padding: 6px 10px; width:100%;">
                                <option value="Select">Select</option>
                                <option value="Domestic">Domestic</option>
                                <option value="International">International</option>

                            </select>



                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group" style="padding: 10px 0 0 0;">
                            <label for="sub" class="cats-option" style="font-size:15px !important; ">Operator</label>
                            <select name="cat_operator" id="cat_operator" style="border: 2px solid #4A95B1; padding: 6px 10px; width:100%;">
                                <option value="Select">Select</option>
                                <option value="Domestic">Domestic</option>
                                <option value="International">International</option>

                            </select>



                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group" style="padding:10px 0 0 0;">
                            <label for="sub" class="cats-option" style="font-size:15px !important; ">I/O Calls</label>
                            <select name="cat_operator" id="cat_operator" style="border: 2px solid #4A95B1; padding: 6px 10px; width:100%;">
                                <option value="Select">Select</option>
                                <option value="Domestic">Domestic</option>
                                <option value="International">International</option>

                            </select>



                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>



            </div>

            <!--<div class="form-group" style="padding: 20px 0 0 0;">
                  <label for="sub" class="cats-option" style="font-size: 25px !important; ">Category</label>
                  <select name="cat_operator" id="cat_operator" style="width:300px !important; border: 2px solid #ccc; padding: 6px 10px;">
                   <option value="Select">Select</option>
    <option value="Domestic">Domestic</option>
      <option value="International">International</option>

    </select>



                </div>-->
            <div class="box style-primary">
                <div class="box-head">

                    <header>
                        <h3 class="text-light">Analytics <strong>O/P</strong></h3>
                        <small class="opacity-75">Your data here..</small>
                    </header>
                    <div class="tools stick-top-right">
                        <div class="btn-group btn-group-transparent">
                            <div class="btn-group">
                                <a href="#" class="btn btn-equal btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-flask"></i></a>
                                <ul class="dropdown-menu animation-dock pull-right menu-box-styling" role="menu" style="text-align: left;">
                                    <li><a href="javascript:void(0);" data-style="style-primary">Style primary</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-info">Style info</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-success">Style success</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-warning">Style warning</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-danger">Style danger</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-inverse">Style inverse</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support1">Style support 1</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support2">Style support 2</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support3">Style support 3</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support4">Style support 4</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support5">Style support 5</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support1-gradient">Style support-gradient 1</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support2-gradient">Style support-gradient 2</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support3-gradient">Style support-gradient 3</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support4-gradient">Style support-gradient 4</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-support5-gradient">Style support-gradient 5</a></li>
                                    <li><a href="javascript:void(0);" data-style="style-primary-gradient">Style primary-gradient</a></li>
                                </ul>
                            </div>
                            <a class="btn btn-equal btn-sm btn-refresh"><i class="fa fa-refresh"></i></a>
                            <a class="btn btn-equal btn-sm btn-collapse"><i class="fa fa-angle-down"></i></a>
                            <a class="btn btn-equal btn-sm btn-close"><i class="fa fa-times"></i></a>
                        </div>

                    </div>
                </div>
                <div class="box-body">
                    <div id="visitor-chart" class="flot height-5" data-title="Site visits" data-color="#ffffff,#ff9c00"></div>
                </div>
                <div class="box-body style-body">
                    <div class="col-sm-5">
                        <div class="pie-chart flot">
                            <div class="chart size-3 v-inline-middle" data-title="Site visits" data-color="#397085,#ff9c00"></div>
                            <div class="legend v-inline-middle"></div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <br/><br/>
                        <div class="width-5 pull-right">
                            <div class="pull-left">
                                <span class="text-sm text-muted">My balans &nbsp;&nbsp;</span><br/>
                                <span class="text-bold">33,896.50 &nbsp;&nbsp;</span>
                            </div>
                            <span class="inlinesparkline" data-type="bar" data-height="33" data-bar-color="#397085">3,2,1,11,9,5,3,4,2,6,5,2,6,2</span>
                        </div>
                        <div class="width-5 pull-right">
                            <div class="pull-left">
                                <span class="text-sm text-muted">New Orders &nbsp;&nbsp;</span><br/>
                                <span class="text-bold">17,555 &nbsp;&nbsp;</span>
                            </div>
                            <span class="inlinesparkline" data-type="bar" data-height="33" data-bar-color="#ff9c00">6,5,2,6,2,4,5,1,3,2,1,11,9,5</span>
                        </div>
                    </div>
                </div>
            </div><!--end .box -->
        </div><!--end .col-lg-6 -->
        <!-- END LINE CHART -->

        <!-- BEGIN DONUT CHART -->
        <div class="col-md-3">
            <div class="box style-body text-center">


                <div class="card-body no-padding">
                    <div class="alert alert-callout alert-info no-margin">
                        <strong class="pull-right text-success text-lg">0,38% <i class="md md-trending-up"></i></strong>
                        <strong class="text-xl">$ 32,829</strong><br/>
                        <span class="opacity-50">Revenue</span>
                        <div class="stick-bottom-left-right">
                            <div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"></div>
                        </div>
                    </div>
                </div><!--end .card-body -->





            </div><!--end .box -->

            <div class="box style-body text-center">


                <div class="card-body no-padding">
                    <div class="alert alert-callout alert-info no-margin" style="background:#4A95B1; color:#FFF;">
                        <strong class="pull-right text-success text-lg">0,01% <i class="md md-trending-up"></i></strong>
                        <strong class="text-xl">$ 432,901</strong><br/>
                        <span class="opacity-50">Trend Analysis</span>
                        <div class="stick-bottom-left-right">
                            <div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"></div>
                        </div>
                    </div>
                </div><!--end .card-body -->





            </div><!--end .box -->

            <div class="box style-body text-center">


                <div class="card-body no-padding">
                    <div class="alert alert-callout alert-info no-margin">
                        <strong class="pull-right text-success text-lg">0,38% <i class="md md-trending-up"></i></strong>
                        <strong class="text-xl">$ 42.90</strong><br/>
                        <span class="opacity-50">Dispute &amp; Settlement</span>
                        <div class="stick-bottom-left-right">
                            <div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"></div>
                        </div>
                    </div>
                </div><!--end .card-body -->





            </div><!--end .box -->

            <div class="box style-body text-center">


                <div class="card-body no-padding">
                    <div class="alert alert-callout alert-info no-margin" style="background:#4A95B1; color:#FFF;">
                        <strong class="pull-right text-success text-lg" style="">0,38% <i class="md md-trending-up"></i></strong>
                        <strong class="text-xl">$ 32,829</strong><br/>
                        <span class="opacity-50">P/L Account</span>
                        <div class="stick-bottom-left-right">
                            <div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"></div>
                        </div>
                    </div>
                </div><!--end .card-body -->





            </div><!--end .box -->
        </div><!--end .col-lg-3 -->
        <!-- END DONUT CHART -->

    </div>

@stop