@extends('layouts.admin.boostbox.index')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">General Setting</div>
        <div class="panel-body">
 
            {!! Form::open(['route' => 'admin.option.add', 'method' => 'post', 'files' => true, 'class' =>
                    'form-horizontal form-bordered form-banded']) !!}
             @include('admin.option.form')
            {!! Form::close() !!}
        </div>
    </div>
@stop
