<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="name" class="control-label"></label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <img src="{{ image_url(isset($options->logo) ? $options->logo : '') }}" alt="" height="120" width="150" />
    </div>
</div>

<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="file" class="control-label">Logo</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <input type="file" name="logo" id="logo" class="form-control">
        <span for="logo" class="help-block">{{ $errors->first('logo') }}</span>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="company_name" class="control-label">Company Name</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <input type="text" id="company_name" name="company_name" value="{{ old('company_name', isset($options->company_name) ? $options->company_name : '') }}" class="form-control" placeholder="Contact Address">
        <span for="company_name" class="help-block">{{ $errors->first('company_name') }}</span>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="name" class="control-label">Email Address</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <input type="text" id="email" name="email" value="{{ old('email', isset($options->email) ? $options->email : '') }}" class="form-control" placeholder="Contact Address">
        <span for="email" class="help-block">{{ $errors->first('email') ? $errors->first('email') : 'This address is used for admin purposes, like new user notification.'}}</span>
    </div>
</div>


<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="contact_number" class="control-label">Contact Number</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <input type="text" id="contact_number" name="contact_number" value="{{ old('contact_number', isset($options->contact_number) ? $options->contact_number : '') }}" class="form-control" placeholder="Contact Address">
        <span for="contact_number" class="help-block">{{ $errors->first('contact_number') }}</span>
    </div>
</div>


<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="name" class="control-label">Date Format</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        @foreach($dateFormats as $key => $dateFormat)
            <label class="radio inline" title="{{ $key }}">
                <input type="radio" name="date_format"
                       value="{{ $key }}" {{ (isset($options->date_format) && old('date_format', $options->date_format) == $key) ? 'checked' : '' }}>
                {{ $dateFormat }}
            </label>

        @endforeach
    </div>
</div>


<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="name" class="control-label">Weeks Starts On</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        <select name="start_of_week" id="start_of_week" class="form-control">
            @foreach($week as $key => $day)
                <option value="{{ $key }}" {{ (isset($options->start_of_week) && old('start_of_week', $options->start_of_week) == $key) ? 'selected' : ''}}>{{ $day }}</option>
            @endforeach
        </select>
        <span class="help-block error">{{ $errors->first('start_of_week') }} </span>
    </div>
</div>


<div class="form-action">
    <div class="form-footer col-lg-offset-1 col-md-offset-2 col-sm-offset-6">
        <button type="submit" class="btn btn-primary"> Save Settings</button>
    </div>
</div>