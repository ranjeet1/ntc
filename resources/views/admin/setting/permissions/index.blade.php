@extends('layouts.admin.boostbox.index')

@section('content')

    <div class="row">

        <div class="col-md-12 col-lg-8 col-sm-12">

            <h3>Configure Notifications</h3>

            <ul>
                <h5>User Permissions</h5>
                <li>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <input type="checkbox">
                        </span>
                        <input type="text" class="form-control" placeholder="Checkbox addon">
                    </div>
                </li>
            </ul>

        </div>
    </div>

@stop