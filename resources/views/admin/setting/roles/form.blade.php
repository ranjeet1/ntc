<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="name" class="control-label">Name</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        {!! Form::text('name',null, ["class" => "form-control", 'required', "id" => "name", "placeholder" => "Name of Role"])!!}
        <span for="name" class="help-block">{{ $errors->first('name') }}</span>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="display_name" class="control-label">Display Name</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        {!! Form::text('display_name', null, ["class" => "form-control", 'required', "id" => "display_name", "placeholder" => "Display Name of Role"])!!}
        <span for="name" class="help-block">{{ $errors->first('display_name') }}</span>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="description" class="control-label">Short Description</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        {!! Form::textarea('description',null, ["class" => "form-control", 'required', "rows" => "3", "id" => "description", "placeholder" => "Description of Role"]) !!}
        <span for="name" class="help-block">{{ $errors->first('description') }}</span>
    </div>
</div>
<div class="form-footer col-lg-offset-3 col-md-offset-2 col-sm-offset-3">
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="{{ route('admin.roles.index') }}" class="btn btn-default">Cancel</a>
</div>