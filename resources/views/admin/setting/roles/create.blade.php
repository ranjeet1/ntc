@extends('layouts.admin.boostbox.index')

@section('content')
    <div class="row">
        <div class="box box-bordered">
            <div class="col-lg-6">
                <div class="box-head">
                    <header>
                        <h4 class="text-light">
                            <i class="fa fa-pencil fa-fw"></i> Add <strong>New Role</strong>
                        </h4>
                    </header>
                </div>

                <div class="box-body">
                    <div class="well">
                        <span class="label label-success"><i class="fa fa-comment"></i></span>
						<span>
							Add User roles used in the system.
						</span>
                    </div>
                    {!! Form::open(['route' => 'admin.roles.store', 'class'=>'form-horizontal form-bordered
                    form-banded','files'=>true, 'role' =>
                    'form']) !!}
                    @include('admin.setting.roles.form')
                    {!! Form::close() !!}
                </div>
            </div>
            <!--end .box -->
        </div>
        <!--end .col-lg-12 -->
    </div>
@stop
