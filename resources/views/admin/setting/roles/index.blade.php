@extends('layouts.admin.boostbox.index')

@section('content')


    <div class="row">
        <div class="col-lg-12">
            <div class="box box-bordered">
                <div class="box-head">
                    <header><h4 class="text-light"> <strong>User Roles</strong></h4></header>
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.roles.create') }}">
                        <i class="fa fa-aw fa-plus-circle"></i> Add New Role
                    </a>
                </div>
                <div class="box-body">
                    <p>
                        User roles used in the system. If you are admin, you can create roles and assign permission to
                        roles.
                    </p>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $k => $role)
                                <tr>
                                    <td>{{ ++$k }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->display_name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>
                                        <a href="{{ route('admin.roles.edit', [$role->id]) }}" class="btn btn-xs btn-default btn-equal"
                                           data-toggle="tooltip"
                                           data-placement="top" data-original-title="Edit Role">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {!! delete_form(route('admin.roles.destroy', [$role->id]), '', 'btn btn-xs btn-default btn-equal', 'Delete Role') !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end .table-responsive -->
                </div>
                <!--end .box-body -->
            </div>
            <!--end .box -->
        </div>
        <!--end .col-lg-12 -->
    </div>


@stop