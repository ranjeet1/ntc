@extends('layouts.admin.boostbox.index')

@section('content')
    <div class="row">

        <div class="box box-bordered">
            <div class="col-lg-6">
                <div class="box-head">
                    <header>
                        <h4 class="text-light">
                            <i class="fa fa-pencil fa-fw"></i> Edit <strong>Role</strong>
                        </h4>
                    </header>
                </div>

                <div class="box-body">
                    <div class="well">
                        <span class="label label-success"><i class="fa fa-comment"></i></span>
							<span>
								Edit Role - {{ $role->display_name }}
							</span>
                    </div>

                    {!! Form::model($role, ['route' => ['admin.roles.update', $role->id], 'class'=>'form-horizontal
                    form-bordered
                    form-banded','role' => 'form', 'method' => 'patch']) !!}
                    @include('admin.setting.roles.form')
                    {!! Form::close() !!}

                </div>
            </div>
            <!--end .box -->
        </div>
        <!--end .col-lg-12 -->
    </div>
@stop
