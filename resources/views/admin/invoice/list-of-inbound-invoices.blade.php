@extends('layouts.admin.boostbox.index')

@section('content')
		<!--end #sidebar-->
<!-- END SIDEBAR -->

<!-- BEGIN CONTENT-->

<div id="content">

	<div class="section-body">

		<!-- BEGIN BASIC FORM INPUTS -->
		<div class="row">
			<div class="col-lg-12">
				<div class="box ">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<div class="box-head">
								<header>
									<h2 class="text-light"
										style="padding-top:20px;color:#0054A5; text-align:center; padding-left:10px;">


										Inbound Invoices
										{{--({{count($invoices)}}&nbsp;Found)--}}
									</h2>
								</header>
							</div>
						</div>


					</div>
					<div class="box-body no-padding">
						<div class="section-body">
							<div class="row">
								<div class="col-lg-12">
									<div class="box style-transparent">
										<div class="invoice-filter">
											<div class="row">
												<div class="col-lg-3 col-md-3 col-xs-12">

													<div class="form-group">
														<label class="control-label">Starting Date</label>
														<div class='input-group date' id='demo-date-start'>
															<input type="text" id="datetimepicker2" class="form-control"
																   style="background:#FFF; border:2px solid #ccc;"
																   placeholder="4-23-2016"/>
															<span class="input-group-addon"
																  style=" border-left:none; border-right:2px solid #ccc; border-top:2px solid #ccc; border-bottom:2px solid #ccc;"><i
																		class="fa fa-calendar"
																		style="color:#4A95B1; "></i></span>
														</div>
													</div>

												</div>
												<div class="col-lg-3 col-md-3 col-xs-12">
													<div class="form-group">
														<label class="control-label">End Date</label>
														<div class='input-group date' id='demo-date-end'>
															<input type="text" class="form-control"
																   style="background:#FFF; border:2px solid #ccc;"
																   placeholder="5-23-2016"/>
															<span class="input-group-addon"
																  style=" border-left:none; border-right:2px solid #ccc; border-top:2px solid #ccc; border-bottom:2px solid #ccc;"><i
																		class="fa fa-calendar"
																		style="color:#4A95B1;"></i></span>
														</div>
													</div>
												</div>


												<div class="col-lg-3 col-md-3 col-xs-12">
													<div class="form-group">
														<label class="control-label">Service</label>
														<select name="operatorservice" id="operatorservice"
																class="form-control"
																style="border:2px solid #ccc; padding: 5px 3px; width:100%; background:#FFF; color:#a3a2a2;">
															<option value="Select">Select Service</option>
															<option value="Multiple">Multiple</option>
															<option value="Bilateral">Bilateral</option>
														</select>
													</div>

												</div>
												<div class="col-lg-3 col-md-3 col-xs-12">

													<div class="input-group" style="margin-top:22px;">
														<input type="text" class="form-control"
															   style="margin:0; height:33px; border:2px solid #CACACA; background:#FFF; "
															   width="100%;" placeholder="Search Invoice">
      <span class="input-group-btn" style="margin-left:30px;">
        <button class="btn btn-default"
				style="background:#4A95B1 !important; border:none; outline:none; position: relative; z-index:999; margin-left:2px; height:33px;"
				type="button"><i class="fa fa-search" style="color:#fff;"></i></button>
      </span>
													</div>

												</div>
											</div>


										</div>

										<div class="table-responsive">
											<table class="table table-bordered user-manager-new">
												<thead>
												<tr style="font-size:15px;">
													<th width="30px"><input type="checkbox" value="primary2"></th>
													<th width="30px">SN</th>
													<th>Operator Name</th>
													<th>Total</th>
													<th>Invoice Number</th>
													<th>Date</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
												</thead>
												<tbody><?php $i = 1;?>
												@if(isset($invoices)&&count($invoices)>0)
													@foreach($invoices as $k => $invoice)

														<tr class="gradeX">


														<tr>
															<td><input type="checkbox" value="primary2"></td>
															<td>{{$i++}}</td>
															<td><a href="operator/invoice/{{$invoice->operatorId
														}}">{{$invoice->name
														}}</a></td>
															<td>{{$invoice->total_amount}}</td>
															<td>{{$invoice->invoice_number}}</td>
															<td>{{date('F d Y',strtotime($invoice->created_at))}}</td>


															<td>Completed</td>
															<td>

													<a href="{{url('admin/operator',[$invoice->operatorId])
													}}/invoice">
																	<button type="button"
																			class="btn btn-xs btn-primary btn-equal"
																			data-toggle="tooltip" data-placement="top"
																			data-original-title="Generate Invoice"><i
																				class="fa fa-file-text fa-fw"></i>

																	</button>
																</a>

																<button type="button"
																		class="btn btn-xs btn-primary btn-equal"
																		data-toggle="tooltip" data-placement="top"
																		data-original-title="Delete"><i
																			class="fa fa-trash-o"></i></button>
															</td>
													@endforeach
												@else
													<tr>
														<td colspan="10">No inbound invoice found.</td>
													</tr>



												@endif
												</tbody>

											</table>
											{!!$invoices->render()!!}

										</div>

										<!-- Large modal -->


									</div>
									<!--end .box -->
								</div>
								<!--end .col-lg-12 -->
							</div>
							<!--end .row -->
						</div>
					</div>
				</div>
				<!--end .box -->
			</div>
			<!--end .col-lg-12 -->
		</div>
		<!--end .row -->

	</div>
</div>
<!--end #content-->
<!-- END CONTENT -->

</div>

<script>
	$('.show-mores').on('click', function (e) {
		e.preventDefault();
		var hideArea = $(this).parent('.media-body').find('.hide-area');
		if (hideArea.hasClass('toggled')) {
			hideArea.animate({height: 65});
			hideArea.removeClass('toggled');
			$(this).find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
		} else {
			hideArea.css('height', 'auto');
			hideArea.addClass('toggled');

			$(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
		}
	});

	$(function () {
		$('#datetimepicker2').datetimepicker({
			locale: 'ru'
		});
	});

</script>

@stop