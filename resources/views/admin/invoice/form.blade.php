<table>
	<?php
	if(\Request::segment(4) !='edit'){ ?>
	<tr>
		<td width="45%" valign="top"
			style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
			Document Number
		</td>
		<td width="55%"
			style=" padding-top:8px; font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
			<strong>
				<div class="form-group {{ ($errors->has('document_num'))?'has-error':'' }}" >
					{!! Form::text('document_num', null, ['class' => 'form-control']) !!}
					{!! $errors->first('document_num', '<span class="text-danger">:message</span>') !!}
				</div>

			</strong></td>
	</tr>

	<tr>
		<td width="45%" valign="top"
			style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
			Invoice Number
		</td>
		<td width="55%"
			style=" padding-top:8px; font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
			<strong>
				<div class="form-group {{ ($errors->has('invoice_number'))?'has-error':'' }}" >
					{!! Form::text('invoice_number', null, ['class' => 'form-control']) !!}
					{!! $errors->first('invoice_number', '<span class="text-danger">:message</span>') !!}
				</div>

			</strong></td>
		<?php } ?>
	</tr>

	<tr>
		<td width="45%" valign="top"
			style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
			Settlement Date
		</td>
		<td width="55%"
			style=" padding-top:8px; font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
			<strong>
				<div class="form-group {{ ($errors->has('settlement_date'))?'has-error':'' }}" >
					{!! Form::date('settlement_date', null, ['class' => 'form-control']) !!}
					{!! $errors->first('settlement_date', '<span class="text-danger">:message</span>') !!}
				</div>

			</strong></td>
	</tr>

	<tr>
		<td width="45%" valign="top"
			style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
			Statement Due Date
		</td>
		<td width="55%"
			style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
			<strong>
				<div class="form-group {{ ($errors->has('settlement_due_date'))?'has-error':'' }}" >
					{!! Form::date('settlement_due_date', null, ['class' => 'form-control']) !!}
					{!! $errors->first('settlement_due_date', '<span class="text-danger">:message</span>') !!}
				</div>
			</strong></td>
	</tr>

	<tr>
		<td width="45%" valign="top"
			style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
			Billing Start Date
		</td>
		<td width="55%"
			style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
			<strong>	<div class="form-group {{ ($errors->has('billing_start_date'))?'has-error':'' }}" >
					{!! Form::date('billing_start_date', null, ['class' => 'form-control']) !!}
					{!! $errors->first('billing_start_date', '<span class="text-danger">:message</span>') !!}
				</div></strong></td>
	</tr>

	<tr>
		<td width="45%" valign="top"
			style="padding-top:8px;  ont-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-seri">
			Billing End Date
		</td>
		<td width="55%"
			style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
			<strong><div class="form-group {{ ($errors->has('billing_end_date'))?'has-error':'' }}" >
					{!! Form::date('billing_end_date', null, ['class' => 'form-control']) !!}
					{!! $errors->first('billing_end_date', '<span class="text-danger">:message</span>') !!}
				</div>

			</strong></td>
	</tr>

	<tr>
		<td width="45%" valign="top"
			style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
			{{--Operator Id--}}
		</td>
		<td width="55%"
			style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
			<strong>

				<div class="form-group {{ ($errors->has('operator_id'))?'has-error':'' }}" >
					<input type="text" name="operator_id" value="{{$invoices->id}}" hidden >
					{!! $errors->first('operator_id', '<span class="text-danger">:message</span>') !!}
				</div>
				</strong></td>
	</tr>


	{{--<tr>--}}
		{{--<td width="45%" valign="top"--}}
			{{--style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">--}}
			{{--Call Count--}}
		{{--</td>--}}
		{{--<td width="55%"--}}
			{{--style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">--}}
			{{--<strong>--}}
				{{--<div class="form-group {{ ($errors->has('call_count'))?'has-error':'' }}" >--}}
					{{--{!! Form::text('call_count', null, ['class' => 'form-control']) !!}--}}
					{{--{!! $errors->first('call_count', '<span class="text-danger">:message</span>') !!}--}}
				{{--</div></strong></td>--}}
	{{--</tr>--}}


	{{--<tr>--}}
		{{--<td width="45%" valign="top"--}}
			{{--style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">--}}
			{{--Call Minute--}}
		{{--</td>--}}
		{{--<td width="55%"--}}
			{{--style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">--}}
			{{--<strong>	<div class="form-group {{ ($errors->has('call_minutes'))?'has-error':'' }}" >--}}
					{{--{!! Form::text('call_minutes', null, ['class' => 'form-control']) !!}--}}
					{{--{!! $errors->first('call_minutes', '<span class="text-danger">:message</span>') !!}--}}
				{{--</div></strong></td>--}}
	{{--</tr>--}}
	{{--<tr>--}}
		{{--<td width="45%" valign="top"--}}
			{{--style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">--}}
			{{--Rate Applied--}}
		{{--</td>--}}
		{{--<td width="55%"--}}
			{{--style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">--}}
			{{--<strong>	<div class="form-group {{ ($errors->has('rate_applied'))?'has-error':'' }}" >--}}
					{{--{!! Form::text('rate_applied', null, ['class' => 'form-control']) !!}--}}
					{{--{!! $errors->first('rate_applied', '<span class="text-danger">:message</span>') !!}--}}
				{{--</div></strong></td>--}}
	{{--</tr>--}}
	{{--<tr>--}}
		{{--<td width="45%" valign="top"--}}
			{{--style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">--}}
			{{--Total Amount--}}
		{{--</td>--}}
		{{--<td width="55%"--}}
			{{--style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">--}}
			{{--<strong>	<div class="form-group {{ ($errors->has('total_amount'))?'has-error':'' }}" >--}}
					{{--{!! Form::text('total_amount', null, ['class' => 'form-control']) !!}--}}
					{{--{!! $errors->first('total_amount', '<span class="text-danger">:message</span>') !!}--}}
				{{--</div></strong></td>--}}



	{{--</tr>--}}


</table>