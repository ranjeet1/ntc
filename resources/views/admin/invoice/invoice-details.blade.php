<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Inbound Invoice</title>
	<style>
		body {
			background: #ababab;
		}
	</style>
	<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet'
		  type='text/css'/>

</head>

<body>
<div style="background:#FFF; width:950px; margin:0 auto; padding:50px">
	<div style="border:2px solid #000000; padding:20px;">
		<table width="100%" border="0">
			<tr>
				<td width="30%"><img src="{{asset('design-invoice/invoice-inbound and outbound/ntc-logo.jpg')}}" alt=""
									 border="0"/></td>
				<td valign="top" style="text-align:center;"><span
							style="font-family: 'Roboto', sans-serif;  color:#000; font-size:35px; font-weight:bold; line-height:45px;">Nepal Telecom</span><br/><span
							style="font-family: 'Roboto', sans-serif;  color:#000; font-size:20px; font-weight:bold; line-height:28px;">(Nepal Doorsanchar Company Limited)<br/>Revenue Department</<br/>Central
					Office, Bhadrakali Plaza<br/>
					Kathmandu, Nepal</span>

				</td>
				<td width="30%"></td>
			</tr>
		</table>

		<table width="100%" border="0">
			<tr>
				<td style=" margin:0; padding:0; font-family: 'arial', sans-serif;  font-size:20px;  background:#F5BB79; border:1px solid #000; text-align:center; line-height:26px;">
					INTERCONNECT OUTBOUND STATEMENT
				</td>
			</tr>
			<tr>
				<td>
					@if(Session::has('success'))
						<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em>
								{{Session::get('success')}}</em></div>
					@endif
				</td>
			</tr>
		</table>


		<table width="100%">
			<tr style="margin-bottom:10px;">
				<td width="60%">
					<table>
						@if(isset($operatorDetails)&&count($operatorDetails)>0)


							<tr>
								<td width="15%"
									style="font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
									Bill
									to
								</td>

								<td width="60%" style="font-size:13px; font-family:Arial, Helvetica, sans-serif;">
									<strong>
										{{$operatorDetails->name}}</strong></td>
							</tr>

							<tr>
								<td width="15%" valign="top"
									style=" padding-top:8px; font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
									Address
								</td>

								<td width="60%"
									style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
									<strong>To,<br/>
										{{$operatorDetails->contact_person_name}}</strong>
								</td>
							</tr>


							<tr>
								<td width="15%" valign="top"
									style="padding-top:8px; font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
									Phone
								</td>
								<td width="60%"
									style="padding-top:8px; font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
									{{$operatorDetails->phone_number}}</td>
							</tr>

							<tr>
								<td width="15%" valign="top"
									style="padding-top:8px; font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
									Pan No.
								</td>
								<td width="60%"
									style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;"></td>
							</tr>

							<tr>
								<td width="15%" valign="top"
									style="padding-top:8px; font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
									Type
								</td>
								<td width="60%"
									style=" padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
									<strong>Original</strong></td>


								@endif
							</tr>

					</table>

				</td>

				<td width="40%">

					<table width="100%" border="0">
						<tr>


							<a href="{{url('admin/invoice')}}/{{$invoicesDetail->id}}/edit" class="btn
							btn-primary">
								<button>Edit</button>
							</a>
						<tr>
							<td width="40%"
								style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
								Document No
							</td>

							<td width="60%"
								style="padding-top:8px;  padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif;">
								<strong>{{$invoicesDetail->invoice_number}}</strong></td>
						</tr>


						<tr>
							<td width="45%" valign="top"
								style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
								Invoice Number
							</td>
							<td width="55%"
								style=" padding-top:8px; font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
								<strong>{{$invoicesDetail->invoice_number}}</strong></td>
						</tr>

						<tr>
							<td width="45%" valign="top"
								style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
								Statement Date
							</td>
							<td width="55%"
								style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
								<strong>{{$invoicesDetail->settlement_date}}</strong></td>
						</tr>

						<tr>
							<td width="45%" valign="top"
								style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
								Statement Due Date
							</td>
							<td width="55%"
								style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
								<strong>{{$invoicesDetail->settlement_due_date}}</strong></td>
						</tr>

						<tr>
							<td width="45%" valign="top"
								style="padding-top:8px;  ont-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
								Billing Period
							</td>
							<td width="55%"
								style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
								<strong>{{date('F d Y',strtotime($invoicesDetail->billing_start_date))}} to {{date('F d Y',strtotime($invoicesDetail->billing_end_date))}}</strong></td>
						</tr>

						<tr>
							<td width="45%" valign="top"
								style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
								NT Phone No
							</td>
							<td width="55%"
								style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
								<strong>+977-1-210363/322</strong></td>
						</tr>


						<tr>
							<td width="45%" valign="top"
								style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
								Fax No
							</td>
							<td width="55%"
								style="padding-top:8px;  font-size:13px; font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
								<strong>+977-1-4268867</strong></td>
						</tr>


						{{--Document Number {{$invoicesDetail->document_num}}--}}
						{{--<br>--}}

						{{--Invoice Number: {{$invoicesDetail->invoice_number}}--}}
						{{--<br>--}}
						{{--Settlement Date: {{$invoicesDetail->settlement_date}}--}}
						{{--<br>--}}
						{{--Settlement Due Date: {{$invoicesDetail->settlement_due_date}}--}}
						{{--<br>--}}
						{{--Billing Start Date: {{$invoicesDetail->billing_start_date}}--}}
						{{--<br>--}}
						{{--Billing End Date: {{$invoicesDetail->billing_end_date}}--}}
						{{--<br>--}}
						{{--Operator Id: {{$invoicesDetail->operator_id}}--}}
						{{--<br>--}}
						{{--Call Count: {{$invoicesDetail->call_count}}--}}
						{{--<br>--}}
						{{--Call Minutes: {{$invoicesDetail->call_minutes}}--}}
						{{--<br>--}}
						{{--Rate Applied: {{$invoicesDetail->rate_applied}}--}}
						{{--<br>--}}
						{{--Total Amount: {{$invoicesDetail->total_amount}}--}}
						{{--<br>--}}
						{{--Invoice Number: {{$invoicesDetail->created_by}}--}}
						{{--<br>--}}
						{{--Approved By: {{$invoicesDetail->approved_by}}--}}
						{{--<br>Published By: {{$invoicesDetail->published_by}}--}}
						{{--<br>Authorized By: {{$invoicesDetail->authorized_by}}--}}

						<tr>
							<td width="40%" valign="top"
								style="padding-top:8px;  font-size:15px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
								NT TPIN
							</td>

							<td width="60%"
								style="padding-top:8px;  font-size:13px;padding-top:8px;  font-family:Arial, Helvetica, sans-serif; line-height:20px; font-weight: 400;">
								<strong>300044614</strong>
							</td>
						</tr>
						{{--<br>Status: @if($invoicesDetail->status=='0')--}}
						{{--{{'Draft'}}--}}
						{{--@elseif($invoicesDetail->status=='1')--}}
						{{--{{'Submitted'}}--}}
						{{--@elseif($invoicesDetail->status=='2')--}}
						{{--{{'Approved'}}--}}
						{{--@elseif($invoicesDetail->status=='3')--}}
						{{--{{'Published'}}--}}
						{{--@else--}}
						{{--{{'Reconciled'}}--}}
						{{--@endif--}}
						<br>
						</tr>
					</table>

		</table>

		</td>

		</tr>
		</table>

		<div style="border:1px solid #000;"></div>
		<div style="font-size:18px; color:#000; text-align:center; padding:15px 0 10px 0; font-family:Arial, Helvetica, sans-serif; font-weight:bold;">
			Subject: Monthly Invoice
		</div>

		<div style="font-size:14px; color:#000; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:20px;">
			Dear Sir/Madam,<br/>
			Please acknowledge the following monthly traffic account for mentioned period.
		</div>

		<table width="100%"
			   style="border-collapse: collapse; border-spacing: 0; font-family:Arial, Helvetica, sans-serif; line-height:24px; font-size:14px; text-align:center;">
			<tr style="background:#E3E3E3;  font-weight:bold; font-size:15px;">
				<td style="border:1px solid #000;">Date</td>
				<td style="border:1px solid #000;">Billing Operator</td>
				<td style="border:1px solid #000;">Billing Operator Name</td>
				<td style="border:1px solid #000;">Component Direction</td>
				<td style="border:1px solid #000;">Rate Profile</td>
				<td style="border:1px solid #000;">Call Count</td>
				<td style="border:1px solid #000;">Event Duration Minutes</td>
				<td style="border:1px solid #000;">Average Rate</td>
				<td style="border:1px solid #000;">Total Amount</td>


			</tr>

			@if(isset($matchOperatorCodeWithBillingOperatorofDataFinalTable)&&count($matchOperatorCodeWithBillingOperatorofDataFinalTable)>0)



				<tr style="background:#fff; font-size:13px;">
					<td style="border:1px solid #000; ">
						<strong>{{$matchOperatorCodeWithBillingOperatorofDataFinalTable->date}}</strong></td>
					<td style="border:1px solid #000;">
						<strong>{{$matchOperatorCodeWithBillingOperatorofDataFinalTable->billing_operator}}</strong>
					</td>
					<td style="border:1px solid #000;">
						<strong>{{$matchOperatorCodeWithBillingOperatorofDataFinalTable->billing_operator_name}}</strong>
					</td>
					<td style="border:1px solid #000;">
						<strong>{{$matchOperatorCodeWithBillingOperatorofDataFinalTable->component_direction}}</strong>
					</td>
					<td style="border:1px solid #000;">
						<strong>{{$matchOperatorCodeWithBillingOperatorofDataFinalTable->rate_profile}}</strong>
					</td>
					<td style="border:1px solid #000;">
						<strong>{{$matchOperatorCodeWithBillingOperatorofDataFinalTable->call_count}}</strong>
					</td>
					<td style="border:1px solid #000;">
						<strong>{{$matchOperatorCodeWithBillingOperatorofDataFinalTable->event_duration_minutes}}</strong>
					</td>
					<td style="border:1px solid #000;">
						<strong>{{$matchOperatorCodeWithBillingOperatorofDataFinalTable->average_rate}}</strong>
					</td>
					<td style="border:1px solid #000;">
						<strong>{{$matchOperatorCodeWithBillingOperatorofDataFinalTable->total_amount}}</strong>
					</td>

			@else

				<tr>
					<td>No invoice records to show</td>
				</tr>

				@endif

				</tr>


		</table>


		<div style=" margin:0; padding:0 0 270px 0; font-size:14px; color:#000; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:20px;">
		</div>


		<div style=" margin:0; padding:0 0 80px 0; font-size:14px; color:#000; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:20px;">
			In accordance with the International Telecommunications Regulations, this statement will be considered as
			accepted if you do not question it
			within two months.
		</div>


		<div style=" margin:0; padding:10px 0 80px 0; font-size:14px; color:#000; font-weight:600; font-family:Arial, Helvetica, sans-serif; line-height:20px;">
			For<br/>
			Nepal Doorsanchar Company Limited
		</div>

		<div style=" margin:0; padding:10px 0 10px 10px; font-size:14px; color:#000; font-weight:600; font-family:Arial, Helvetica, sans-serif; line-height:20px;">
			Authorised Signatory
		</div>

		<table width="100%" style="border-collapse: collapse; border-spacing: 0">
			<tr>
				<td width="35%"
					style=" padding:20px 0 0 0;font-family:Arial, Helvetica, sans-serif; color:#000; font-size:13px;">
					website: www.ntc.net.np
				</td>
				<td style="  padding:20px 0 0 0; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:13px;">
					Mailing Address : teamsettlement@ntc.net.np
				</td>
			</tr>
		</table>


	</div>
</div>

<br/>
<div style="background:#FFF; width:950px; margin:0 auto; padding:50px">
	<div style="border:2px solid #000000; padding:20px;">
		<table width="100%" border="0">
			<tr>
				<td width="30%"><img src="{{asset('design-invoice/invoice-inbound and outbound/ntc-logo.jpg')}}" alt=""
									 border="0"/></td>
				<td valign="top" style="text-align:center;"><span
							style="font-family: 'Roboto', sans-serif;  color:#000; font-size:35px; font-weight:bold; line-height:45px;">Nepal Telecom</span><br/><span
							style="font-family: 'Roboto', sans-serif;  color:#000; font-size:20px; font-weight:bold; line-height:28px;">(Nepal Doorsanchar Company Limited)<br/>Revenue Department</<br/>Central
					Office, Bhadrakali Plaza<br/>
					Kathmandu, Nepal</span>

				</td>
				<td width="30%"></td>
			</tr>
		</table>
		<table width="50%" border="0" style="padding-top:20px; padding-bottom:10px;">
			<tr>
				<td style="font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:18px;">Party Name</td>
				<td style="font-family:Arial, Helvetica, sans-serif;  font-size:16px;">
					<strong> {{$operatorDetails->name}}</strong></td>
			</tr>

			<tr>
				<td style=" padding:5px 0 0 0; font-family:Arial, Helvetica, sans-serif; font-weight:bold; font-size:18px; padding-top:8px;">
					Statement Type
				</td>
				<td style="padding:5px 0 0 0; font-family:Arial, Helvetica, sans-serif;  font-size:16px;padding-top:8px;">
					<strong>Outbound - BILATERAL</strong></td>
			</tr>
		</table>


		<table width="100%"
			   style="border-collapse: collapse; border-spacing: 0; font-family:Arial, Helvetica, sans-serif; font-weight:bold; line-height:24px; font-size:14px; text-align:center;">

			<tr style="background:#F5BB79;">
				<td colspan="8"
					style="border:1px solid #000; font-size:16px; font-weight:bold; line-height:25px; text-align:center;">
					Interconnect Call Details
				</td>


			</tr>
			<tr style="background:#E3E3E3; font-weight:bold; font-size:15px;">
				<td style="border:1px solid #000;">Traffic Period</td>
				<td style="border:1px solid #000;">Destination</td>
				<td style="border:1px solid #000;">Time Premium</td>
				<td style="border:1px solid #000;">Total Calls</td>
				<td style="border:1px solid #000;">Total Minutes</td>
				<td style="border:1px solid #000;">Rate</td>
				<td style="border:1px solid #000;">Currency</td>
				<td style="border:1px solid #000;">Amount</td>

			</tr>

			@if(isset($monthlyOutBoundInvoicesDetails))
				@foreach($monthlyOutBoundInvoicesDetails as $monthlyOutBoundInvoicesDetail)
					<tr style="background:#fff; font-size:13px;">
						<td style="border:1px solid #000;">
							<strong>{{date('F d Y',strtotime($monthlyOutBoundInvoicesDetail->created_at))}}</strong>
						</td>
						<td style="border:1px solid #000;"><strong>{{$operatorDetails->country}}</strong></td>
						<td style="border:1px solid #000;"><strong>NORMAL HOURS</strong></td>
						<td style="border:1px solid #000;">
							<strong>{{$monthlyOutBoundInvoicesDetail->call_count}}</strong>
						</td>
						<td style="border:1px solid #000;">
							<strong>{{$monthlyOutBoundInvoicesDetail->call_minutes}}</strong></td>
						<td style="border:1px solid #000;">
							<strong>{{$monthlyOutBoundInvoicesDetail->average_rate}}</strong></td>
						<td style="border:1px solid #000;"><strong>USD</strong></td>
						<td style="border:1px solid #000;">
							<strong>{{$monthlyOutBoundInvoicesDetail->total_amount}}</strong></td>
						@endforeach
						@endif
					</tr>

					<tr style="background:#F5BB79; font-size:14px; font-weight:bold;">
						<td colspan="3" style="border:1px solid #000; text-align:left; padding-left:5px;">Grand Total
						</td>
						<td style="border:1px solid #000;">{{$sumCallCount}}</td>
						<td style="border:1px solid #000;">{{$sumCallMinute}}</td>
						<td style="border:1px solid #000;"></td>
						<td style="border:1px solid #000;">USD</td>
						<td style="border:1px solid #000;">{{$sumTotalAmount}}</td>


					</tr>

		</table>


		<table width="100%" style="border-collapse: collapse; border-spacing: 0">
			<tr>
				<td width="35%"
					style=" padding:20px 0 0 0;font-family:Arial, Helvetica, sans-serif; color:#000; font-size:13px;">
					website: www.ntc.net.np
				</td>
				<td style="  padding:20px 0 0 0; font-family:Arial, Helvetica, sans-serif; color:#000; font-size:13px;">
					Mailing Address : teamsettlement@ntc.net.np
				</td>
			</tr>
		</table>


	</div>
</div>


</body>
</html>
