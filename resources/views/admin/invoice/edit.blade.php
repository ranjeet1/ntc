@extends('layouts.admin.boostbox.index')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">


            {!! Form::model($invoices,['route' => array('admin.invoice.update', $invoices->id) , 'class'=>'form-horizontal',
            'method'=>'PATCH']) !!}
            @include('admin.invoice.form')
            <input type="submit" value="submit">
            {!! Form::close() !!}
        </div>
    </div>
@stop
