@extends('layouts.admin.boostbox.index')

@section('css')
<style>

    .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
        min-height: 1px !important;
        padding-left: 5px !important;
        padding-right: 5px !important;
    }

</style>
@stop

@section('content')

    <!-- BEGIN BASIC FORM INPUTS -->
    <div class="row" id="app">
        <div class="col-lg-12">
            <div class="box box-outlined">
                <div class="box-head">
                    <header><h4 class="text-light"><i class="fa fa-pencil fa-fw"></i> Create <strong>Rate Profile </strong></h4></header>
                </div>
                <div class="box-body no-padding">
                    <form class="form-horizontal form-bordered" id="createRateProfile" @submit="validateRateProfile" action="{{ route('admin.contracts.{id}.rate-profile.store', [$contract->id]) }}" accept-charset="utf-8" method="post" enctype="multipart/form-data">

                        {!! csrf_field() !!}

                        @include('admin.rateProfile.form')

                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{ route('admin.contracts.{id}.rate-profile.index', [$contract->id]) }}" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div><!--end .box-body -->
            </div><!--end .box -->
        </div><!--end .col-lg-12 -->
    </div><!--end .row -->
    <!-- END BASIC FORM INPUTS -->


@stop

@section('js')
    <script>
        var storeUrl = $('#createRateProfile').attr('action');

        if (!Array.prototype.forEach)
        {
            Array.prototype.forEach = function(fun /*, thisp*/)
            {
                var len = this.length;

                if (typeof fun != "function")
                    throw new TypeError();

                var thisp = arguments[1];
                for (var i = 0; i < len; i++)
                {
                    if (i in this)
                        fun.call(thisp, this[i], i, this);
                }
            };
        }

        new Vue({
            el: '#app',
            data: {
                order : 1,
                tierDetailId : '',
                rateType : '',
                flatAmount: '',
                fixedRate: '',
                hybrid : [
                    {
                        ceiling : '',
                        backCeiling : '',
                        value : '',
                        type: 'flat',
                        errors : {
                            ceiling : '',
                            value : ''
                        }
                    },
                    {
                        ceiling : '',
                        backCeiling : '',
                        value : '',
                        type: 'incremental',
                        errors : {
                            ceiling : '',
                            value : ''
                        }
                    },
                    {
                        ceiling : Infinity,
                        backCeiling : '',
                        value : '',
                        type: 'incremental',
                        errors : {
                            ceiling : '',
                            value : ''
                        }
                    }
                ],
                errors: {
                        tierDetailId : '',
                        rateType : '',
                        fixedRate : '',
                        flatAmount : ''

                }
            },
            methods: {
                addNewHybrid: function(index, type) {
                    var newType = type == 'back_to_first' ? 'incremental' : type;
                    this.hybrid.splice(
                            index+1,
                            0,
                            {
                                    ceiling : '',
                                    backCeiling : '',
                                    value : '',
                                    type : newType,
                                    errors : {
                                        ceiling : '',
                                        value : ''
                                    }
                            }
                    );
                },
                deleteHybrid: function(hybrid) {
                    this.hybrid.$remove(hybrid);
                },
                ceilingSortingMethod: function (a, b) {
                    return a.ceiling - b.ceiling;
                },
                toggleCeiling: function(pure) {
                    if(pure.type == 'back_to_first') {
                        pure.type = 'incremental';
                        this.setCeilingInfinity(pure);
                    } else if(pure.type == 'incremental') {
                        pure.type = 'back_to_first';
//                        pure.ceiling = '';
                    }

                },
                setCeilingInfinity: function(pure) {
                    setTimeout(function() {
                        this.hybrid.forEach(
                                function (element, index, array) {
                                    if( element.ceiling == Infinity ) {

                                        element.ceiling = '';
                                    }
                                }
                        );

                        pure.ceiling = Infinity;

                    }.bind(this),700);

                },
                validateRateProfile: function(e) {
                    var errors = 0;

                    if(this.tierDetailId == '') {
                        errors++;
                        this.errors.tierDetailId = 'Please select a tier.';
                    } else {
                        this.errors.tierDetailId = '';
                    }

                    if(this.rateType == '') {
                        errors++;
                        this.errors.rateType = 'Please select a rate type.';
                    } else {
                        this.errors.rateType = '';
                    }

                    if(this.rateType == 'flat' && this.flatAmount == '') {
                        errors++;
                        this.errors.flatAmount = 'Please enter amount.';
                    } else {
                        this.errors.flatAmount = '';
                    }

                    if(this.rateType == 'fixed' && this.fixedRate == '') {
                        errors++;
                        this.errors.fixedRate = 'Please enter rate.';
                    } else {
                        this.errors.fixedRate = '';
                    }

                    var vm = this;

                    this.hybrid.forEach(
                        function (element, index, array) {
                            if( (vm.rateType == 'incremental' && element.type == 'incremental') || vm.rateType == 'hybrid' ) {

                                if(element.ceiling <= 0 || element.ceiling == '') {
                                    errors++;
                                    element.errors.ceiling = 'Please enter a valid ceiling';
                                } else {
                                    element.errors.ceiling = '';
                                }

                                if( element.type == 'back_to_first' ) {
                                    if ((element.backCeiling <= 0 || element.backCeiling == '')) {
                                        errors++;
                                        element.errors.ceiling = 'Please enter a valid ceiling';
                                    } else if ((element.backCeiling > 0 || element.backCeiling != '')) {
                                        element.errors.ceiling = '';
                                    }
                                }

                                if(element.value <= 0 || element.value == '') {
                                    errors++;
                                    element.errors.value = 'Please enter a valid value';
                                } else {
                                    element.errors.value = '';
                                }
                            }

                        }
                    );

                    if( (this.rateType == 'hybrid' || this.rateType == 'incremental') && this.hybrid.length < 2) {
                        errors++;
                        alert('Hybrid rate profile must contain at least two rows.');
                    }

                    e.preventDefault();

                    if(errors > 0) {

                    } else {
                        $.post( storeUrl, this.$data, function( result ) {
                            console.log(result);
                            window.location = result.redirectUrl;
                        }.bind(this));
                    }

                },
                countCeiling: function(checkValue) {

                    var count= 0;
                    this.hybrid.forEach(
                            function (element, index, array) {
                                if( element.ceiling == checkValue ) {

                                    count++;
                                }

                            }
                    );

                    return count;
                }
            },
            computed: {
                showRadio : function() {

                    var count= 0;
                    this.hybrid.forEach(
                            function (element, index, array) {
                                if( element.type == 'back_to_first' ) {

                                    count++;
                                }

                            }
                    );

                    return count == 0;
                }
            }
            ,
            watch: {
                rateType: {
                    handler: function (val, oldVal) {

                        if(val == 'incremental') {
                            this.hybrid.forEach(
                                    function (element, index, array) {
                                        if (element.type == 'back_to_first') {

                                            element.type = 'incremental';
//                                            element.ceiling = Infinity;
                                        }

                                    }
                            );
                        }

                    },
                    deep: true
                }
            }
        })

    </script>

@stop