@extends('layouts.admin.boostbox.index')

@section('content')

    <!-- BEGIN BASIC FORM INPUTS -->
    <div class="row" id="app">
        <div class="col-lg-12">
            <div class="box box-outlined">
                <div class="box-head">
                    <header><h4 class="text-light"><i class="fa fa-pencil fa-fw"></i> Edit <strong>Rate Profile</strong></h4></header>
                </div>
                <div class="box-body no-padding">
                    <form class="form-horizontal  form-bordered" action="{{ route('admin.rate-profile.update', [$contract->id, $rateProfile->id]) }}" accept-charset="utf-8" method="post" enctype="multipart/form-data">

                        {!! csrf_field() !!}
                        {{ method_field('patch') }}

                        @include('admin.rateProfile.form')

                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{ route('admin.rate-profile.index') }}" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div><!--end .box-body -->
            </div><!--end .box -->
        </div><!--end .col-lg-12 -->
    </div><!--end .row -->
    <!-- END BASIC FORM INPUTS -->




@stop


