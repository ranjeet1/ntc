@extends('layouts.admin.boostbox.index')

@section('content')



    <!-- START DATATABLE 2 -->
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-bordered">
                <div class="box-head">
                    <header>
                        <h4 class="text-light">Rate Profiles <strong>Table</strong>
                            {{--<small>Tabletools & row details</small>--}}

                            <a class="btn btn-sm btn-primary" href="{{ route('admin.contracts.{id}.rate-profile.create',
					        [$contractId]) }}">
                                <i class="fa fa-aw fa-plus-circle"></i> Add New Rate Profile
                            </a>
                        </h4>


                    </header>
                </div>
                <div class="box-body no-padding table-responsive">
                    <table id="datatable2" class="table table-hover">
                        <thead>
                        <tr>
                            <th class="col-md-1">S.N</th>
                            <th class="col-md-4">Tier</th>
                            <th class="col-md-3">Ceiling</th>
                            <th class="col-md-3">Rate Type</th>
                            <th class="text-right1" class="col-md-1">Actions</th>
                        </tr>
                        </thead>

                        <tbody>
                        @if($rateProfiles->count() > 0)
                            @foreach($rateProfiles as $k => $rateProfile)
                                <?php $count = ($rateProfiles->perPage() * ($rateProfiles->currentPage() - 1)); ?>
                                <tr class="gradeX">
                                    <td>{{ $count ? ($count + $k + 1) : ++$k }}</td>
                                    <td>{{ $rateProfile->tier->fullName }}</td>
                                    <td>{{ $rateProfile->ceiling }}</td>
                                    <td>{{ $rateProfile->rate_type }}</td>
                                    <td class="text-right">
                                        <a href="{{ route('admin.contracts.{id}.rate-profile.edit', [$contractId, $rateProfile->id]) }}"
                                           class="btn btn-xs btn-default btn-equal"
                                           data-toggle="tooltip"
                                           data-placement="top" data-original-title="Edit row">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {!! delete_form(route('admin.contracts.{id}.rate-profile.destroy', [$contractId, $rateProfile->id]), '', 'btn btn-xs btn-default btn-equal', 'Delete Profile') !!}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="gradeX">
                                <td colspan="7">No Operators Found.</td>
                            </tr>
                        @endif

                        </tbody>
                    </table>
                    {!! $rateProfiles->appends($_GET)->links() !!}


                </div>
                <!--end .box-body -->
            </div>
            <!--end .box -->
        </div>
        <!--end .col-lg-12 -->
    </div>
    <!-- END DATATABLE 2 -->

@stop