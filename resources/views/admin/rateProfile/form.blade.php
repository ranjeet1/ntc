<div class="form-group">
    <div class="col-md-2">
        <label for="tier_detail_id" class="control-label">Tier</label>
    </div>

    <div class="col-md-8">
        <select name="tier_detail_id" v-model="tierDetailId" id="tier_detail_id" class="form-control">
            <option value="">select</option>
            @foreach($tiers as $tier):
            <option value="{{ $tier->id }}">{{ $tier->fullName }}</option>
            @endforeach
        </select>
        <span class="help-block">
            {{ $errors->first('tier_detail_id') }}
            @{{ errors.tierDetailId }}
        </span>
    </div>
</div>


<div class="form-group">
    <div class="col-md-2">
        <label for="rate_type" class="control-label">Rate Type</label>
    </div>
    <div class="col-md-8">
        <select name="rate_type" id="rate_type" class="form-control" v-model="rateType">
            <option value="" >select</option>
            @foreach($rateTypes as $key => $rateType):
            <option value="{{ $key }}">{{ $rateType }}</option>
            @endforeach
        </select>
        <span class="help-block">
            {{ $errors->first('rate_type') }}
            @{{ errors.rateType }}
        </span>
    </div>
</div>

<div class="form-group" v-show="rateType == 'flat'">
    <div class="col-md-2">
        <label class="control-label">Flat Amount </label>
    </div>

    <div class="col-md-4">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-usd icon-style-success"></i></span>
            <input name="flat_amount" v-model="flatAmount" type="text" class="form-control" placeholder="Enter Flat Amount">
            {{--<span class="input-group-addon">.00</span>--}}
        </div>
        <span class="help-block">
            {{ $errors->first('flat_amount') }}
            @{{ errors.flatAmount }}
        </span>
    </div>
</div>

<div class="form-group" v-show="rateType == 'fixed'">
    <div class="col-md-2">
        <label class="control-label">Fixed Rate</label>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-usd icon-style-success"></i></span>
            <input type="text" name="fixed_rate" v-model="fixedRate" class="form-control" placeholder="Enter Fixed Rate">
            <span class="input-group-addon"><strong>/ min</strong></span>
        </div>
        <span class="help-block">
            {{ $errors->first('fixed_rate') }}
            @{{ errors.fixedRate }}
        </span>
    </div>
</div>


<div class="form-group" v-show="rateType == 'hybrid' || ( rateType == 'incremental' && pure.type == 'incremental' )" v-for="(index, pure) in hybrid | orderBy ceilingSortingMethod order" >
    <div class="col-md-2 text-center">
        <em class="control-label">
            <span>@{{ pure.type == 'flat' ? 'Flat Amount' : 'Incremental' }}</span>
        </em>
    </div>

    <div class="col-sm-1 text-center">
        <span v-show="pure.type != 'back_to_first'">below</span>
        <em v-else>above</em>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon" v-show="showRadio">
                <input name="infinityRadio" value="Infinity" @click="setCeilingInfinity(pure)" type="radio" :checked="pure.ceiling == Infinity">
            </span>
            <input type="text" class="form-control" name="ceiling[]" value="@{{ pure.ceiling }}" v-model="pure.ceiling" debounce="700" placeholder="Enter Ceiling" :readonly="pure.ceiling == Infinity" v-show="pure.type != 'back_to_first'">
            <input type="text" class="form-control" name="backCeiling[]" value="@{{ pure.backCeiling }}" v-model="pure.backCeiling" placeholder="Enter Ceiling" v-show="pure.type == 'back_to_first'">
            <span class="input-group-addon"><strong>mins</strong></span>
        </div>
        <small class="inline" v-show="pure.ceiling != Infinity && showRadio">click on <em>radio</em> to set <em>ceiling</em> to <em>infinity</em></small>
        <span class="help-block">
            @{{ pure.errors.ceiling }}
        </span>
    </div>

    <div class="col-md-1 text-center">
        <span v-show="pure.type != 'back_to_first'">costs</span>
        <em v-else>costs back to</em>
    </div>

    <div class="col-md-3">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-usd icon-style-success"></i></span>
            <input type="text" name="rate[]" class="form-control" v-model="pure.value" placeholder="Enter Fixed @{{ pure.type == 'flat' ? 'Amount' : 'Rate' }}">
            <span class="input-group-addon" v-show="pure.type != 'flat'"><strong>/ min</strong></span>
        </div>
        <span class="help-block">
            @{{ pure.errors.value }}
        </span>
    </div>

    <div class="col-md-1">
        <div class="btn-group">
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                Action <i class="fa fa-caret-down"></i>
            </button>
            <ul class="dropdown-menu animation-zoom" role="menu" style="text-align: left;">
                <li><a href="javascript:;" @click="addNewHybrid(index, pure.type)" ><i class="fa fa-plus"></i> Add new row</a></li>
                <li v-show="pure.ceiling < Infinity && pure.type != 'back_to_first'"><a href="javascript:;" @click="deleteHybrid(pure)" ><i class="fa fa-times icon-style-danger"></i>Remove this row</a></li>
                <li v-show="(rateType == 'hybrid' && pure.ceiling == Infinity && pure.type == 'incremental') || pure.type == 'back_to_first'"><a href="javascript:;" @click="toggleCeiling(pure)" ><i class="fa fa-refresh icon-style-danger"></i> @{{ pure.type == 'back_to_first' ? 'Set to Infinity' : 'Back to first' }}</a></li>

            </ul>
        </div>
    </div>

</div>


{{--@{{ $data | json }}--}}