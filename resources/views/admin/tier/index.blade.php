@extends('layouts.admin.boostbox.index')

@section('content')

    <div class="col-lg-12">
        <form class="form-horizontal" action="{{ route('admin.tier.import') }}" role="form" method="post"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="form-group">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label for="file" class="control-label">Import Tier File</label>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-5">
                    <input type="file" name="file" id="file" class="form-control">
                    <span for="name" class="help-block">{{ $errors->first('file') }}</span>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>


    <!-- START DATATABLE 2 -->
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-bordered">
                <div class="box-head">
                    <header>
                        <h4 class="text-light">All <strong>Tiers</strong>
                            {{--<small>Tabletools & row details</small>--}}
                        </h4>
                    </header>
                </div>
                <div class="box-body no-padding table-responsive">
                    <table class="table table-responsive data-table">
                        <thead>
                        <tr>
                            <td>SN</td>
                            <td>Tier Name</td>
                            <td>Tier Code</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($tiers as $k => $tier)
                            <tr>
                                <td>{{ ++$k }}</td>
                                <td>{{ $tier->tier_name}}</td>
                                <td>{{ $tier->tier_code}}</td>
                                <td>
                                    <a href="{{ route('admin.tier.edit', $tier->id) }}"
                                       class="btn btn-xs btn-default btn-equal"
                                       data-toggle="tooltip"
                                       data-placement="top" data-original-title="Edit row">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    {!! delete_form(route('admin.tier.destroy', [$tier->id]), '', 'btn
                                    btn-xs btn-default btn-equal', 'Delete row') !!}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">Tiers not found.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <!--end .box-body -->
            </div>
            <!--end .box -->
        </div>
        <!--end .col-lg-12 -->
    </div>
    <!-- END DATATABLE 2 -->

@stop

@section('js')

    <script>
        $(function () {
            $('.data-table').dataTable();
        })
    </script>

@stop