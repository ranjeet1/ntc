@extends('layouts.admin.boostbox.index')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Edit Tier [{{$tier->tier_name}}]</div>
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::model($tier,['route' => array('admin.tier.update', $tier->id) , 'class'=>'form-horizontal',
            'method'=>'PATCH','files'=>true]) !!}
            @include('admin.tier.form')
            {!! Form::close() !!}
        </div>
    </div>
@stop
