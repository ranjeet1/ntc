<div class="form-group">
    {!! Form::label('tier_name', 'Tier Name:', ['class'=>'col-sm-2 control-label'])!!}
    <div class="col-sm-7">
        {!! Form::text('tier_name',null, ["class"=>"form-control"])!!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('tier_code', 'Tier Code:', ['class'=>'col-sm-2 control-label'])!!}
    <div class="col-sm-7">
        {!! Form::text('tier_code',null, ["class"=>"form-control"])!!}
    </div>
</div>

<div class="form-action">
    <div class="col-sm-7 col-lg-offset-2">
        {!! Form::submit('Submit',['class'=>'btn pull-right btn-primary']) !!}
    </div>
</div>
