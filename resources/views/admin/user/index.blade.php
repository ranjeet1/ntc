@extends('layouts.admin.boostbox.index')
@section('content')
    <div class="row">
        <div class="col-lg-12">

            <div class="box box-bordered">
                <div class="box-head">
                    <header><h4 class="text-light">Users <strong>System Users</strong></h4></header>
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.user.create') }}">
                        <i class="fa fa-aw fa-plus-circle"></i> Add New User
                    </a>
                </div>

                <div class="box-body table-responsive">
                    <table id="datatable2" class="table table-hover">
                        <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Role</th>
                            <th>Address</th>
                            <th class="text-right1" style="width:90px">Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @if($users->count() > 0)
                            @foreach($users as $k => $user)

                                <tr class="gradeX">
                                    <td>{{ ++$k }}</td>
                                    <td>
                                        <a href="{{ route('admin.user.show',[$user->id]) }}"><img
                                                    src="{{ image_url($user->photo) }}" width="60"
                                                    alt="{{ $user->first_name }}"/>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{route('admin.user.show',[$user->id])}}">{{ $user->fullName() }}</a>
                                    </td>
                                    <td>
                                        {{ $user->email }}
                                    </td>
                                    <td>
                                        {{ $user->phone_number }}
                                    </td>
                                    <td>
                                        @if($user->roles()->first())
                                            <a href="javascript:;" data-toggle="modal"
                                               data-target=".bs-example-modal-lg">
                                                {{ $user->roles()->first()->display_name }} <i
                                                        class="fa fa-aw fa-pencil-square-o text-danger"></i>
                                            </a>
                                        @else
                                            <a href="javascript:;" data-toggle="modal"
                                               data-target=".assign-role-modal">
                                                Not Assigned <i class="fa fa-aw fa-plus-square-o text-danger"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $user->address() }}
                                    </td>
                                    <td class="text-right">
                                        <a href="{{ route('admin.user.edit', [$user->id]) }}"
                                           class="btn btn-xs btn-default btn-equal"
                                           data-toggle="tooltip"
                                           data-placement="top" data-original-title="Edit row">
                                            <i class="fa fa-pencil"></i>
                                        </a>

                                        {!! delete_form(route('admin.user.destroy', [$user->id]), '', 'btn btn-xs
                                        btn-default btn-equal', 'Delete row') !!}

                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="gradeX">
                                <td colspan="9">No Users Found.</td>
                            </tr>
                        @endif

                        </tbody>
                    </table>


                </div>
                <!--end .box-body -->
            </div>
            <!--end .box -->
        </div>
        <!--end .col-lg-12 -->
    </div>

    <div class="modal fade assign-role-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="box-head">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            style="font-size:38px; margin-top:-5px; outline:none;"><span
                                aria-hidden="true">&times;</span></button>
                    <header>
                        <h2 class="text-light" style="padding-top:20px;"><i class="fa fa-pencil fa-fw"></i>Assign role to User</h2>
                    </header>
                </div>
                <form class="form-horizontal"  role="form" style="padding:30px;">

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <label for="name" class="control-label">Select Role</label>
                                        <select name="role" id="role"
                                                style="border: 1px solid #ccc; padding: 6px 10px; width:100%;">
                                            <option value="Select">Select Role</option>
                                            <option value="Admin">Admin</option>
                                            <option value="Accounting/Business">Accounting/Business</option>
                                            <option value="Analyist">Analyist</option>
                                            <option value="Viewer">Viewer</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-primary">+ Add</button>
                </form>
            </div>
        </div>
    </div>

@stop