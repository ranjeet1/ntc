@extends('layouts.admin.boostbox.index')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Add New User</div>
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['route' => 'admin.user.store', 'class'=>'form-horizontal form-bordered form-banded','files'=>'true']) !!}
            @include('admin.user.form')
            {!! Form::close() !!}
        </div>
    </div>
@stop
