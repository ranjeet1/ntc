<div class="box-body style-white">

    {!! Form::model($user,['route' => array('admin.user.update', $user->id) ,
    'class'=>'form-horizontal', 'method'=>'PATCH', 'files'=> 'true']) !!}

    <div class="clearfix" style="height: 20px"></div>

    <div class="form-group">
        <div class="col-lg-1 col-md-2 col-sm-3">
            <label for="email1" class="control-label">Email</label>
        </div>
        <div class="col-lg-11 col-md-10 col-sm-9">
            <input type="email" value="{{ $user->email }}" name="email" id="email" class="form-control"
                   placeholder="Email">
            {!! $errors->first('email') !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <label for="role" class="control-label">Select Role</label>
                </div>
                <div class="col-lg-10 col-md-8 col-sm-6">
                    <select name="role" id="role" class="form-control">
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}" {{ $user->hasRole($role->name) ? "selected='selected'" : "" }}>{{ $role->display_name }}</option>
                        @endforeach
                    </select>
                    <p class="help-block">Please assign user to a role. Permission are granted based on the role given to user.</p>
                    {!! $errors->first('role') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <label for="email1" class="control-label">FirstName</label>
                </div>
                <div class="col-lg-10 col-md-8 col-sm-6">
                    <input type="text" value="{{ $user->first_name }}" name="first_name" id="first_name"
                           class="form-control"
                           placeholder="First Name"
                            >
                    {!! $errors->first('first_name') !!}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <label for="email1" class="control-label">LastName</label>
                </div>
                <div class="col-lg-10 col-md-8 col-sm-6">
                    <input type="text" value="{{ $user->last_name }}" name="last_name" id="last_name"
                           class="form-control"
                           placeholder="Last Name"
                            >
                    {!! $errors->first('last_name') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="form-group">
                <div class="col-lg-2 ">
                    <label for="photo" class="control-label">Photo</label>
                </div>

                <div class="col-lg-10 ">
                    <input type="file" name="photo" id="photo" class="form-control">
                    <span for="name" class="help-block">{{ $errors->first('photo') }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <label for="idf" class="control-label">Identification</label>
                </div>

                <div class="col-lg-10 col-md-8 col-sm-6">
                    <input type="file" name="idf" id="idf" class="form-control">
                    <span for="name" class="help-block">{{ $errors->first('idf') }}</span>
                </div>
            </div>
        </div>

    </div>

    <hr/>

    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <label for="phone" class="control-label">Phone Number</label>
                </div>
                <div class="col-lg-10 col-md-8 col-sm-6">
                    <input type="text" value="{{ $user->phone_number }}" name="phone_number" id="phone_number" class="form-control"
                           placeholder="Contact Phone number">
                    {!! $errors->first('phone_number') !!}
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <label for="country" class="control-label">Country</label>
                </div>
                <div class="col-lg-10 col-md-8 col-sm-6">
                    <input type="text" value="{{ $user->country }}" name="country" id="country" class="form-control"
                           placeholder="User Country">
                    {!! $errors->first('country') !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <label for="email1" class="control-label">City</label>
                </div>
                <div class="col-lg-10 col-md-8 col-sm-6">
                    <input type="text" value="{{ $user->city }}" name="city" id="city"
                           class="form-control"
                           placeholder="User City">
                    {!! $errors->first('city') !!}
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <div class="col-lg-2 col-md-4 col-sm-6">
                    <label for="street_name" class="control-label">Street</label>
                </div>
                <div class="col-lg-10 col-md-8 col-sm-6">
                    <input type="text" value="{{ $user->street_name }}" name="street_name" id="street_name"
                           class="form-control"
                           placeholder="Street Name">
                    {!! $errors->first('street_name') !!}
                </div>
            </div>
        </div>
    </div>

    <hr/>

    <div class="form-group">
        <div class="col-lg-1 col-md-2 col-sm-3">
            <label for="info" class="control-label">
                User Info
                <small>Brief info of user</small>
            </label>
        </div>
        <div class="col-lg-11 col-md-10 col-sm-9">
            <textarea name="info" id="info" class="form-control" rows="3" placeholder="User Info"></textarea>
        </div>
    </div>
    <div class="form-footer col-lg-offset-1 col-md-offset-2 col-sm-offset-3">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="submit" class="btn btn-default">Reset</button>
    </div>
    {!! Form::close() !!}

</div>
