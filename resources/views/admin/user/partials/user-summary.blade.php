<div class="box-tiles style-white">
    <div class="row">

        <!-- START PROFILE SIDEBAR -->
        <div class="col-sm-3 style-inverse">
            <div class="holder">
                <img class="img-rounded img-responsive" src="{{image_url($user->photo)}}"
                     alt=""/>

                <div class="stick-bottom-left">
                    <a class="btn btn-inverse btn-equal" data-toggle="tooltip"
                       data-placement="top" data-original-title="Contact me"><i
                                class="fa fa-envelope"></i></a>
                    <a class="btn btn-inverse btn-equal" data-toggle="tooltip"
                       data-placement="top" data-original-title="Follow me"><i
                                class="fa fa-twitter"></i></a>
                    <a class="btn btn-inverse btn-equal" data-toggle="tooltip"
                       data-placement="top" data-original-title="Personal info"><i
                                class="fa fa-facebook"></i></a>
                </div>
            </div>
            <div class="box-body style-inverse">
                <p class="text-support5-alt">
                    <span class="text-xl text-light">{{$user->first_name}} {{$user->last_name}}</span><br/>
                    <span class="text-sm">Consultant at CodeCovers</span>
                </p>
            </div>
            <div class="box-body-darken style-inverse">
                <ul class="nav nav-pills nav-stacked nav-transparent">
                    <li><a href="#"><span class="badge pull-right">42</span>Projects</a></li>
                    <li><a href="#">Friends</a></li>
                    <li><a href="#"><span class="badge pull-right">3</span>Messages</a></li>
                </ul>
            </div>
            <div class="box-body style-inverse">
                <address class="text-support5-alt">
                    <strong>Daniel Johnson, Inc.</strong><br>
                    621 Johnson Ave, Suite 600<br>
                    San Francisco, CA 54321
                </address>
                <address class="text-support5-alt">
                    <abbr title="Phone"><i class="fa fa-phone fa-fw"></i></abbr> (123)
                    456-7890<br>
                    <abbr title="Birthday"><i class="fa fa-gift fa-fw"></i></abbr> January 15,
                    1976
                </address>
            </div>
        </div>
        <!--end .col-sm-3 -->
        <!-- END PROFILE SIDEBAR -->

        <!-- START PROFILE CONTENT -->
        <div class="col-sm-9">
            <div class="alert alert-warning">
                <i class="fa fa-comments"></i> You have 7 new comments
                <a href="#" class="link-default">Read comments</a>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8">
                        <p class="lead">Nullam quis risus eget urna mollis ornare vel eu leo.
                            Cum sociis natoque penatibus et magnis dis parturient montes.</p>

                        <p>Cum sociis natoque penatibus et magnis dis parturient montes,
                            nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor
                            fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor
                            ligula, eget lacinia odio sem nec elit.</p>
                    </div>
                    <div class="col-sm-4">
                        <div class="pie-chart flot text-center">
                            <div class="chart size-3 v-inline-middle" data-title="Site visits"
                                 data-color="#FBEED5,#2E383D"></div>
                            <div class="legend v-inline-middle text-left"></div>
                        </div>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <!-- Extra row gap-->
                <div class="row">
                    <div class="col-md-8">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Phone Number</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip" data-placement="top"
                                                data-original-title="Edit row"><i
                                                    class="fa fa-pencil"></i></button>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip" data-placement="top"
                                                data-original-title="Copy row"><i
                                                    class="fa fa-copy"></i></button>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip" data-placement="top"
                                                data-original-title="Delete row"><i
                                                    class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jacob</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip" data-placement="top"
                                                data-original-title="Edit row"><i
                                                    class="fa fa-pencil"></i></button>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip" data-placement="top"
                                                data-original-title="Copy row"><i
                                                    class="fa fa-copy"></i></button>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip" data-placement="top"
                                                data-original-title="Delete row"><i
                                                    class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Larry</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    <td>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip" data-placement="top"
                                                data-original-title="Edit row"><i
                                                    class="fa fa-pencil"></i></button>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip" data-placement="top"
                                                data-original-title="Copy row"><i
                                                    class="fa fa-copy"></i></button>
                                        <button type="button"
                                                class="btn btn-xs btn-inverse btn-equal"
                                                data-toggle="tooltip" data-placement="top"
                                                data-original-title="Delete row"><i
                                                    class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--end .table-responsive -->
                    </div>
                    <!--end .col-sm-8 -->
                    <div class="col-md-4">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge">$ 300</span>
                                Sales Today
                            </li>
                            <li class="list-group-item">
                                <span class="badge">$ 2.100</span>
                                Sales this week
                            </li>
                            <li class="list-group-item">
                                <span class="badge">$ 198.000</span>
                                Total sales
                            </li>
                        </ul>
                    </div>
                    <!--end .col-sm-4 -->
                </div>
                <!--end .row -->
            </div>
        </div>
        <!--end .col-sm-9 -->
        <!-- END PROFILE CONTENT -->

    </div>
    <!--end .row -->
</div>
<!--end .box-body -->