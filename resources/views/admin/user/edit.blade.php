@extends('layouts.admin.boostbox.index')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Edit User [{{$user->first_name}}]</div>
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {!! Form::model($user,['route' => array('admin.user.update', $user->id) ,
            'class'=>'form-horizontal','files'=>'true',
            'method'=>'PATCH']) !!}
            @include('admin.user.form')
            {!! Form::close() !!}


        </div>
    </div>
@stop
