<div class="row">
    <div class="col-md-8 col-lg-6 col-sm-12">

        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                {!! Form::label('first_name', 'Salutation:', ['class'=>'control-label'])!!}
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                <select name="salutation" id="salutation" class="form-control">
                    <option value="Mr.">Mr</option>
                    <option value="Mrs.">Mrs.</option>
                    <option value="Er.">Er.</option>
                    <option value="Dr.">Dr.</option>
                </select>
                <span for="name" class="help-block">{{ $errors->first('salutation') }}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                {!! Form::label('first_name', 'First Name:', ['class'=>'control-label'])!!}
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                {!! Form::text('first_name',null, ["class"=>"form-control", "placeholder" => "Enter User Firs Name"])!!}
                <span for="name" class="help-block">{{ $errors->first('first_name') }}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                {!! Form::label('last_name', 'Last Name:', ['class'=>'control-label'])!!}
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                {!! Form::text('last_name',null, ["class"=>"form-control", "placeholder" => "Enter User Last Name"])!!}
                <span for="last_name" class="help-block">{{ $errors->first('last_name') }}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                {!! Form::label('email', 'Email:', ['class'=>'control-label'])!!}
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                {!! Form::text('email',null, ["class"=>"form-control", "placeholder" => "Enter User Email"])!!}
                <span for="email" class="help-block">{{ $errors->first('email') }}</span>
            </div>
        </div>

    </div>

    <div class="col-md-8 col-lg-6 col-sm-12">

        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                {!! Form::label('phone_number', 'Phone Number:', ['class'=>'control-label'])!!}
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                {!! Form::text('phone_number',null, ["class"=>"form-control", "placeholder" => "Enter Contact Number"])!!}
                <span for="email" class="help-block">{{ $errors->first('phone_number') }}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                {!! Form::label('country', 'Country:', ['class'=>'control-label']) !!}
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                {!! Form::text('country',null, ["class"=>"form-control", "placeholder" => "Enter User Country"])!!}
                <span for="email" class="help-block">{{ $errors->first('country') }}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                {!! Form::label('city', 'City:', ['class'=>'control-label'])!!}
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                {!! Form::text('city',null, ["class"=>"form-control", "placeholder" => "Enter User City"]) !!}
                <span for="city" class="help-block">{{ $errors->first('city') }}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                {!! Form::label('street_name', 'Street:', ['class'=>'control-label'])!!}
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                {!! Form::text('street_name',null, ["class"=>"form-control", "placeholder" => "Enter User Street"])
                !!}
                <span for="email" class="help-block">{{ $errors->first('street_name') }}</span>
            </div>
        </div>

    </div>
</div>

<hr/>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                <label for="photo" class="control-label">Role</label>
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                <select name="role" id="role" class="form-control">
                    <option value="">Select Role</option>
                    @foreach($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->display_name }}</option>
                    @endforeach
                </select>
                <span for="name" class="help-block">{{ $errors->first('role') }}</span>
            </div>
        </div>
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                <label for="photo" class="control-label">Upload Photo</label>
            </div>
            <div class="col-lg-9 col-md-10 col-sm-9">
                <input type="file" name="photo" id="photo" class="form-control">
                <span for="name" class="help-block">{{ $errors->first('photo') }}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-3 col-md-2 col-sm-3">
                <label for="idf" class="control-label">Upload Identification document</label>
            </div>

            <div class="col-lg-9 col-md-10 col-sm-9">
                <input type="file" name="idf" id="idf" class="form-control">
                <span for="name" class="help-block">{{ $errors->first('idf') }}</span>
            </div>
        </div>
    </div>
</div>


<hr/>

<div class="form-action">
    <div class="form-footer">
        <button type="submit" class="btn btn-primary"> Save User</button>
        <a href="{{ route('admin.user.index') }}" class="btn btn-default">Cancel</a>
    </div>
</div>

</div>
</div>
