@extends('layouts.admin.boostbox.index')
@section('content')

    <div class="row">
        <div class="col-lg-12">

            <div class="box style-transparent">

                <!-- START PROFILE TABS -->
                <div class="box-head">
                    <ul class="nav nav-tabs tabs-transparent" data-toggle="tabs">
                        <li class="active"><a href="#overview"><i class="fa fa-inbox"></i> Overview</a></li>
                        <li><a href="#editDetails"><i class="fa fa-edit"></i> Change details</a></li>
                    </ul>
                </div>
                <!-- END PROFILE TABS -->

                <div class="tab-content">

                    <!-- START PROFILE OVERVIEW -->
                    <div class="tab-pane active" id="overview">
                        @include('admin.user.partials.user-summary')
                    </div>
                    <!--end .tab-pane -->
                    <!-- END PROFILE OVERVIEW -->

                    <!-- START PROFILE EDITOR -->

                    <div class="tab-pane" id="editDetails">

                        {{--user edit form--}}
                        @include('admin.user.partials.edit-form')

                    </div>
                    <!--end .tab-pane -->
                    <!-- END PROFILE EDITOR -->

                </div>
                <!--end .tab-content -->
            </div>
            <!--end .box -->
        </div>
        <!--end .col-lg-12 -->
    </div>


@stop