<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="name" class="control-label">Name</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        {!! Form::text('name',null, ["class" => "form-control", 'required', "id" => "name", "placeholder" => "Name of Service"])!!}
        <span for="name" class="help-block">{{ $errors->first('name') }}</span>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-3 col-md-2 col-sm-3">
        <label for="slug" class="control-label">Slug</label>
    </div>
    <div class="col-lg-9 col-md-10 col-sm-9">
        {!! Form::text('slug',null, ["class"=>"form-control", "required", "id" => "name", "placeholder" => "Slug"])!!}
        <span for="name" class="help-block">{{ $errors->first('slug') }}</span>
    </div>
</div>

<div class="form-footer col-lg-offset-3 col-md-offset-2 col-sm-offset-3">
    {!! Form::submit('Submit',['class' => 'btn btn-primary']) !!}
    <a href="{{ route('admin.service.index') }}" class="btn btn-default">Cancel</a>
</div>