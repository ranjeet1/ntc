@extends('layouts.admin.boostbox.index')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-outlined">
                <div class="box-head">
                    <header><h4 class="text-light"><i class="fa fa-pencil fa-fw"></i> Add <strong>Service
                            </strong></h4></header>
                </div>
                <div class="box-body">
                    {!! Form::open(['route' => 'admin.service.store', 'class' => 'form-horizontal form-bordered
                    form-banded', 'files' => true ]) !!}
                    @include('admin.service.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
