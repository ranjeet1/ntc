@extends('layouts.admin.boostbox.index')

@section('content')

<div class="row">
	<div class="col-lg-6">
		<div class="box box-outlined">
			<div class="box-head">
				<header><h4 class="text-light"><i class="fa fa-pencil fa-fw"></i> Edit <strong>Service
							List</strong></h4></header>
			</div>
			<div class="box-body no-padding">
				{!! Form::model($service,['route' => array('admin.service.update', $service->id) , 'class' => 'form-horizontal form-bordered
                form-banded',
					'method'=>'PATCH']) !!}

				@include('admin.service.form')

				{!! Form::close() !!}

			</div><!--end .box-body -->
		</div><!--end .box -->
	</div><!--end .col-lg-12 -->
</div><!--end .row -->
<!-- END BASIC FORM INPUTS -->


@stop