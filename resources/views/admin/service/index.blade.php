@extends('layouts.admin.boostbox.index')

@section('content')


    <div class="row">
        <div class="col-lg-12">
            <div class="box box-outlined">
                <div class="box-head">
                    <header><h4 class="text-light">Services <strong>table</strong></h4></header>
                    <a class="btn btn-sm btn-primary" href="{{ route('admin.service.create') }}">
                        <i class="fa fa-aw fa-plus-circle"></i> Add New Service
                    </a>
                </div>
                <div class="box-body">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <td>Sn</td>
                            <td>Service Name</td>
                            <td>Slug</td>
                            <td>Added On</td>
                            <td class="pull-right">Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @if($services->count() > 0)
                            @foreach($services as $k => $service)
                                <?php $count = ($services->perPage() * ($services->currentPage() - 1)); ?>
                                <tr>
                                    <td>{{$count ? ($count + $k + 1) : ++$k }}</td>
                                    <td>{{ $service->name }}</td>
                                    <td>{{ $service->slug }}</td>
                                    <td>{{date('F d Y',strtotime($service->created_at))}}</td>


                                    <td class="text-right">
                                        <a href="{{ route('admin.service.edit', [$service->id]) }}"
                                           class="btn btn-xs btn-default btn-equal"
                                           data-toggle="tooltip"
                                           data-placement="top" data-original-title="Edit row">
                                            <i class="fa fa-pencil"></i>
                                        </a>

                                        {!! delete_form(route('admin.service.destroy', [$service->id]), '', 'btn btn-xs
                                        btn-default btn-equal', 'Delete row') !!}
                                    </td>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">No Service Types Found</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    {!! $services->appends($_GET)->links() !!}
                </div>
            </div>
        </div>
    </div>
@stop