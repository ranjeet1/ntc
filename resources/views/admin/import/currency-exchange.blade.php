@extends('layouts.admin.boostbox.index')

@section('content')

    <div class="row">
        <div class="span8">

            <div class="widget ">

                <div class="widget-header">
                    <i class="icon-plus-sign"></i>
                    <h3>Forex</h3>
                </div>
                <!-- /widget-header -->
                

                <div class="widget-content">
                    {!! Form::open(['route' => 'import.currency.add', 'method' => 'post', 'files' => true, 'class' =>
                    'form-horizontal']) !!}
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="logo">Import file here</label>

                            <div class="controls">
                                <input type="file" class="span4" name="file" class="form-control" id="file"/>
                                <span class="help-block error">{{ $errors->first('file') }} </span>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="{{ route('dashboard') }}" class="btn">Cancel</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop