@extends('layouts.admin.boostbox.index')

@section('content')



<div class="row">
    {!! Form::open(['url' => ['admin/crosswalk-operator-details'], 'method' => 'POST', 'class' => 'form-horizontal','files'=>true]) !!}

    <div class="form-group {{ ($errors->has('remark'))?'has-error':'' }}">
        {!! Form::label('remark', trans('Import Operator Files'),['class'=>'col-sm-2 col-md-2 control-label']) !!}
        <div class="col-sm-10 col-md-10">
            {!! Form::file('file', null, 'required',['class' => 'form-control']) !!}
            {!! $errors->first('file', '<span class="text-danger">:message</span>') !!}
      	<button type="submit" class="btn btn-primary">Submit</button>

        </div>
    </div>
    {!!Form::close()!!}
</div>




 <div class="panel-body">
     @if(isset($operatorDetails) && count($operatorDetails) > 0)

          <table class="table table-striped">
                <thead>
                <tr>
                    <th>sn</th>
                    <th>Imported Date</th>
                    <th>Operator Code</th>
                    <th>Operator Name</th>
                    
                   
                </tr>
                </thead>
              <tbody>
               <?php  $i=1; ?>

                   @foreach($operatorDetails as $operatorDetail)

                        <tr>
                     <td> {{$i++}}</td>
                        <td>{{$operatorDetail->imported_date}}</td>
                        {{--date('d-m-Y',strtotime($operatorDetail->created_at))--}}
                           
                            <td>{{$operatorDetail->operator_code }} </td>
                            <td>{{$operatorDetail->operator_name}}</td>
                           
                        <td class="text-right action">
                        </td>
                    </tr>
                    @endforeach
                @else
                    <div class="alert alert-danger" role="alert">
                      No records to show
                    </div>
                @endif
                </tbody>
            </table>
                {!!$operatorDetails->render()!!}


        </div>

@stop