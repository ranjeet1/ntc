@extends('layouts.admin.index')

@section('content')

<div class="ntc-invoice">
<div class="container invoice-bg">



<div class="card">
<div class="card-head">
<ul class="nav nav-tabs" data-toggle="tabs">
<li class="active"><a href="#contract">Contract</a></li>
<li><a href="#profile">Profile</a></li>

</ul>
</div><!--end .card-head -->
<div class="card-body tab-content">
<div class="tab-pane active" id="contract"> 


<h2>Manage Rate Profile for Operator</h2>
<br>


<div class="row ">

<form class="form">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">


    <div class="form-group">
    <input type="text" class="form-control" name="country" id="country">
    <label for="Country">Country</label>
    </div>

    <div class="form-group">
    <input type="text" class="form-control" name="operator" id="operator">
     <label for="Operator">Operator</label>
    </div>


    <div class="form-group">
    <input type="text" class="form-control" name="type" id="type">
    <label for="type">Type</label>
    </div>

   

      <div class="form-group">
    <input type="text" class="form-control" name="currency" id="currency">
    <label for="help1">Currency</label>
    </div>


      <div class="form-group">
    <input type="text" class="form-control" name="minutes" id="minutes">
    <label for="help1">Minutes</label>
    </div>
    
    <div class="form-group">
    <textarea name="remarks" id="remarks" class="form-control" rows="4" placeholder=""></textarea>
    <label for="textarea1">Remarks</label>
    </div>

   


   


</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="row">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="form-group">
<select id="select1" name="select1" class="form-control">
   <option value="0">Select Option</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    
</select>
<label for="select1">Termination (Tire)</label>
    
    </div>
 </div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="row">
<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
<div class="form-group">


<input type="text" class="form-control" name="headerSearch" placeholder="Enter your keyword">
</div>



</div>  
     

      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
      <div class="form-group">
<button type="submit" style="margin:10px 0 0 0;" class="btn ink-reaction "><i class="fa fa-search"></i></button>
      </div>
      </div>



</div>

 



</div>

</div>

<div class="form-group">
<select id="contracttype" name="contracttype" class="form-control">
    <option value="0">Select Option</option>
    <option value="1">Open</option>
    <option value="2">Close</option>
    
    
</select>
<label for="select1">Contract Type</label>
    
    </div>

    <div class="form-group">
    <input type="text" class="form-control" name="volumeceiling" id="volumeceiling">
    <label for="help1">Volume Ceiling</label>
    </div>

    <div class="form-group">
    <input type="text" class="form-control" name="rateperminutes" id="rateperminutes">
    <label for="help1">Rate per Minutes</label>
    </div>

    <div class="form-group">
   
      <input type="checkbox" value="u1" style="margin-top:16px; width: 18px; height: 18px;">
                               
    <label for="tax" style="font-size: 16px; ">Tax</label>
    </div>

    <div class="form-group pull-right">
<button type="button" class="btn ink-reaction btn-raised btn-lg btn-info">Submit</button>
      </div>

</div>


 </form>
</div>
 



                   

</div>

<div class="tab-pane" id="profile">

<h2>Operator Profile</h2>
<div class="row">
 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
 <div class="contractor-profile-left"><img src="../public/assets/img/avatar42dba.jpg" class="img-responsive" alt="" border="0" /></div>
<a href=""><i class="fa fa-envelope" style="font-size: 20px; color:#0273a8; margin-right:7px;"></i></a>
<a href=""><i class="fa fa-facebook" style="font-size: 20px; color:#0273a8; margin-right:7px;"></i></a>
<a href=""><i class="fa fa-twitter" style="font-size: 20px; color:#0273a8;"></i></a>
     


 </div>

 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
 <div class="contractor-profile"><span>Name:</span> ABC Company </div>
 <div class="contractor-profile"><span>Country:</span> Australia</div>
 <div class="contractor-profile"><span>Contact Address:</span> Australia Sydney</div>
 <div class="contractor-profile"><span>Contact Person:</span> Xyz</div>
 <div class="contractor-profile"><span>Phone Details:</span> 977-9845782235</div>
 <div class="contractor-profile"><span>Mailing Details:</span> info@yourdomain.com</div>
 <div class="contractor-profile"><span>Bank Account:</span> </div>
     

     
 </div>   



</div>

<div class="row" style="margin-top: 30px;">
 <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
<h3>Company Sales Statement</h3>



<ul class="list-group">
                                                        <li class="list-group-item">
                                                            <span class="badge btn-info">$ 300</span>
                                                            Sales Today
                                                        </li>
                                                        <li class="list-group-item">
                                                            <span class="badge btn-info">$ 2.100</span>
                                                            Sales this week
                                                        </li>
                                                        <li class="list-group-item">
                                                            <span class="badge btn-info">$ 198.000</span>
                                                            Total sales
                                                        </li>
                                                    </ul>
 </div>

</div>

</div>






 </div>
 </div>

</div>
</div>

@stop