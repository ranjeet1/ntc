@extends('layouts.admin.index')

@section('content')

<div class="ntc-invoice">
<div class="container invoice-bg">
<div class="row ">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<img src="../../ntc/public/assets/img/ntc-logo.jpg" alt="" border="0" />

</div>

<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
<div class="invoice-header">
<div class="title">Nepal Telecom</div>
<div class="small-title">(Nepal Doorsanchar Company Limited)<br />
Revenue Department<br />
Central Office, Bhadrakali Plaza<br />
Kathmandu, Nepal</div>



</div>


</div>



</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="statement-info">INTERCONNECT INBOUND STATEMENT</div>
</div>
</div>

<div class="row banner-border">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<div class="banner-details">
<label>Billed to</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">Pakistan Tele comm Company Ltd</div>

</div>
</div>


</div>

<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<div class="banner-details">
<label>Address</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">To,<br />
Pakistan Telecommunication Company Ltd,<br />
Office of the General Manager Int<br />
Revenue C Block, 3rd floor PTCL H/Qs<br />
G-8/4,Islamabad, Pakistan.</div>

</div>
</div>


</div>
<br /><br /><br /><br />
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<div class="banner-details">
<label>Phone No.</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output"></div>

</div>
</div>


</div>

<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<div class="banner-details">
<label>Pan No.</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output"></div>

</div>
</div>


</div>

<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<div class="banner-details">
<label>Type</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">Original</div>

</div>
</div>


</div>

</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-details">
<label>Document No</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">NT/PCTLCM/VCG/AUG-15</div>

</div>
</div>


</div>

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-details">
<label>NT TPIN</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">300044614</div>

</div>
</div>


</div>

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-details">
<label>Statement Number</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">NT/PCTLCM/VCG/JUL-15</div>

</div>
</div>


</div>

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-details">
<label>Statement Date</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">11-AUG-2015</div>

</div>
</div>


</div>

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-details">
<label>Statement Due Date</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">10-SEP-2015</div>

</div>
</div>


</div>

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-details">
<label>Billing Period</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">01-Jul-2015 to 31-Jul-2015</div>

</div>
</div>


</div>



<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-details">
<label>NT Phone No.</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">+977-1-210363/322</div>

</div>
</div>


</div>

<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<div class="banner-details">
<label>Fax No.</label>

</div>
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<div class="banner-details">
<div class="output">+977-1-4268867</div>

</div>
</div>


</div>

</div>

</div>


<div class="row bill-calculation">
<div class="title">Subject: Monthly Statement</div>

<div class="subject">
Dear Sir/Madam,<br />
Please acknowledge the following monthly traffic account for mentioned period.
</div>


<div class="table-responsive" style="margin-top:20px;">
                        <table class="table no-margin hoverTable">
                            <thead>
                                <tr>
                                    <th>Traffic Period</th>
                                    <th>Description</th>
                                    <th>Total Calls</th>
                                    <th>Total Minutes</th>
                                    <th>Currency</th>
                                    <th>Amount</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>AUG-15</td>
                                    <td>BILATERAL</td>
                                    <td>4,804.00</td>
                                    <td>9,886.652</td>
                                    <td>USD</td>
                                    <td>870.03</td>
                                    
                                </tr>
                                
                                <tr>
                                    <td colspan="2">Grand Total</td>
                                    <td>4,804.00</td>
                                    <td>9,886.65</td>
                                    <td>USD</td>
                                    <td>870.03</td>
                                    
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                    
 <div class="receivable-amount">Total Recievable Amount (USD): <span>2,085.28</span></div>                   
   <div class="amount-word">Amount in Words : Eight Hundred Seventy Dollars Three Cents Only.
   <br /><br /><br /><br /><br /><br />
   
   
   </div>  
   
    <div class="amount-word">In accordance with the International Telecommunications Regulations, this statement will be considered as accepted if you do not question it
within two months.
  
   </div>               

</div>


<br /><br /><br /><br /><br />



<div class="subject">
For<br />
Nepal Doorsanchar Company Limited
</div>
<br /><br />
<div class="sds">
.....................................<br />
<span style="font-weight:bold">Authorised Signatory</span>
</div>



</div>
</div>

<div class="ntc-invoice">
<div class="container invoice-bg">
<div class="row ">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
<img src="../../ntc/public/assets/img/ntc-logo.jpg" alt="" border="0" />

</div>

<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
<div class="invoice-header">
<div class="title">Nepal Telecom</div>
<div class="small-title">(Nepal Doorsanchar Company Limited)<br />
Revenue Department<br />
Central Office, Bhadrakali Plaza<br />
Kathmandu, Nepal</div>



</div>


</div>



</div>


<div class="container second-billing">
<div class="row">
<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><div class="title">Party Name</div></div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12"><div class="sub-title">Pakistan Tele comm Company Ltd</div></div>
</div>
<div class="row">
<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 "><div class="title">Statement Type</div></div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12"><div class="sub-title">Outbound - BILATERAL</div></div>
</div>
</div>





<div class="table-responsive" style="margin-top:20px;">
                        <table class="table no-margin billingtable">
                            <thead>
                                <tr>
                                    <th colspan="8">Interconnect Call Details</th>
                                   
                                    
                                </tr>
                            </thead>
                            <tbody>
                            
                             <tr class="main-row">
                                    <td>Traffic Period</td>
                                    <td>Destination</td>
                                    <td>Time Premium</td>
                                    <td>Total Calls</td>
                                    <td>Total Minutes</td>
                                    <td>Rate</td>
                                     <td>Currency</td>
                                      <td>Amount</td>
                                    
                                </tr>
                                
                                 <tr class="sub-row">
                                    <td>JUL-15</td>
                                    <td>Pakistan</td>
                                    <td>Normal Hours</td>
                                    <td>4,804.00</td>
                                    <td>9,886.65</td>
                                    <td>0.0880</td>
                                     <td>USD</td>
                                      <td>870.03</td>
                                    
                                </tr>
                               
                                
                                 <tr class="last-row">
                                    <td colspan="3">Grand Total</td>
                                    <td>4,804.00</td>
                                    <td>9,886.65</td>
                                    <td></td>
                                    <td>USD</td>
                                    <td>870.03</td>
                                    
                                </tr>
                                
                                
                            </tbody>
                        </table>
                    </div>
                    
                    
                   

</div>




</div>


@stop