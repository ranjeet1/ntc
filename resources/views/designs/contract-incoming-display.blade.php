@extends('layouts.admin.index')

@section('content')

<div class="ntc-invoice">
<div class="container invoice-bg">

<h2>Proposed Rate Revision</h2>




<div class="table-responsive" style="margin-top:20px;">


<table class="" width="100%" cellspacing=0 border=1>
                    <tbody>
                        
                        <tr style="height:56px;">
                            <td style="font-family:Calibri;text-align:center;font-size:12px;background-color:#FFFF00;color:#000000;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" rowspan=2>
                                <nobr>SN</nobr>
                            </td>
                            <td style="font-family:Calibri;text-align:center;font-size:12px;background-color:#FFFF00;color:#000000;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" rowspan=2>
                                <nobr>Country</nobr>
                            </td>
                            <td style="font-family:Calibri;text-align:center;font-size:12px;background-color:#FFFF00;color:#000000;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" rowspan=2>
                                <nobr>Operators</nobr>
                            </td>
                            <td style="font-family:Calibri;text-align:center;font-size:12px;background-color:#FFFF00;color:#000000;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" rowspan=2>
                                <nobr>Type</nobr>
                            </td>
                            <td style="font-family:Calibri;text-align:center;font-size:12px;background-color:#FFFF00;color:#000000;font-weight:bold;border-left:1px solid;border-top:1px solid;border-bottom:1px solid;border-left-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" colspan=3>
                                <nobr>Existing Rate
                                    
                                    <br/>
(Jan to Mar 2016)
                                
                                </nobr>
                            </td>
                            <td style="font-family:Calibri;text-align:center;font-size:10px;background-color:#FFFF00;color:#000000;font-weight:bold;border-left:1px solid;border-right:1px solid;border-top:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;min-width:50px" rowspan=2>
                                <nobr>Currency</nobr>
                            </td>
                            
                            
                            
                            
                            
                            
                        </tr>
                       <tr style="height:52px;">
                            <td style="font-family:Calibri;text-align:center;font-size:10px;background-color:#FFFF00;color:#000000;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>Termination</nobr>
                            </td>
                            <td style="font-family:Calibri;text-align:center;font-size:10px;background-color:#FFFF00;color:#000000;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>Volume Ceiling</nobr>
                            </td>
                            <td style="font-family:Calibri;text-align:center;font-size:10px;background-color:#FFFF00;color:#000000;font-weight:bold;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>Rate per 
                                    
                                    <br/>
minute
                                
                                </nobr>
                            </td>
                            
                           
                        </tr>
                        <tr style="height:20px;">
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" rowspan=3>
                                <nobr>1</nobr>
                            </td>
                            <td style="text-align:center;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" rowspan=3>
                                <nobr>Australia</nobr>
                            </td>
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" rowspan=3>
                                <nobr>Danfe Tel</nobr>
                            </td>
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" rowspan=3>
                                <nobr>T2</nobr>
                            </td>
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>Nepal Telecom Fixed</nobr>
                            </td>
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>x</nobr>
                            </td>
                            <td style="min-width:50px">
                                <nobr>&nbsp;</nobr>
                            </td>
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px" rowspan=3>
                                <nobr>USD</nobr>
                            </td>
                           
                        </tr>
                        <tr style="height:40px;">
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>Nepal Telecom Mobile &amp; Offnet 1 </nobr>
                            </td>
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>x</nobr>
                            </td>
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>&nbsp;</nobr>
                            </td>
                            
                            
                            
                            
                        </tr>
                        <tr style="height:20px;">
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>Offnet 2</nobr>
                            </td>
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>x</nobr>
                            </td>
                            <td style="text-align:center;background-color:#FFFFFF;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>&nbsp;</nobr>
                            </td>
                            
                        </tr>
                        <tr style="height:20px;">
                            <td style="text-align:center;background-color:#C0C0C0;border-left:1px solid;border-right:1px solid;border-top:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;min-width:50px" rowspan=3>
                                <nobr>2</nobr>
                            </td>
                            <td style="text-align:center;border-left:1px solid;border-right:1px solid;border-top:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;min-width:50px" rowspan=3>
                                <nobr>Bhutan</nobr>
                            </td>
                            <td style="text-align:center;background-color:#C0C0C0;border-left:1px solid;border-right:1px solid;border-top:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;min-width:50px" rowspan=3>
                                <nobr>Bhutan 
                                    
                                    <br/>
Telecom
                                
                                </nobr>
                            </td>
                            <td style="text-align:center;background-color:#C0C0C0;border-left:1px solid;border-right:1px solid;border-top:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;min-width:50px" rowspan=3>
                                <nobr>R</nobr>
                            </td>
                            <td style="text-align:center;background-color:#C0C0C0;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>Nepal Telecom</nobr>
                            </td>
                            <td style="text-align:center;background-color:#C0C0C0;border:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;border-bottom-color:#000000;min-width:50px">
                                <nobr>x</nobr>
                            </td>
                            <td style="min-width:50px">
                                <nobr>&nbsp;</nobr>
                            </td>
                            <td style="text-align:center;background-color:#C0C0C0;border-left:1px solid;border-right:1px solid;border-top:1px solid;border-left-color:#000000;border-right-color:#000000;border-top-color:#000000;min-width:50px" rowspan=3>
                                <nobr>USD</nobr>
                            </td>
                          
                       
                            
                        </tr>
                        
                       
                    </tbody>
                </table>



 
                       



</div>
                    

</div>
</div>

@stop