@extends('layouts.pdf')

@section('main-content')
    <div id="customer">

        <table id="meta">
            <tr>
                <td rowspan="5" style="border: 1px solid white; border-right: 1px solid black; text-align: left"
                    width="62%">
                    <strong>Invoiced To</strong> <br>
                    {{ $agent->name }} <br>
                    Email: {{ $agent->email }} <br>
                    Phone: {{ $agent->phone }}
                </td>
                <td class="meta-head">INVOICE #</td>
                <td></td>
            </tr>
            <tr>
                <td class="meta-head">Invoice Date</td>
                <td>{{ formatDate(date('Y-m-d', time())) }}</td>
            </tr>
            <tr>
                <td class="meta-head">Due Date</td>
                <td></td>
            </tr>

            <tr>
                <td class="meta-head">Amount Due</td>
                <td>
                    <div class="due">
                        <?php
                            $amountDue = 0;
                            foreach($agentAdvertisements as $adv):
                                $amountDue += floatval($adv->total_cost) - floatval($adv->amount_received);
                            endforeach;
                        ?>
                        {{ formatCurrency($amountDue) }}
                    </div>
                </td>
            </tr>

        </table>

    </div>

    <table id="items">

        <tr>
            <th width="45%">Particular</th>
            <th align="right">Cost</th>
            <th align="right">Paid</th>
            <th align="right">Remaining</th>
        </tr>
        <?php
            $subTotal = 0;
            $tax = 0;
        ?>
        @foreach($agentAdvertisements as $advertisement)
            <tr class="item-row">
                <td class="description">{{ $advertisement->name }}</td>
                <td align="right">{{ formatCurrency($advertisement->total_cost) }}</td>
                <td align="right">{{ formatCurrency($advertisement->amount_received) }}</td>
                <td align="right"><span class="price">{{ formatCurrency($advertisement->total_cost-$advertisement->amount_received) }}</span></td>
            </tr>
            <?php
                $subTotal += (float) $advertisement->total_cost-(float) $advertisement->amount_received;
                if($advertisement->include_tax == 'yes')
                    $tax += ($subTotal*$taxRate*0.01);
            ?>
        @endforeach

        <tr>
            <td class="blank"></td>
            <td colspan="2" class="total-line">Sub Total</td>
            <td class="total-value">
                <div id="subtotal">{{ formatCurrency($subTotal) }}</div>
            </td>
        </tr>

        <tr>
            <td class="blank"></td>
            <td colspan="2" class="total-line">TAX</td>
            <td class="total-value">
                <div id="total">{{ formatCurrency($tax) }}</div>
            </td>
        </tr>

        <tr>
            <td class="blank"></td>
            <td colspan="2" class="total-line balance">Grand Total</td>
            <td class="total-value balance">
                <div class="due">{{ formatCurrency($subTotal+$tax) }}</div>
            </td>
        </tr>

    </table>
@stop