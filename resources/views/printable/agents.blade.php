@extends('layouts.pdf')

@section('main-content')
    <h3>Agents Lists</h3>
    <table id="items">
        <thead>
        <tr>
            <th width="25%">Name</th>
            <th align="">Email</th>
            <th align="">Phone</th>
            <th align="">Remarks</th>
        </tr>
        </thead>
        <tbody>
        @foreach( $agents as $agent )
            <tr>
                <td class="description">{{ $agent->name }}</td>
                <td align="">{{ $agent->email }}</td>
                <td align="">{{ !empty($agent->phone) ? $agent->phone : '-' }}</td>
                <td align="">{{ !empty($agent->remarks) ? $agent->remarks : 'NA' }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop