@extends('layouts.pdf')

@section('main-content')
    <div id="customer">

        <table id="meta">
            <tr>
                <td rowspan="5" style="border: 1px solid white; border-right: 1px solid black; text-align: left"
                    width="62%">
                    <strong>Invoiced To</strong> <br>
                    {{ $advertisement->agent->name }} <br>
                    Email: {{ $advertisement->agent->email }} <br>
                    Phone: {{ $advertisement->agent->phone }}
                </td>
                <td class="meta-head">INVOICE #</td>
                <td></td>
            </tr>
            <tr>
                <td class="meta-head">Status</td>
                <td>{{ $advertisement->paymentStatus() }}</td>
            </tr>
            <tr>
                <td class="meta-head">Invoice Date</td>
                <td>{{ formatDate(date('Y-m-d', time())) }}</td>
            </tr>
            <tr>
                <td class="meta-head">Due Date</td>
                <td>{{ formatDate($advertisement->end_date) }}</td>
            </tr>

            <tr>
                <td class="meta-head">Amount Due</td>
                <td>
                    <div class="due">{{ formatCurrency($advertisement->total_cost-$advertisement->amount_received) }}</div>
                </td>
            </tr>

        </table>

    </div>

    <table id="items">
        <tr>
            <th width="45%">Particulars</th>
            <th align="right">Date</th>
            <th align="right">Payment Method</th>
            <th align="right">Amount</th>
        </tr>
        <?php $subTotal = 0 ?>
        @foreach($transactions as $transaction)
            <?php $subTotal = $transaction->amount_received; ?>
            <tr class="item-row">
                <td class="description">{{ $transaction->remarks }}</td>
                <td align="right">{{ formatDate($transaction->date) }}</td>
                <td align="right">{{ ucwords($transaction->payment_type) }}</td>
                <td align="right">{{ formatCurrency($transaction->amount_received) }}</td>
            </tr>
        @endforeach

        <tr>
            <td class="blank"></td>
            <td colspan="2" class="total-line">Sub Total</td>
            <td class="total-value">
                <div id="subtotal">{{ formatCurrency($subTotal) }}</div>
            </td>
        </tr>
        <tr>

            <td class="blank"></td>
            <td colspan="2" class="total-line">TAX</td>
            <td class="total-value">
                <div id="total">Nrs 0</div>
            </td>
        </tr>

        <tr>
            <td class="blank"></td>
            <td colspan="2" class="total-line balance">Grand Total</td>
            <td class="total-value balance">
                <div class="due">{{ formatCurrency($subTotal) }}</div>
            </td>
        </tr>

    </table>
@stop