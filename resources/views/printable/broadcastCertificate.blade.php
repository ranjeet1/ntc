<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manoj FM</title>
    <link href='https://fonts.googleapis.com/css?family=Poppins:400,500,600,700' rel='stylesheet' type='text/css'>
    <style>
        body, html {
            margin: 0;
            padding: 0;
            background: #f0f0f0;
        }

        .form-print {
            margin: 0 auto;
            padding: 0;
            width: 800px;
        }

        .form-print-bg {
            margin: 0;
            padding: 30px 40px 100px 80px;
            background: #fff;
        }

        .form-print .heading {
            margin: 0;
            padding: 150px 20px 20px 20px;
            font-family: 'Poppins', sans-serif;
            text-align: center;
            font-size: 30px;
            font-weight: 600;
            color: #000;
            text-decoration: underline;
        }

        .form-print p {
            margin: 0;
            padding: 5px 30px 20px 50px;
            Font-family: 'Poppins', sans-serif;
            font-size: 14px;
            color: #000;
        }

        .printform {
            margin: 0;
            padding: 0px 0 0 50px;
        }

        .printform label {
            float: left;
            width: 200px;
            font-size: 15px;
            font-weight: 400;
            margin: 0;
            padding: 0;
            Font-family: 'Poppins', sans-serif;
        }

        .printform .forms {
            text-align: left;
            margin: 0;
            padding: 3px 0 3px 0;
            Font-family: 'Poppins', sans-serif;
        }

        .printform span {
            text-align: left;
            margin: 0;
            height: 28px;
            Font-family: 'Poppins', sans-serif;
            width: 300px;
        }

        .signiture {
            float: right;
            margin: 0 30px 10px 0;
            padding: 0;
            border-bottom: dashed 1px #000;
            width: 120px;
        }

        .form-name {
            clear: both;
            margin: 0;
            padding: 0px 30px 0 0;
            text-align: right;
            Font-family: 'Poppins', sans-serif;
            font-size: 15px;
            line-height: 20px;
        }

        .bottom-slogan {
            margin: 0;
            padding: 80px 0 130px 0;
            text-align: center;
            Font-family: 'Poppins', sans-serif;
            font-size: 18px;
        }

        #page-wrap {
            width: 800px;
            margin: 0 auto;
        }


    </style>


    <!-- Bootstrap Core CSS -->


</head>
<body>

<div id="page-wrap">
    <br/><br/>
    <button style="padding: 5px" onclick="printDiv('print-content'); event.preventDefault();">Print this
        page
    </button>

    <div id="print-content">

        <div class="form-print">
            <div class="form-print-bg">
                <div class="heading">Broadcasting Certificate</div>
                <p>{{ $request->get('body') }}</p>

                <form method="POST" class="printform" action="">


                    <div class="forms">
                        <label class="control-label">Client Name</label>: {{ $request->get('client_name') }}

                    </div>

                    <div class="forms">
                        <label class="control-label">Program</label>: {{ $request->get('program') }}
                    </div>

                    <div class="forms">
                        <label class="control-label">Broadcasting Date Period</label>: {{ $request->get('start_date') }}
                        to {{ $request->get('end_date') }}
                    </div>

                    <div class="forms">
                        <label class="control-label">Time</label>: {{ $request->get('time') }}
                    </div>

                    <div class="forms">
                        <label class="control-label">Total</label>: {{ $request->get('total') }}
                    </div>

                    <div class="signiture">&nbsp;</div>
                    <div class="form-name">{{ $request->get('name') }}</div>
                    <div class="form-name">{{ $request->get('title') }}</div>


                </form>


            </div>


        </div>
    </div>
</div>


<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

    document.addEventListener("DOMContentLoaded", function(event) {
        printDiv('print-content');
    });

</script>

</body>
</html>
