@extends('layouts.pdf')

@section('main-content')
    <div id="customer">

        <table id="meta">
            <tr>
                <td rowspan="5" style="border: 1px solid white; xborder-right: 1px solid black; text-align: left"
                    width="62%">
                    Date : {{ formatDate($transaction->received_date) }}
                    <br/><br/>
                    <h3>For : {{ $transaction->advertisement->name }}</h3>
                    <hr/>
                    <br/>
                    <strong>Billed To</strong> <br>
                    {{ $transaction->agent->name }} <br>
                    Email: {{ $transaction->agent->email }} <br>
                    Phone: {{ $transaction->agent->phone }}
                </td>
            </tr>
        </table>

    </div>

    <table id="items">

        <tr>
            <th width="55%">Particular</th>
            <th align="right">Type</th>
            <th align="right">Amount</th>
        </tr>


        <tr class="item-row">
            <td class="description">{{ $transaction->remarks }}</td>
            <td>{{ ucwords($transaction->payment_type) }}</td>
            <td align="right">{{ formatCurrency($transaction->amount_received) }}</td>
        </tr>
        <tr>
            <td colspan="2" class="total-line">Sub Total</td>
            <td align="right">{{ formatCurrency($transaction->amount_received) }}</td>
        </tr>
        <tr>
            <td colspan="2" class="total-line">TAX</td>
            <td align="right">Nrs. 0.00</td>
        </tr>
        <tr>
            <td colspan="2" class="total-line balance">Grand Total</td>
            <td align="right">{{ formatCurrency($transaction->amount_received) }}</td>
        </tr>

    </table>
@stop