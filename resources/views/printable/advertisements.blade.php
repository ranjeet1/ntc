@extends('layouts.pdf')

@section('main-content')
    <h3>Advertisement Lists</h3>
    <table id="items">
        <thead>
        <tr>
            <th style="width: 15%">Name</th>
            <th>Image</th>
            <th>Type</th>
            <th>Agent</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th align="">Remarks</th>
        </tr>
        </thead>
        <tbody>
        @foreach( $advertisements as $advertisement )
            <tr>
                <td class="description">{{ $advertisement->name }}</td>
                <td align=""><img src="{{ asset('uploads/content/thumb/'. $advertisement->image) }}" width="100px" alt=""/></td>
                <td align="">{{ !empty($advertisement->agent) ? $advertisement->advertisementType->name : 'NA' }}</td>
                <td align="">{{ !empty($advertisement->agent) ? $advertisement->agent->name : 'NA' }}</td>
                <td align="">{{ !empty($advertisement->agent) ? formatDate($advertisement->start_date) : 'NA' }}</td>
                <td align="">{{ !empty($advertisement->agent) ? formatDate($advertisement->end_date) : 'NA' }}</td>
                <td align="">{{ !empty($advertisement->remarks) ? $advertisement->remarks : 'NA' }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop