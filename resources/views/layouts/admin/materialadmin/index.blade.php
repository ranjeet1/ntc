<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
@include('layouts.admin.materialadmin.partials.head')


<body class="menubar-hoverable header-fixed menubar-pin ">

@yield('css')

@include('layouts.admin.materialadmin.partials.header')

<div id="base">

    <div class="offcanvas">
    </div>

    <div id="content">


        <section>

            <div class="section-header">
                <ol class="breadcrumb">
                    <li><a href="#">home</a></li>
                    <li class="active">Blank page</li>
                </ol>
            </div>


                @yield('content')


        </section>


    </div>

    @include('layouts.admin.materialadmin.partials.menubar')

    @include('layouts.admin.materialadmin.partials.offcanvas')

</div>

@include('layouts.admin.materialadmin.partials.footer')

@yield('js')

</body>

</html>