<!DOCTYPE html>
<html lang="en">




<!-- Mirrored from www.codecovers.eu/materialadmin/pages/login by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Apr 2016 15:08:47 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <title>Material Admin - Login</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <link type="text/css" rel="stylesheet" href="{{ asset("assets/css/theme-default/bootstrap94be.css?1422823238") }}" />

    <link type="text/css" rel="stylesheet" href="{{ asset("assets/css/theme-default/materialadminb0e2.css?1422823243")}}" />

    <link type="text/css" rel="stylesheet" href="{{ asset("assets/css/theme-default/font-awesome.min753e.css?1422823239")}}" />

    <link type="text/css" rel="stylesheet" href="{{ asset("assets/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240")}}" />


    <!-- END STYLESHEETS -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{ asset("assets/js/libs/utils/html5shiv.js?1422823601")}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/libs/utils/respond.min.js?1422823601')}}"></script>
    <![endif]-->
</head>






<body class="menubar-hoverable header-fixed ">

@yield('content')


<!-- BEGIN JAVASCRIPT -->
<script src="{{ asset('assets/js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/spin.js/spin.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/autosize/jquery.autosize.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/nanoscroller/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ asset('assets/js/core/cache/63d0445130d69b2868a8d28c93309746.js') }}"></script>
<script src="{{ asset('assets/js/core/demo/Demo.js') }}"></script>


<!-- END JAVASCRIPT -->



</body>

<!-- Mirrored from www.codecovers.eu/materialadmin/pages/login by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 24 Apr 2016 15:08:52 GMT -->
</html>