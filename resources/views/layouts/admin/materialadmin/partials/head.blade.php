<head>
    <title>Material Admin - Blank page</title>

    <!-- BEGIN META -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/bootstrap94be.css?1422823238') }}" />

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/materialadminb0e2.css?1422823243')}}" />

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/font-awesome.min753e.css?1422823239')}}" />

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240')}}" />


    <!-- END STYLESHEETS -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{ asset("assets/js/libs/utils/html5shiv.js?1422823601")}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/libs/utils/respond.min.js?1422823601')}}"></script>
    <![endif]-->
</head>