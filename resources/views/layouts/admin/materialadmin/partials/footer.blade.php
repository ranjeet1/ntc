
<!-- BEGIN JAVASCRIPT -->
<script src="{{ asset('assets/js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('assets/js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{ asset('assets/js/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/js/libs/spin.js/spin.min.js')}}"></script>
<script src="{{ asset('assets/js/libs/autosize/jquery.autosize.min.js')}}"></script>
<script src="{{ asset('assets/js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
<script src="{{ asset('assets/js/core/cache/63d0445130d69b2868a8d28c93309746.js')}}"></script>
<script src="{{ asset('assets/js/core/demo/Demo.js')}}"></script>


<!-- END JAVASCRIPT -->