<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>Admin Login</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css') }}/login.css" />

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700,800' rel='stylesheet' type='text/css'/>
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css') }}/theme-5/bootstrap1cc3.css?1401441891" />

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css') }}/theme-5/boostbox47dd.css?1401441889" />

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css') }}/theme-5/boostbox_responsive47dd.css?1401441889" />

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css') }}/theme-5/font-awesome.min1cc3.css?1401441891" />

    <!-- END STYLESHEETS -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/boostbox/libs/utils/html5shiv.js?1401441990"></script>
    <script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/boostbox/libs/utils/respond.min.js?1401441990"></script>
    <![endif]-->
</head>





<body 	class="body-dark">



@yield('content')

<!-- BEGIN JAVASCRIPT -->
<script src="{{ asset('assets/boostbox/js') }}/libs/jquery/jquery-1.11.0.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/core/BootstrapFixed.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/bootstrap/bootstrap.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/spin.js/spin.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/slimscroll/jquery.slimscroll.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/core/App.js"></script>
<script src="{{ asset('assets/boostbox/js')  }}/core/demo/Demo.js"></script>


<!-- END JAVASCRIPT -->

</body>

</html>