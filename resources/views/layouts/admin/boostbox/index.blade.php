<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

@include('layouts.admin.boostbox.partials.head')

<body class="">

<!-- BEGIN HEADER-->
@include('layouts.admin.boostbox.partials.header')
<!-- END HEADER-->

<!-- BEGIN BASE-->
<div id="base">

    <!-- BEGIN SIDEBAR-->
    @include('layouts.admin.boostbox.partials.sidebar')
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT-->
    <div id="content">

        <section>

            <div class="">

                {{--<div class="pull-left">--}}
                    {{--<h3 class="text-standard"><i class="fa fa-fw fa-arrow-circle-right text-gray-light"></i> Dashboard </h3>--}}
                {{--</div>--}}

                <div class="pull-right">
                    {!! Breadcrumbs::renderIfExists() !!}
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>

            <div class="section-body">

                @include('layouts.admin.boostbox.partials.flash')

                @yield('content')

            </div><!--end .section-body -->
        </section>

    </div><!--end #content-->
    <!-- END CONTENT -->

</div><!--end #base-->
<!-- END BASE -->

<!-- BEGIN JAVASCRIPT -->
@include('layouts.admin.boostbox.partials.footer')

@yield('js')
<!-- END JAVASCRIPT -->

</body>


</html>