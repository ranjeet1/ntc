<head>
    <title>Interconnect Admin</title>

    <!-- BEGIN META -->
    <meta name="_token" content="{{ csrf_token() }}"/>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,600,700,800' rel='stylesheet' type='text/css'/>

    {{--<link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/bootstrap7e8b.css?1401441883" />--}}
    {{--<link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/boostbox170b.css?1401441881" />--}}
    {{--<link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/boostbox_responsive170b.css?1401441881" />--}}
    {{--<link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/font-awesome.min7e8b.css?1401441883" />--}}


    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/boostbox.css" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/boostbox_responsive.css" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/font-awesome.min.css" />

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/libs/jquery-ui/jquery-ui-1.10.4.custom.css" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/libs/fullcalendar/fullcalendar8439.css?1401442097" />

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/libs/DataTables/jquery.dataTables872c.css" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/theme-3') }}/libs/DataTables/TableTools872c.css" />

    <link href="{{ asset('assets/boostbox/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/boostbox/plugins/lity/lity.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/boostbox/plugins/intl-tel-input/intlTelInput.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/boostbox/css/custom.css') }}"/>
    @yield('css')
    <!-- END STYLESHEETS -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/boostbox/libs/utils/html5shiv.js?1401441990"></script>
    <script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/boostbox/libs/utils/respond.min.js?1401441990"></script>
    <![endif]-->
</head>