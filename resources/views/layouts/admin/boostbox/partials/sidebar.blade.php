<div id="sidebar">
    <div class="sidebar-back"></div>
    <div class="sidebar-content">
        <div class="nav-brand">
            <a class="main-brand" href="{{ url('admin') }}">
                <h3 class="text-light text-white"><span class="ntc-text">Interconnect<br/> <strong> Admin</strong>   </span></h3>
            </a>
        </div>


        <!-- BEGIN MENU SEARCH -->
        {{--<form class="sidebar-search" role="search">--}}
            {{--<a href="javascript:void(0);"><i class="fa fa-search fa-fw search-icon"></i><i--}}
                        {{--class="fa fa-angle-left fa-fw close-icon"></i></a>--}}

            {{--<div class="form-group">--}}
                {{--<div class="input-group">--}}
                    {{--<input type="text" class="form-control navbar-input" placeholder="Search..." style="color:black;">--}}
                {{--<span class="input-group-btn">--}}
                    {{--<button class="btn btn-equal" type="button"><i class="fa fa-search"></i></button>--}}
                {{--</span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</form>--}}
        <!-- END MENU SEARCH -->

        <!-- BEGIN MAIN MENU -->
        <ul class="main-menu">
            <!-- Menu Dashboard -->
            <li>
                <a href="{{ url('admin') }}">
                    <i class="fa fa-home fa-fw fa-2x"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <!-- User and Roles Links -->
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-users fa-fw fa-2x"></i><span class="title">User Management</span> <span
                            class="expand-sign">+</span>
                </a>
                <ul>
                    <li><a href="{{ route('admin.user.index') }}">Users</a></li>
                    <li><a href="{{ route('admin.roles.index') }}">Roles</a></li>
                    <li><a href="{{ route('admin.permissions.index') }}">Permissions</a></li>
                </ul>
            </li>



            <!-- Menu Pages -->
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-mobile-phone fa-fw fa-2x"></i><span class="title">Operators</span> <span
                            class="expand-sign">+</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="{{ route('admin.operator.index') }}">Operators</a></li>
                    <li><a href="{{ route('admin.invoice.index') }}">In Bound Invoices</a></li>
                    <li><a href="#">Outbound Invoice</a></li>
                </ul>
                <!--end /submenu -->
            </li>
            <!--end /menu-item -->
            <!-- Menu Tables -->

            <!-- Menu Pages -->
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-thumbs-up fa-fw fa-2x"></i><span class="title">Settlement of Statement</span>
                </a>

            </li>
            <!-- Menu Tables -->
            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-mail-forward fa-fw fa-2x"></i><span class="title">Dispute Management</span> <span
                            class="expand-sign">+</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="#">Recording</a></li>

                    <li><a href="#">Completions/Settlement</a></li>

                    <li><a href="#">Bill &amp; Invoice Adjustments done Automatically</a></li>

                </ul>
                <!--end /submenu -->
            </li>

            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-file-text fa-fw fa-2x"></i><span class="title">Debit &amp; Credit Memo</span> <span
                            class="expand-sign">+</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="">Recording</a></li>

                    <li><a href="">Completions/Settlement</a></li>


                </ul>
                <!--end /submenu -->
            </li>

            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-money fa-fw fa-2x"></i><span class="title">Accounting</span> <span
                            class="expand-sign">+</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="../pages/profile.html">Individual Ledger of Parties</a></li>

                    <li><a href="../pages/invoice.html">Gross Payable/Receivable</a></li>


                </ul>
                <!--end /submenu -->
            </li>

            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-pencil-square-o fa-fw fa-2x"></i><span class="title">Reporting</span> <span
                            class="expand-sign">+</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="../pages/profile.html">Trend Analysis</a></li>

                    <li><a href="../pages/invoice.html">profit Loss</a></li>
                    <li><a href="../pages/invoice.html">AR-AP Comparision</a></li>


                </ul>
                <!--end /submenu -->
            </li>

            <li>
                <a href="javascript:void(0);">
                    <i class="fa fa-puzzle-piece fa-fw fa-2x"></i><span class="title">Setting</span> <span
                            class="expand-sign">+</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="{{ route('admin.option') }}">General Setting</a></li>
                    <li><a href="{{ route('admin.operator-types.index') }}">Operator Types</a></li>
                    <li><a href="{{ route('admin.service.index') }}">Operator Services</a></li>
                    <li><a href="{{ route('admin.tier.index') }}">Tier Details</a></li>
                </ul>
                <!--end /submenu -->
            </li>


        </ul>
        <!--end .main-menu -->
        <!-- END MAIN MENU -->
    </div>
</div><!--end #sidebar-->