<!--[if lte IE 8]>
<script src="//www.codecovers.eu/assets/js/modules/boostbox/libs/excanvas-modified/excanvas.min.js"></script>
<![endif]-->

<script src="{{ asset('assets/boostbox/js') }}/libs/jquery/jquery-1.11.0.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/jquery-ui/jquery-ui-1.10.4.custom.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/core/BootstrapFixed.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/bootstrap/bootstrap.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/spin.js/spin.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/moment/moment.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/flot/jquery.flot.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/flot/jquery.flot.time.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/flot/jquery.flot.resize.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/flot/jquery.flot.orderBars.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/flot/jquery.flot.pie.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/jquery-knob/jquery.knob.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/sparkline/jquery.sparkline.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/slimscroll/jquery.slimscroll.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/fullcalendar/fullcalendar.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/core/demo/DemoCharts.js"></script>
{{--<script src="{{ asset('assets/boostbox/js') }}/core/demo/DemoDashboard.js"></script>--}}
<script src="{{ asset('assets/boostbox/js') }}/core/App.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/core/demo/Demo.js"></script>

<script src="{{ asset('assets/boostbox/js') }}/libs/DataTables/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets/boostbox/js') }}/libs/DataTables/extras/ColVis/js/ColVis.min.js"></script>
{{--<script src="{{ asset('assets/boostbox/js') }}/libs/DataTables/extras/TableTools/media/js/TableTools.min.js"></script>--}}
{{--<script src="{{ asset('assets/boostbox/js') }}/core/demo/DemoTableDynamic.js"></script>--}}
<script src="{{ asset('assets/boostbox/js') }}/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>

<script src="{{ asset('assets/boostbox/plugins/sweetalert/sweetalert-dev.js') }}"></script>
<script src="{{ asset('assets/boostbox/plugins/lity/lity.min.js') }}"></script>
<script src="{{ asset('assets/boostbox/plugins/vue/vue.js') }}"></script>
<script src="{{ asset('assets/boostbox/js/admin.js') }}"></script>
<script src="{{ asset('assets/boostbox/plugins/lity/lity.min.js') }}"></script>
<script src="{{ asset('assets/boostbox/plugins/intl-tel-input/intlTelInput.js') }}"></script>
